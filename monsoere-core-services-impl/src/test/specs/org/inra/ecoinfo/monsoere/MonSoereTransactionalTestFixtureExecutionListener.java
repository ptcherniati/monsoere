package org.inra.ecoinfo.monsoere;

import java.sql.SQLException;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.dataset.IDatasetManager;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.monsoere.refdata.espece.IEspeceDAO;
import org.inra.ecoinfo.monsoere.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.monsoere.refdata.projetsitethemedatatype.IProjetSiteThemeDatatypeDAO;
import org.inra.ecoinfo.monsoere.refdata.site.ISiteMonSoereDAO;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.IMetadataManager;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.springframework.test.context.TestContext;

/**
 *
 * @author tcherniatinsky
 */
public class MonSoereTransactionalTestFixtureExecutionListener extends TransactionalTestFixtureExecutionListener {

    private static String testName = "";

    /**
     *
     */
    public static IMetadataManager metadataManager;

    /**
     *
     */
    public static IDatasetManager datasetManager;

    /**
     *
     */
    public static IProjetSiteThemeDatatypeDAO projetSiteThemeDatatypeDAO;

    /**
     *
     */
    public static IDatasetDAO datasetDAO;

    /**
     *
     */
    public static IVersionFileDAO versionFileDAO;

    /**
     *
     */
    public static IProjetDAO projetDAO;

    /**
     *
     */
    public static ISiteMonSoereDAO siteMonSoereDAO;

    /**
     *
     */
    public static IEspeceDAO especeDAO;

    /**
     *
     */
    public static IVariableDAO variableDAO;

    /**
     *
     */
    public static ILocalizationManager localizationManager;

    /**
     *
     */
    public static IExtractionManager extractionManager;

    /**
     *
     */
    public static INotificationsManager notificationsManager;

    /**
     *
     */
    public static IFileCompManager fileCompManager;
//    public static IAssociateFileCompManager associateFileCompManager;

    /**
     *
     */
    public static IFileCompConfiguration fileCompConfiguration;
//    public static IAssociateDAO associateDAO;


    /**
     * Before test class.
     *
     * @param testContext
     * @link(TestContext) the test context
     * @throws Exception the exception @see
     * org.springframework.test.context.support.AbstractTestExecutionListener
     * #beforeTestClass(org.springframework.test.context.TestContext)
     */
    @Override
    public void beforeTestClass(final TestContext testContext) throws Exception {
        super.beforeTestClass(testContext);
        MonSoereTransactionalTestFixtureExecutionListener.metadataManager = (IMetadataManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("metadataManager");
        MonSoereTransactionalTestFixtureExecutionListener.datasetManager = (IDatasetManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datasetManager");
        MonSoereTransactionalTestFixtureExecutionListener.projetSiteThemeDatatypeDAO = (IProjetSiteThemeDatatypeDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("projetSiteThemeDatatypeDAO");
        MonSoereTransactionalTestFixtureExecutionListener.datasetDAO = (IDatasetDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datasetDAO");
        MonSoereTransactionalTestFixtureExecutionListener.versionFileDAO = (IVersionFileDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("versionFileDAO");
        MonSoereTransactionalTestFixtureExecutionListener.projetDAO = (IProjetDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("projetDAO");
        MonSoereTransactionalTestFixtureExecutionListener.siteMonSoereDAO = (ISiteMonSoereDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("siteMonSoereDAO");
        MonSoereTransactionalTestFixtureExecutionListener.especeDAO = (IEspeceDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("especeDAO");
        MonSoereTransactionalTestFixtureExecutionListener.variableDAO = (IVariableDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("variableDAO");
        MonSoereTransactionalTestFixtureExecutionListener.localizationManager = (ILocalizationManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("localizationManager");
        MonSoereTransactionalTestFixtureExecutionListener.extractionManager = (IExtractionManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("extractionManager");
        MonSoereTransactionalTestFixtureExecutionListener.notificationsManager = (INotificationsManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("notificationsManager");
        MonSoereTransactionalTestFixtureExecutionListener.fileCompManager = (IFileCompManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("fileCompManager");
//        MonSoereTransactionalTestFixtureExecutionListener.associateFileCompManager = (IAssociateFileCompManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("associateFileCompManager");
        MonSoereTransactionalTestFixtureExecutionListener.fileCompConfiguration = (IFileCompConfiguration) TransactionalTestFixtureExecutionListener.applicationContext.getBean("fileCompConfiguration");
//        MonSoereTransactionalTestFixtureExecutionListener.associateDAO = (IAssociateDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("associateDAO");

        if (testName.isEmpty()) {
            testName = "../target/concordion/" + testContext.getTestClass().getName().replaceAll(".*\\.(.*?)Fixture", "$1");
        }
        System.setProperty("concordion.output.dir", testName);
    }

    /**
     * Clean tables.
     *
     * @throws SQLException the sQL exception
     */
    @Override
    protected void cleanTables() throws SQLException {
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from group_utilisateur cascade;")
                .execute();
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from utilisateur cascade;")
                .execute();
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from groupe where group_name!='public';")
                .execute();
    }
}

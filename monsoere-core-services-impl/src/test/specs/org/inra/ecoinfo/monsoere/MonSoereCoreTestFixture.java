package org.inra.ecoinfo.monsoere;

import org.concordion.api.ExpectedToPass;
import org.inra.ecoinfo.AbstractTestFixture;
import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tcherniatinsky
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {MonSoereTransactionalTestFixtureExecutionListener.class})
@ExpectedToPass
public class MonSoereCoreTestFixture extends AbstractTestFixture {

    /**
     *
     */
    public MonSoereCoreTestFixture() {
        super();
        TransactionalTestFixtureExecutionListener.startSession();
    }
}

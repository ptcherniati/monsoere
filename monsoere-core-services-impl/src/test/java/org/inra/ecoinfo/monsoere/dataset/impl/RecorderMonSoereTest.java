/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.monsoere.dataset.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.monsoere.dataset.IDeleteRecord;
import org.inra.ecoinfo.monsoere.dataset.IProcessRecord;
import org.inra.ecoinfo.monsoere.dataset.ITestFormat;
import org.inra.ecoinfo.monsoere.test.utils.MockUtils;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.xml.sax.SAXException;

/**
 *
 * @author ptcherniati
 */
public class RecorderMonSoereTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    private MockUtils    m;
    @Mock
    IDeleteRecord        deleteRecord;
    @Mock
    IProcessRecord       processRecord;
    @Mock
    ITestFormat          testFormat;
    @Spy
    RecorderMonSoere     instance;

    @Mock
    DatasetDescriptor    datasetDescriptor;

    @Mock
    ILocalizationManager localizationManager;

    /**
     *
     */
    public RecorderMonSoereTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        m = MockUtils.getInstance();
        RecorderMonSoere.setStaticLocalizationManager(MockUtils.localizationManager);
        instance.setDeleteRecord(deleteRecord);
        instance.setProcessRecord(processRecord);
        instance.setTestFormat(testFormat);
        instance.setDatasetDescriptor(datasetDescriptor);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecords method, of class RecorderMonSoere.
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteRecords() throws Exception {
        RecorderMonSoere instance = new RecorderMonSoere();
        IDeleteRecord deleteRecord = Mockito.mock(IDeleteRecord.class);
        instance.setDeleteRecord(deleteRecord);
        instance.deleteRecords(m.versionFile);
        Mockito.verify(deleteRecord).deleteRecord(m.versionFile);
    }

    /**
     * Test of getMonSoereMessage method, of class RecorderMonSoere.
     */
    @Test
    public void testGetMonSoereMessage() {
        String key = RecorderMonSoere.PROPERTY_CST_DATE_TYPE;
        String expResult = "date";
        String result = RecorderMonSoere.getMonSoereMessage(key);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getMonSoereMessageWithBundle method, of class RecorderMonSoere.
     */
    @Test
    public void testGetMonSoereMessageWithBundle() {
        String bundlePath = "org.inra.ecoinfo.monsoere.refdata.projetsitethemedatatype.messages";
        String key = "PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB";
        String expResult = "Ligne %d colonne %d, le site portant le code \"%s\" n'existe pas en base de données";
        String result = RecorderMonSoere.getMonSoereMessageWithBundle(bundlePath, key);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of record method, of class RecorderMonSoere.
     * @throws java.lang.Exception
     */
    @Test
    public void testRecord() throws Exception {
        VersionFile versionFile = m.versionFile;
        String fileEncoding = "UTF8";
        Mockito.when(m.versionFile.getData()).thenReturn("data;autre data".getBytes());
        Mockito.when(processRecord.lookupDatasetDescriptor("datasetDescriptorPath.xml"))
                .thenReturn(datasetDescriptor);
        instance.setDatasetDescriptorPath("datasetDescriptorPath.xml");
        Mockito.doReturn(localizationManager).when(instance).getLocalizationManager();
        Mockito.when(
                localizationManager.getMessage(RecorderMonSoere.MONSOERE_DATASET_BUNDLE_NAME,
                        RecorderMonSoere.PROPERTY_MSG_ERROR_BAD_FORMAT)).thenReturn("message");
        ArgumentCaptor<CSVParser> parser = ArgumentCaptor.forClass(CSVParser.class);
        ArgumentCaptor<VersionFile> vf = ArgumentCaptor.forClass(VersionFile.class);
        ArgumentCaptor<String> fe = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<DatasetDescriptor> ds = ArgumentCaptor.forClass(DatasetDescriptor.class);
        instance.record(versionFile, fileEncoding);
        Mockito.verify(testFormat).testFormat(parser.capture(), vf.capture(), fe.capture(),
                ds.capture());
        Assert.assertEquals("data", parser.getValue().nextValue());
        Assert.assertEquals("autre data", parser.getValue().nextValue());
        Assert.assertEquals(m.versionFile, vf.getValue());
        Assert.assertEquals("UTF8", fe.getValue());
        Assert.assertEquals(datasetDescriptor, ds.getValue());
        Mockito.verify(processRecord).processRecord(parser.capture(), vf.capture(), fe.capture(),
                ds.capture());
        Assert.assertEquals("data", parser.getValue().nextValue());
        Assert.assertEquals("autre data", parser.getValue().nextValue());
        Assert.assertEquals(m.versionFile, vf.getValue());
        Assert.assertEquals("UTF8", fe.getValue());
        Assert.assertEquals(datasetDescriptor, ds.getValue());
    }

    /**
     * Test of setDatasetDescriptorPath method, of class RecorderMonSoere.
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws org.xml.sax.SAXException
     * @throws org.inra.ecoinfo.utils.exceptions.BadFormatException
     * @throws java.io.IOException
     */
    @Test
    public void testSetDatasetDescriptorPath() throws BusinessException, BadFormatException,
            IOException, SAXException {
        String fileEncoding = "UTF8";
        Mockito.when(m.versionFile.getData()).thenReturn("data;autre data".getBytes());
        Mockito.doReturn(localizationManager).when(instance).getLocalizationManager();
        Mockito.when(
                localizationManager.getMessage(RecorderMonSoere.MONSOERE_DATASET_BUNDLE_NAME,
                        RecorderMonSoere.PROPERTY_MSG_ERROR_BAD_FORMAT)).thenReturn("message");
        String datasetDescriptorPath = "path";
        instance.setDatasetDescriptorPath(datasetDescriptorPath);
        Mockito.when(processRecord.lookupDatasetDescriptor("path")).thenReturn(datasetDescriptor);
        instance.record(m.versionFile, fileEncoding);
        Mockito.verify(processRecord).lookupDatasetDescriptor("path");
    }

    /**
     * Test of setStaticLocalizationManager method, of class RecorderMonSoere.
     */
    @Test
    public void testSetStaticLocalizationManager() {
        ILocalizationManager localizationManager = Mockito.mock(ILocalizationManager.class);
        RecorderMonSoere.setStaticLocalizationManager(localizationManager);
        Mockito.when(localizationManager.getMessage("key", "value")).thenReturn("message");
        String message = RecorderMonSoere.getMonSoereMessageWithBundle("key", "value");
        Assert.assertEquals("message", message);
    }

    /**
     * Test of testFormat method, of class RecorderMonSoere.
     * @throws java.lang.Exception
     */
    @Test
    public void testTestFormat() throws Exception {
        VersionFile versionFile = m.versionFile;
        String fileEncoding = "UTF8";
        Mockito.when(m.versionFile.getData()).thenReturn("data;autre data".getBytes());
        Mockito.when(processRecord.lookupDatasetDescriptor("datasetDescriptorPath.xml"))
                .thenReturn(datasetDescriptor);
        instance.setDatasetDescriptorPath("datasetDescriptorPath.xml");
        Mockito.doReturn(localizationManager).when(instance).getLocalizationManager();
        Mockito.when(
                localizationManager.getMessage(RecorderMonSoere.MONSOERE_DATASET_BUNDLE_NAME,
                        RecorderMonSoere.PROPERTY_MSG_ERROR_BAD_FORMAT)).thenReturn("message");
        ArgumentCaptor<CSVParser> parser = ArgumentCaptor.forClass(CSVParser.class);
        ArgumentCaptor<VersionFile> vf = ArgumentCaptor.forClass(VersionFile.class);
        ArgumentCaptor<String> fe = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<DatasetDescriptor> ds = ArgumentCaptor.forClass(DatasetDescriptor.class);
        instance.testFormat(versionFile, fileEncoding);
        Mockito.verify(testFormat).testFormat(parser.capture(), vf.capture(), fe.capture(),
                ds.capture());
        Assert.assertEquals("data", parser.getValue().nextValue());
        Assert.assertEquals("autre data", parser.getValue().nextValue());
        Assert.assertEquals(m.versionFile, vf.getValue());
        Assert.assertEquals("UTF8", fe.getValue());
        Assert.assertEquals(datasetDescriptor, ds.getValue());
    }

}

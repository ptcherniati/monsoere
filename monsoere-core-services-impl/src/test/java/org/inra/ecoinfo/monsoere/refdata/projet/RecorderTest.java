/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.monsoere.refdata.projet;

import com.Ostermiller.util.CSVParser;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.monsoere.test.utils.MockUtils;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author ptcherniati
 */
public class RecorderTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    MockUtils  m        = MockUtils.getInstance();
    Recorder   instance;
    String     encoding = "UTF-8";

    @Mock
    Properties propertiesNomFR;

    @Mock
    Properties propertiesNomEN;

    @Mock
    Properties propertiesDefinitionEN;

    /**
     *
     */
    public RecorderTest() {
    }

    /**
     *
     * @throws PersistenceException
     */
    @Before
    public void setUp() throws PersistenceException {
        m = MockUtils.getInstance();
        instance = new Recorder();
        MockitoAnnotations.initMocks(this);
        instance.setProjetDAO(m.projetDAO);
        ((SpringLocalizationManager) MockUtils.localizationManager)
                .setLocalizationDAO(m.localizationDAO);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.setMgaServiceBuilder(m.mgaServiceBuilder);
        when(m.localizationDAO.getByNKey(anyString(), anyString(), anyString(), anyString()))
                .thenReturn(Optional.empty());
        Mockito.when(m.localizationDAO.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRANCE))
                .thenReturn(propertiesNomFR);
        Mockito.when(m.localizationDAO.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH))
                .thenReturn(propertiesNomEN);
        Mockito.when(
                m.localizationDAO.newProperties(Nodeable.getLocalisationEntite(Projet.class), "definition", Locale.ENGLISH))
                .thenReturn(propertiesDefinitionEN);
        Mockito.when(propertiesNomFR.getProperty("projet")).thenReturn("projetfr");
        Mockito.when(propertiesNomEN.getProperty("projet")).thenReturn("projeten");
        Mockito.when(propertiesDefinitionEN.getProperty("Définition")).thenReturn("definitionen");
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecord method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteRecord() throws Exception {
        String text = "Projet;Channel project;Projet manche;Channel project";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        when(m.projetDAO.getByCode("Projet")).thenReturn(Optional.of(m.projet));
        instance.deleteRecord(parser, file, encoding);
        Mockito.verify(m.projetDAO).remove(m.projet);
        // bad type site code
        text = "Projetbad;Channel project;Projet manche;Channel project";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        BusinessException error = null;
        when(m.projetDAO.getByCode(anyString())).thenReturn(Optional.empty());
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("can't get projet", error.getMessage());
        error = null;
        // peristence exception
        text = "Projet;Channel project;Projet manche;Channel project";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.doThrow(new PersistenceException("error2")).when(m.projetDAO).remove(m.projet);
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("can't get projet", error.getMessage());
    }

    /**
     * Test of getAllElements method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllElements() throws Exception {
        final List<Projet> projets = Arrays.asList(new Projet[] { m.projet });
        Mockito.when(m.projetDAO.getAll(Projet.class)).thenReturn(projets);
        List<Projet> result = instance.getAllElements();
        Mockito.verify(m.projetDAO).getAll(Projet.class);
        Assert.assertTrue(1 == result.size());
        Assert.assertEquals(projets.get(0), result.get(0));
    }

    /**
     * Test of getNewLineModelGridMetadata method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNewLineModelGridMetadata() throws Exception {
        final List<Projet> projets = Arrays.asList(new Projet[] { m.projet });
        Mockito.when(m.projetDAO.getAll(Projet.class)).thenReturn(projets);
        Mockito.when(m.projet.getDescriptionProjet()).thenReturn("Définition");
        Mockito.when(m.projet.getName()).thenReturn("projet");
        instance.initModelGridMetadata();
        LineModelGridMetadata result = instance.getNewLineModelGridMetadata(m.projet);
        Assert.assertTrue(5 == result.getColumnsModelGridMetadatas().size());
        Assert.assertEquals("projet", result.getColumnModelGridMetadataAt(0).getValue());
        Assert.assertEquals("projetfr", result.getColumnModelGridMetadataAt(1).getValue());
        Assert.assertEquals("projeten", result.getColumnModelGridMetadataAt(2).getValue());
        Assert.assertEquals("Définition", result.getColumnModelGridMetadataAt(3).getValue());
        Assert.assertEquals("definitionen", result.getColumnModelGridMetadataAt(4).getValue());
    }

    /**
     * Test of initModelGridMetadata method, of class Recorder.
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testInitModelGridMetadata() throws PersistenceException {
        MockUtils.localizationManager = Mockito.spy(MockUtils.localizationManager);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.initModelGridMetadata();
        Mockito.verify(m.localizationDAO).newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRANCE);
        Mockito.verify(m.localizationDAO).newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        Mockito.verify(m.localizationDAO).newProperties(Nodeable.getLocalisationEntite(Projet.class), "definition",
                Locale.ENGLISH);
    }

    /**
     * Test of processRecord method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        Projet projet = Mockito.mock(Projet.class);
        Mockito.when(projet.getCode()).thenReturn("projet_1");
        Mockito.when(projet.getName()).thenReturn("projet_1");
        Mockito.when(projet.getDescriptionProjet()).thenReturn("Description");
        // Mockito.when(m.siteDAO.getByPath("parent/site")).thenReturn(m.site);
        String text = "nom_key;nom_fr;nom_en;definition_fr;definition_en\n"
                + "Projet 1;Projet 1;Projet;Projet manche;Channel project";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        ArgumentCaptor<Projet> ts = ArgumentCaptor.forClass(Projet.class);
        when(m.projetDAO.getByCode(anyString())).thenReturn(Optional.empty());
        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.projetDAO).saveOrUpdate(ts.capture());
        Assert.assertEquals("projet_1", ts.getValue().getName());
        Assert.assertEquals("projet_1", ts.getValue().getCode());
        Assert.assertEquals("Projet manche", ts.getValue().getDescriptionProjet());
        // with existing db
        Mockito.when(m.projetDAO.getByCode("projet_1")).thenReturn(Optional.of(m.projet));
        ts = ArgumentCaptor.forClass(Projet.class);
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.projetDAO, Mockito.times(2)).saveOrUpdate(ts.capture());
        Assert.assertEquals(m.projet, ts.getValue());
        Mockito.verify(m.projet).setDescriptionProjet("Projet manche");

    }

    /**
     * Test of setTypeSiteDAO method, of class Recorder.
     */
    @Test
    public void testSetEspeceDAO() {
        instance = Mockito.spy(new Recorder());
        instance.setProjetDAO(m.projetDAO);
        Mockito.verify(instance).setProjetDAO(m.projetDAO);
    }

}

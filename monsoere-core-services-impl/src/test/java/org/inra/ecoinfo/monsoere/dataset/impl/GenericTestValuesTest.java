/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.monsoere.dataset.impl;

import com.Ostermiller.util.CSVParser;
import java.io.StringReader;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Map;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.monsoere.test.utils.MockUtils;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

/**
 *
 * @author ptcherniati
 */
public class GenericTestValuesTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    MockUtils                          m;
    @Spy
    GenericTestValues                  instance     = new GenericTestValues();
    @Mock
    IDatatypeVariableUniteDAO          datatypeUniteVariableDAO;
    @Mock
    Map<String, DatatypeVariableUnite> variablesTypeDonnees;

    @Mock
    DatatypeVariableUnite              datatypeUniteVariable;

    String                             datatypeName = "datatype";

    /**
     *
     */
    public GenericTestValuesTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        m = MockUtils.getInstance();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of checkDateTypeValue method, of class GenericTestValues.
     */
    @Test
    public void testCheckDateTypeValue() throws DateTimeParseException {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Msg :");
        long lineNumber = 1L;
        int index = 2;
        String value = "25/12/2012";
        Column column = new Column();
        String format = "dd/MM/yyyy";
        column.setFormatType(format);
        column.setName("colonne");
        LocalDate expResult = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, value).toLocalDate();
        LocalDate result = instance.checkDateTypeValue(badsFormatsReport, lineNumber, index, value,
                column);
        Assert.assertEquals(expResult, result);
        Assert.assertFalse(badsFormatsReport.hasErrors());

        value = "25 décembre2012";
        result = instance.checkDateTypeValue(badsFormatsReport, lineNumber, index, value, column);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertTrue(1 == badsFormatsReport.getErrorsMessages().size());
        Assert.assertEquals(
                "Msg : :- \"25 décembre2012\" n'est pas un format de date valide à la ligne 1 colonne 3 (colonne). La date doit-être au format \"dd/MM/yyyy\" ",
                badsFormatsReport.getMessages());
    }

    /**
     * Test of checkFloatTypeValue method, of class GenericTestValues.
     */
    @Test
    public void testCheckFloatTypeValue() {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Msg :");
        long lineNumber = 1L;
        int index = 1;
        String value = "1.04";
        Column column = new Column();
        column.setName("colonne");
        Float expResult = 1.04F;
        Float result = instance.checkFloatTypeValue(badsFormatsReport, lineNumber, index, value,
                column);
        Assert.assertEquals(expResult, result);
        Assert.assertFalse(badsFormatsReport.hasErrors());
        value = "cinquante";
        result = instance.checkFloatTypeValue(badsFormatsReport, lineNumber, index, value, column);
        Assert.assertNull(result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Msg : :- Une valeur flottante est attendue à la ligne 1, colonne 2 (colonne): la valeur actuelle est \"cinquante\" ",
                badsFormatsReport.getMessages());
        column.setFlagType("type");
        result = instance.checkFloatTypeValue(badsFormatsReport, lineNumber, index, "12", column);
        Assert.assertTrue(12L == result);
        Assert.assertTrue(badsFormatsReport.hasErrors());
        Assert.assertEquals(
                "Msg : :- Une valeur flottante est attendue à la ligne 1, colonne 2 (colonne): la valeur actuelle est \"cinquante\" ",
                badsFormatsReport.getMessages());
    }

    /**
     * Test of checkValue method, of class GenericTestValues.
     */
    @Test
    public void testCheckValue() throws DateTimeParseException {
        GenericTestValues instance = Mockito.spy(new GenericTestValues());
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Err :");
        LocalDate date = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, "25/12/2014").toLocalDate();
        long lineNumber = 1L;
        int index = 1;
        String value = "valeur";
        Column column = new Column();
        Mockito.doReturn(10.2F).when(instance)
                .checkFloatTypeValue(badsFormatsReport, lineNumber, index, value, column);
        Mockito.doReturn(date).when(instance)
                .checkDateTypeValue(badsFormatsReport, lineNumber, index, value, column);
        // case PROPERTY_CST_FLOAT_TYPE
        column.setValueType(RecorderMonSoere.PROPERTY_CST_FLOAT_TYPE);
        instance.checkValue(badsFormatsReport, m.versionFile, lineNumber, index, value, column);
        Mockito.verify(instance, Mockito.times(1)).checkFloatTypeValue(badsFormatsReport,
                lineNumber, index, value, column);
        // case PROPERTY_CST_DATE_TYPE
        column.setFlag(true);
        column.setValueType(RecorderMonSoere.PROPERTY_CST_DATE_TYPE);
        instance.checkValue(badsFormatsReport, m.versionFile, lineNumber, index, value, column);
        Mockito.verify(instance, Mockito.times(1)).checkDateTypeValue(badsFormatsReport,
                lineNumber, index, value, column);
        // case "other"
        column.setFlag(false);
        column.setValueType("other");
        instance.checkValue(badsFormatsReport, m.versionFile, lineNumber, index, value, column);
        Mockito.verify(instance, Mockito.times(1)).checkOtherTypeValue(badsFormatsReport,
                lineNumber, index, value, column);
        Assert.assertFalse(badsFormatsReport.hasErrors());

    }

    /**
     * Test of cleanValue method, of class GenericTestValues.
     */
    @Test
    public void testCleanValue() {
        String value = "12,5";
        String expResult = "12.5";
        String result = instance.cleanValue(value);
        Assert.assertEquals(expResult, result);
        value = "12 5,0";
        expResult = "125.0";
        result = instance.cleanValue(value);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of testValues method, of class GenericTestValues.
     * @throws java.lang.Exception
     */
    @Test
    public void testTestValues() throws Exception {
        long startline = 1L;
        String text = "ligne 1;coucou\nligne 2;bonjour\n\nligne 3;fin";
        CSVParser parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        VersionFile versionFile = m.versionFile;
        String encoding = "UTF-8";
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("error");
        instance.testValues(startline, parser, versionFile, encoding, badsFormatsReport,
                m.datasetDescriptor, datatypeName);
        Mockito.verify(m.datasetDescriptor, Mockito.times(1)).getColumns();
        Mockito.verify(m.columns, Mockito.times(3)).get(0);
        Mockito.verify(m.columns, Mockito.times(3)).get(1);
        text = "ligne 1";
        parser = new CSVParser(new StringReader(text));
        parser.changeDelimiter(';');
        instance.testValues(startline, parser, versionFile, encoding, badsFormatsReport,
                m.datasetDescriptor, datatypeName);
        Mockito.verify(m.datasetDescriptor, Mockito.times(2)).getColumns();
        Mockito.verify(m.columns, Mockito.times(4)).get(0);
        Mockito.verify(m.columns, Mockito.times(3)).get(1);

    }

}

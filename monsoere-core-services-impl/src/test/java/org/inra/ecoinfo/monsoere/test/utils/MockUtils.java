package org.inra.ecoinfo.monsoere.test.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationDAO;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.localization.entity.UserLocale;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.mga.business.IMgaBuilder;
import org.inra.ecoinfo.mga.business.IMgaServiceBuilder;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.monsoere.dataset.impl.RecorderMonSoere;
import org.inra.ecoinfo.monsoere.refdata.espece.Espece;
import org.inra.ecoinfo.monsoere.refdata.espece.IEspeceDAO;
import org.inra.ecoinfo.monsoere.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.monsoere.refdata.projet.Projet;
import org.inra.ecoinfo.monsoere.refdata.projetsitethemedatatype.IProjetSiteThemeDatatypeDAO;
import org.inra.ecoinfo.monsoere.refdata.site.ISiteMonSoereDAO;
import org.inra.ecoinfo.monsoere.refdata.site.SiteMonSoere;
import org.inra.ecoinfo.monsoere.refdata.typesite.ITypeSiteDAO;
import org.inra.ecoinfo.monsoere.refdata.typesite.TypeSite;
import org.inra.ecoinfo.monsoere.refdata.variable.VariableMonSoere;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO;
import org.inra.ecoinfo.refdata.theme.IThemeDAO;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.refdata.unite.IUniteDAO;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.slf4j.LoggerFactory;

/**
 *
 *
 *
 * @author ptcherniati
 */
public class MockUtils {

    /**
     *
     */
    public static String DATE_DEBUT = "01/01/2012";

    /**
     *
     */
    public static String DATE_FIN = "31/12/2012";

    /**
     *
     */
    public static String DATE_DEBUT_COMPLETE = "01/01/2012 00:00";

    /**
     *
     */
    public static String DATE_FIN_COMPLETE = "31/12/2012 00:00";

    /**
     *
     */
    public static String TYPE_SITE = "typesite";

    /**
     *
     */
    public static String SITE_CODE = "site";

    /**
     *
     */
    public static String SITE_NOM = "Site";

    /**
     *
     */
    public static Long SITE_ID = 1L;

    /**
     *
     */
    public static String ESPECE = "espece";

    /**
     *
     */
    public static String PROJET = "projet";

    /**
     *
     */
    public static String THEME = "theme";

    /**
     *
     */
    public static String DATATYPE = "datatype";

    /**
     *
     */
    public static String DATATYPE_NOM = "Datatype";

    /**
     *
     */
    public static String VARIABLE_CODE = "variablenom";

    /**
     *
     */
    public static String VARIABLE_NOM = "variableNom";

    /**
     *
     */
    public static String VARIABLE_AFFICHAGE = "variableAffichage";

    /**
     *
     */
    public static String UNITE = "unite";

    /**
     *
     */
    public static String TRAITEMENT = "traitement";

    /**
     *
     */
    public static String TRAITEMENT_AFFICHAGE = "Traitement";
    /**
     *
     */
    public static String SITE_PATH = String.format("%s/%s",
            MockUtils.TYPE_SITE,
            MockUtils.SITE_CODE);
    /**
     *
     */
    public static ILocalizationManager localizationManager;

    private static final String UTILISATEUR = "utilisateur";

    static {
    }

    /**
     *
     * @return
     */
    public static MockUtils getInstance() {

        MockUtils instance = new MockUtils();
        MockitoAnnotations.initMocks(instance);

        try {

            instance.initMocks();

        } catch (DateTimeParseException ex) {

            return null;

        }

        MockUtils.initLocalization();
        when(instance.localizationDAO.getByNKey(anyString(), anyString(), anyString(), anyString()))
                .thenReturn(Optional.empty());
        return instance;

    }

    /**
     *
     */
    public static void initLocalization() {
        Localization.setDefaultLocalisation("fr");

        Localization.setLocalisations(Arrays.asList(new String[]{"fr", "en"}));

        MockUtils.localizationManager = new SpringLocalizationManager();
        ((SpringLocalizationManager) MockUtils.localizationManager).setUserLocale(new UserLocale());
        RecorderMonSoere.setStaticLocalizationManager(MockUtils.localizationManager);
    }

    /**
     *
     */
    @Mock
    public VersionFile versionFile;

    /**
     *
     */
    @Mock
    public IVersionFileDAO versionFileDAO;

    /**
     *
     */
    @Mock
    public Dataset dataset;

    /**
     *
     */
    @Mock
    public Espece espece;

    /**
     *
     */
    @Mock
    public IEspeceDAO especeDAO;

    /**
     *
     */
    @Mock
    public IMgaServiceBuilder mgaServiceBuilder;

    /**
     *
     */
    @Mock
    public Projet projet;

    /**
     *
     */
    @Mock
    public NodeDataSet projetNode;

    /**
     *
     */
    @Mock
    public NodeDataSet projetSiteNode;

    /**
     *
     */
    @Mock
    public NodeDataSet projetSiteThemeNode;

    /**
     *
     */
    @Mock
    public NodeDataSet projetSiteThemeDatatypeNode;

    /**
     *
     */
    @Mock
    public IProjetSiteThemeDatatypeDAO projetSiteThemeDatatypeDAO;

    /**
     *
     */
    @Mock
    public IProjetDAO projetDAO;

    /**
     *
     */
    @Mock
    public INode siteThemeDatatypeNode;

    /**
     *
     */
    @Mock
    public INode siteThemeNode;

    /**
     *
     */
    @Mock
    public TypeSite typeSite;

    /**
     *
     */
    @Mock
    public ITypeSiteDAO typeSiteDAO;

    /**
     *
     */
    @Mock
    public SiteMonSoere site;

    /**
     *
     */
    @Mock
    public ISiteMonSoereDAO siteDAO;

    /**
     *
     */
    @Mock
    public Theme theme;

    /**
     *
     */
    @Mock
    public IThemeDAO themeDAO;

    /**
     *
     */
    @Mock
    public DataType datatype;

    /**
     *
     */
    @Mock
    public IDatatypeDAO datatypeDAO;

    /**
     *
     */
    @Mock
    public Utilisateur utilisateur;

    /**
     *
     */
    @Mock
    public VariableMonSoere variable;

    /**
     *
     */
    @Mock
    public IVariableDAO variableDAO;

    /**
     *
     */
    @Mock
    public Unite unite;

    /**
     *
     */
    @Mock
    public IUniteDAO uniteDAO;

    /**
     *
     */
    @Mock
    public DatatypeVariableUnite datatypeVariableUnite;

    /**
     *
     */
    @Mock
    public List<DatatypeVariableUnite> datatypeVariableUnites;

    /**
     *
     */
    @Mock
    public IDatatypeVariableUniteDAO datatypeVariableUniteDAO;

    /**
     *
     */
    @Mock
    public DatasetDescriptor datasetDescriptor;

    /**
     *
     */
    @Mock
    public Column column;

    /**
     *
     */
    @Mock
    public List<Column> columns;

    /**
     *
     */
    public LocalDateTime dateDebutLocal, dateFinLocal, dateDebutCompleteLocal,  dateFinCompleteLocal;

    /**
     *
     */
    @Mock
    public IDatasetConfiguration datasetConfiguration;

    /**
     *
     */
    @Mock
    public ILocalizationDAO localizationDAO;

    /**
     *
     */
    @Mock
    public IMgaBuilder MgaBuilder;

    /**
     *
     */
    @Mock
    public IMgaIOConfigurator mgaIOConfigurator;

    /**
     *
     */
    @Mock
    public IMgaRecorder recorder;

    /**
     *
     */
    @Mock
    public IMgaIOConfiguration configuration;

    /**
     *
     */
    @Mock
    public IPolicyManager policyManager;

    /**
     *
     */
    public MockUtils() {
        MockitoAnnotations.initMocks(this);
    }

    private Map<Long, VersionFile> getVersions() {

        Map<Long, VersionFile> versions = new HashMap();

        versions.put(1L, versionFile);

        return versions;

    }

    private void initDAOs() throws PersistenceException {

        when(mgaServiceBuilder.getMgaBuilder()).thenReturn(MgaBuilder);
        when(mgaServiceBuilder.getMgaIOConfigurator()).thenReturn(mgaIOConfigurator);
        when(mgaServiceBuilder.getRecorder()).thenReturn(recorder);
        when(mgaIOConfigurator.getConfiguration(anyInt())).thenReturn(Optional.ofNullable(configuration));
        when(configuration.getEntryOrder()).thenReturn(new Integer[]{0, 1, 2, 3});

        // daos
        Mockito.when(siteDAO.getByPath(MockUtils.SITE_PATH)).thenReturn(Optional.of(site));

        Mockito.when(siteDAO.getByPath(MockUtils.SITE_PATH)).thenReturn(Optional.of(site));

        Mockito.when(siteDAO.getByCodeAndParent(MockUtils.SITE_CODE, null)).thenReturn(Optional.of(site));

        Mockito.when(typeSiteDAO.getTypeSiteByCode(MockUtils.TYPE_SITE)).thenReturn(Optional.of(typeSite));

        Mockito.when(typeSiteDAO.getTypeSiteByCode(MockUtils.TYPE_SITE)).thenReturn(Optional.of(typeSite));

        Mockito.when(datatypeDAO.getByCode(MockUtils.DATATYPE)).thenReturn(Optional.of(datatype));

        Mockito.when(variableDAO.getByCode(MockUtils.VARIABLE_CODE)).thenReturn(Optional.of(variable));

        Mockito.when(datatypeVariableUniteDAO.getByDatatype(MockUtils.DATATYPE)).thenReturn(
                datatypeVariableUnites);

        Mockito.when(
                datatypeVariableUniteDAO.getByNKey(MockUtils.DATATYPE, MockUtils.VARIABLE_CODE,
                        MockUtils.UNITE)).thenReturn(Optional.of(datatypeVariableUnite));

        Mockito.when(uniteDAO.getByCode(MockUtils.UNITE)).thenReturn(Optional.of(unite));

        Mockito.when(especeDAO.getEspeceByCode(MockUtils.ESPECE)).thenReturn(Optional.of(espece));
        Mockito.when(variableDAO.getByCode(MockUtils.VARIABLE_CODE)).thenReturn(Optional.of(variable));

        Mockito.when(projetDAO.getByCode(MockUtils.PROJET)).thenReturn(Optional.of(projet));

    }

    private void initDataset() throws DateTimeParseException {
        // init user
        Mockito.when(utilisateur.getLogin()).thenReturn(MockUtils.UTILISATEUR);

        // init dataset
        Mockito.when(dataset.getDateDebutPeriode()).thenReturn(dateDebutLocal);

        Mockito.when(dataset.getDateFinPeriode()).thenReturn(dateFinLocal);

        Mockito.when(dataset.getVersions()).thenReturn(getVersions());

        Mockito.when(dataset.getLastVersion()).thenReturn(versionFile);

        Mockito.when(dataset.getVersions()).thenReturn(getVersions());

//        Mockito.when(dataset.getNodeDataset()).thenReturn(siteThemeDatatype);
        // init version
        Mockito.when(versionFile.getDataset()).thenReturn(dataset);

        Mockito.when(versionFile.getVersionNumber()).thenReturn(1L);

        Mockito.when(versionFile.getUploadUser()).thenReturn(utilisateur);

        Mockito.when(dataset.buildDownloadFilename(datasetConfiguration)).thenReturn(
                String.format("%s_%s_%s_%s.csv", MockUtils.SITE_PATH.replaceAll("/", "_"),
                        MockUtils.DATATYPE,
                        DateUtil.getUTCDateTextFromLocalDateTime(dateDebutLocal, DateUtil.DD_MM_YYYY_FILE),
                        DateUtil.getUTCDateTextFromLocalDateTime(dateFinLocal, DateUtil.DD_MM_YYYY_FILE)));

    }

    private void initDatasetDescriptor() {

        Mockito.when(datasetDescriptor.getColumns()).thenReturn(columns);

        Mockito.when(column.getName()).thenReturn("colonne1", "colonne2", "colonne3", "colonne4",
                "colonne5", "colonne6", "colonne7", "colonne8", "colonne9", "colonne10", null);

        Mockito.when(columns.size()).thenReturn(10);

        Mockito.when(datasetDescriptor.getUndefinedColumn()).thenReturn(2);

        for (int i = 0; i < 10; i++) {

            Mockito.when(columns.get(i)).thenReturn(column);

        }

    }

    private void initDates() throws DateTimeParseException {

        dateDebutLocal = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, MockUtils.DATE_DEBUT);

        dateFinLocal = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, MockUtils.DATE_FIN);
        dateDebutCompleteLocal = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM, MockUtils.DATE_DEBUT_COMPLETE);

        dateFinCompleteLocal = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM, MockUtils.DATE_FIN_COMPLETE);

    }

    private void initEntities() {

        // init site
        Mockito.when(site.getPath()).thenReturn(MockUtils.SITE_PATH);

        Mockito.when(site.getCode()).thenReturn(MockUtils.SITE_CODE);

        Mockito.when(site.getName()).thenReturn(MockUtils.SITE_NOM);

        Mockito.when(site.getId()).thenReturn(MockUtils.SITE_ID);

        Mockito.when(site.getTypeSite()).thenReturn(typeSite);
        Mockito.when(site.getNodeableType()).thenCallRealMethod();

        // init theme
        Mockito.when(theme.getCode()).thenReturn(MockUtils.THEME);
        Mockito.when(theme.getNodeableType()).thenCallRealMethod();

        // init datatype
        Mockito.when(datatype.getCode()).thenReturn(MockUtils.DATATYPE);
        Mockito.when(datatype.getNodeableType()).thenCallRealMethod();

        // init agroecosystem
        Mockito.when(typeSite.getCode()).thenReturn(MockUtils.TYPE_SITE);

        // init ast
        Mockito.when(projetNode.getNodeable()).thenReturn(projet);
        Mockito.when(projetSiteNode.getParent()).thenReturn(projetNode);
        Mockito.when(projetSiteNode.getNodeable()).thenReturn(site);
        Mockito.when(projetSiteThemeNode.getParent()).thenReturn(projetSiteNode);
        Mockito.when(projetSiteThemeNode.getNodeable()).thenReturn(theme);
        Mockito.when(projetSiteThemeDatatypeNode.getParent()).thenReturn(projetSiteThemeNode);
        Mockito.when(projetSiteThemeDatatypeNode.getNodeable()).thenReturn(datatype);
        when(projetSiteThemeDatatypeNode.getParent()).thenReturn(projetSiteThemeNode);
        when(projetSiteThemeNode.getParent()).thenReturn(projetSiteNode);
        when(projetSiteNode.getParent()).thenReturn(projetNode);

        // init variable
        Mockito.when(variable.getCode()).thenReturn(MockUtils.VARIABLE_CODE);

        Mockito.when(variable.getName()).thenReturn(MockUtils.VARIABLE_NOM);

        Mockito.when(variable.getAffichage()).thenReturn(MockUtils.VARIABLE_AFFICHAGE);

        // init unite
        Mockito.when(unite.getCode()).thenReturn(MockUtils.UNITE);
        // espece
        Mockito.when(espece.getCode()).thenReturn(MockUtils.ESPECE);
        Mockito.when(espece.getNom()).thenReturn(MockUtils.ESPECE);
        // projet
        Mockito.when(projet.getCode()).thenReturn(MockUtils.PROJET);
        Mockito.when(projet.getName()).thenReturn(MockUtils.PROJET);
        Mockito.when(projet.getNodeableType()).thenCallRealMethod();

    }

    /**
     *
     * @throws DateTimeParseException
     */
    public void initMocks() throws DateTimeParseException {

        try {

            initDates();

            initEntities();

            initDAOs();

            initDatasetDescriptor();

            initDataset();

        } catch (PersistenceException ex) {

            LoggerFactory.getLogger(MockUtils.class.getName()).error(ex.getMessage(), ex);

        }

    }
}

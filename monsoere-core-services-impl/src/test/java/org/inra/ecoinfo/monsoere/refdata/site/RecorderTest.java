/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.monsoere.refdata.site;

import com.Ostermiller.util.CSVParser;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.monsoere.refdata.typesite.TypeSite;
import org.inra.ecoinfo.monsoere.test.utils.MockUtils;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author ptcherniati
 */
public class RecorderTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    MockUtils  m        = MockUtils.getInstance();
    Recorder   instance;
    String     encoding = "UTF-8";

    @Mock
    Properties propertiesNomFR;

    @Mock
    Properties propertiesNomEN;

    @Mock
    Properties propertiesDefinitionEN;

    /**
     *
     */
    public RecorderTest() {
    }

    /**
     *
     * @throws PersistenceException
     */
    @Before
    public void setUp() throws PersistenceException {
        m = MockUtils.getInstance();
        instance = new Recorder();
        MockitoAnnotations.initMocks(this);
        instance.setSiteMonSoereDAO(m.siteDAO);
        instance.setTypeSiteDAO(m.typeSiteDAO);
        ((SpringLocalizationManager) MockUtils.localizationManager)
                .setLocalizationDAO(m.localizationDAO);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.setMgaServiceBuilder(m.mgaServiceBuilder);
        instance.setPolicyManager(m.policyManager);
        
        Mockito.when(m.localizationDAO.getByNKey(anyString(), anyString(), anyString(), anyString()))
                .thenReturn(Optional.empty());
        Mockito.when(m.localizationDAO.newProperties(Nodeable.getLocalisationEntite(SiteMonSoere.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRANCE))
                .thenReturn(propertiesNomFR);
        Mockito.when(m.localizationDAO.newProperties(Nodeable.getLocalisationEntite(SiteMonSoere.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH))
                .thenReturn(propertiesNomEN);
        Mockito.when(
                m.localizationDAO
                        .newProperties(Nodeable.getLocalisationEntite(SiteMonSoere.class), "description", Locale.ENGLISH))
                .thenReturn(propertiesDefinitionEN);
        Mockito.when(propertiesNomFR.getProperty(MockUtils.SITE_NOM)).thenReturn("sitefr");
        Mockito.when(propertiesNomEN.getProperty(MockUtils.SITE_NOM)).thenReturn("siteen");
        Mockito.when(m.site.getDescription()).thenReturn("description");
        Mockito.when(propertiesDefinitionEN.getProperty("description")).thenReturn("descriptionen");
        Mockito.when(m.siteDAO.getByNameAndParent(MockUtils.SITE_CODE, null)).thenReturn(Optional.of(m.site));
        Mockito.when(m.typeSite.getNom()).thenReturn(MockUtils.TYPE_SITE);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecord method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testDeleteRecord() throws Exception {
        String text = "typesite;site;site;site;Bassin versant d'Oir;Oir catchment;";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        instance.deleteRecord(parser, file, encoding);
        Mockito.verify(m.siteDAO).remove(m.site);
        // bad type site code
        text = "typesite;badsite;site;site;Bassin versant d'Oir;Oir catchment;";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.when(m.siteDAO.getByNameAndParent("badsite", null)).thenThrow(
                new PersistenceException("error"));
        BusinessException error = null;
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("error", error.getMessage());
        error = null;
        // peristence exception
        text = "typesite;site;site;site;Bassin versant d'Oir;Oir catchment;";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.doThrow(new PersistenceException("error2")).when(m.siteDAO).remove(m.site);
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("error2", error.getMessage());
    }

    /**
     * Test of getAllElements method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllElements() throws Exception {
        final List<SiteMonSoere> sites = Arrays.asList(new SiteMonSoere[] { m.site });
        Mockito.when(m.siteDAO.getAllSitesMonSoere()).thenReturn(sites);
        List<SiteMonSoere> result = instance.getAllElements();
        Mockito.verify(m.siteDAO).getAllSitesMonSoere();
        Assert.assertTrue(1 == result.size());
        Assert.assertEquals(sites.get(0), result.get(0));
    }

    /**
     * Test of getNewLineModelGridMetadata method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNewLineModelGridMetadata() throws Exception {
        final List<Site> sites = Arrays.asList(new Site[] { m.site });
        final List<TypeSite> typeSites = Arrays.asList(new TypeSite[] { m.typeSite });
        Mockito.when(m.siteDAO.getAll()).thenReturn(sites);
        Mockito.when(m.typeSiteDAO.getAll()).thenReturn(typeSites);
        instance.initModelGridMetadata();
        LineModelGridMetadata result = instance.getNewLineModelGridMetadata(m.site);
        Assert.assertTrue(7 == result.getColumnsModelGridMetadatas().size());
        Assert.assertEquals("typesite", result.getColumnModelGridMetadataAt(0).getValue());
        Assert.assertEquals("Site", result.getColumnModelGridMetadataAt(1).getValue());
        Assert.assertEquals("sitefr", result.getColumnModelGridMetadataAt(2).getValue());
        Assert.assertEquals("siteen", result.getColumnModelGridMetadataAt(3).getValue());
        Assert.assertEquals("description", result.getColumnModelGridMetadataAt(4).getValue());
        Assert.assertEquals("descriptionen", result.getColumnModelGridMetadataAt(5).getValue());
        Assert.assertEquals("", result.getColumnModelGridMetadataAt(6).getValue());
    }

    /**
     * Test of initModelGridMetadata method, of class Recorder.
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testInitModelGridMetadata() throws PersistenceException {
        MockUtils.localizationManager = Mockito.spy(MockUtils.localizationManager);
        Mockito.when(m.typeSite.getNom()).thenReturn(MockUtils.SITE_NOM);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.initModelGridMetadata();
        Mockito.verify(m.typeSiteDAO).getAll();
        Mockito.verify(m.localizationDAO)
                .newProperties(Nodeable.getLocalisationEntite(SiteMonSoere.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        Mockito.verify(m.localizationDAO).newProperties(Nodeable.getLocalisationEntite(SiteMonSoere.class), "description",
                Locale.ENGLISH);
    }

    /**
     * Test of processRecord method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        SiteMonSoere siteParent = Mockito.mock(SiteMonSoere.class);
        Mockito.when(siteParent.getPath()).thenReturn("parent");
        Mockito.when(siteParent.getParent()).thenReturn(null);
        Mockito.when(siteParent.getDisplayPath()).thenReturn("parent");
        Mockito.when(m.site.getDisplayPath()).thenReturn("parent/Site");
        // Mockito.when(m.siteDAO.getByPath("parent/site")).thenReturn(m.site);
        Mockito.when(m.siteDAO.getByPath("parent")).thenReturn(Optional.of(siteParent));
        String text = "tze_type_nom;zet_nom_fr;zet_nom_en;zet_description_fr;zet_description_en;zet_chemin_parent\n"
                + "typesite;Site;sitefr;siteen;Bassin versant d'Oir;Oir catchment;parent";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        Mockito.when(m.siteDAO.getByPath("parent/site")).thenReturn(Optional.empty());
        ArgumentCaptor<SiteMonSoere> ts = ArgumentCaptor.forClass(SiteMonSoere.class);
        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.siteDAO).saveOrUpdate(ts.capture());
        Assert.assertEquals(m.typeSite, ts.getValue().getTypeSite());
        Assert.assertEquals(MockUtils.SITE_NOM, ts.getValue().getName());
        Assert.assertEquals("parent/Site", ts.getValue().getCode());
        Assert.assertEquals(siteParent, ts.getValue().getParent());
        Assert.assertEquals("Bassin versant d'Oir", ts.getValue().getDescription());
        // with existing db
        Mockito.when(m.siteDAO.getByPath("parent/site")).thenReturn(Optional.of(m.site));
        ts = ArgumentCaptor.forClass(SiteMonSoere.class);
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.siteDAO, Mockito.times(2)).saveOrUpdate(ts.capture());
        Assert.assertEquals(m.site, ts.getValue());
        Mockito.verify(m.site).setDescription("Bassin versant d'Oir");
        Mockito.verify(m.site).setName(MockUtils.SITE_NOM);
        Mockito.verify(m.site).setTypeSite(m.typeSite);
        Mockito.verify(m.site).setParent(siteParent);
        // with bad type site
        Mockito.when(m.typeSiteDAO.getTypeSiteByCode(MockUtils.TYPE_SITE)).thenReturn(Optional.empty());
        ts = ArgumentCaptor.forClass(SiteMonSoere.class);
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        try {
            instance.processRecord(parser, file, encoding);
            Assert.fail("exception expected");
        } catch (BusinessException e) {
            Assert.assertEquals(
                    ".Ligne 1, colonne 1, le type de site typesite n'est pas référencé.\n",
                    e.getMessage());
        }
        // with bad parent
        Mockito.when(m.typeSiteDAO.getTypeSiteByCode(MockUtils.TYPE_SITE)).thenReturn(Optional.of(m.typeSite));
        Mockito.when(m.siteDAO.getByPath("parent")).thenReturn(Optional.empty());
        ts = ArgumentCaptor.forClass(SiteMonSoere.class);
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        try {
            instance.processRecord(parser, file, encoding);
            Assert.fail("exception expected");
        } catch (BusinessException e) {
            Assert.assertEquals(".Ligne 1, colonne 8, le site parent n'est pas référencé.\n",
                    e.getMessage());
        }

    }

    /**
     * Test of setTypeSiteDAO method, of class Recorder.
     */
    @Test
    public void testSetTypeSiteDAO() {
        instance = Mockito.spy(new Recorder());
        instance.setTypeSiteDAO(m.typeSiteDAO);
        Mockito.verify(instance).setTypeSiteDAO(m.typeSiteDAO);
    }

    /**
     * Test of setTypeSiteDAO method, of class Recorder.
     */
    @Test
    public void testSiteMonSoereDAO() {
        instance = Mockito.spy(new Recorder());
        instance.setSiteMonSoereDAO(m.siteDAO);
        Mockito.verify(instance).setSiteMonSoereDAO(m.siteDAO);
    }

}

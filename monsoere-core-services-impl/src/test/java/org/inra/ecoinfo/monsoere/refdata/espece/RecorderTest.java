/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.monsoere.refdata.espece;

import com.Ostermiller.util.CSVParser;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.monsoere.test.utils.MockUtils;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author ptcherniati
 */
public class RecorderTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    MockUtils  m        = MockUtils.getInstance();
    Recorder   instance;
    String     encoding = "UTF-8";

    @Mock
    Properties propertiesNomEN;

    @Mock
    Properties propertiesDefinitionEN;

    /**
     *
     */
    public RecorderTest() {
    }

    /**
     *
     * @throws PersistenceException
     */
    @Before
    public void setUp() throws PersistenceException {
        m = MockUtils.getInstance();
        instance = new Recorder();
        MockitoAnnotations.initMocks(this);
        instance.setEspeceDAO(m.especeDAO);
        ((SpringLocalizationManager) MockUtils.localizationManager).setLocalizationDAO(m.localizationDAO);
        instance.setLocalizationManager(MockUtils.localizationManager);
        Mockito.when(
                m.localizationDAO.newProperties(Espece.NAME_ENTITY_JPA, "esp_definition",
                        Locale.ENGLISH)).thenReturn(propertiesDefinitionEN);
        Mockito.when(propertiesDefinitionEN.getProperty("Définition")).thenReturn("descriptionen");
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecord method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteRecord() throws Exception {
        String text = "Espèce;def_fr;def_en";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        instance.deleteRecord(parser, file, encoding);
        Mockito.verify(m.especeDAO, Mockito.times(1)).remove(m.espece);
        // bad type site code
        text = "Espècebad;def_fr;def_en";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        BusinessException error = null;
        Mockito.when(m.especeDAO.getEspeceByCode(anyString())).thenReturn(Optional.empty());
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("can't get espece", error.getMessage());
        error = null;
        // peristence exception
        text = "Espèce;def_fr;def_en";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.doThrow(new PersistenceException("error2")).when(m.especeDAO).remove(m.espece);
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("can't get espece", error.getMessage());
    }

    /**
     * Test of getAllElements method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllElements() throws Exception {
        final List<Espece> especes = Arrays.asList(new Espece[] { m.espece });
        Mockito.when(m.especeDAO.getAll()).thenReturn(especes);
        List<Espece> result = instance.getAllElements();
        Mockito.verify(m.especeDAO).getAll();
        Assert.assertTrue(1 == result.size());
        Assert.assertEquals(especes.get(0), result.get(0));
    }

    /**
     * Test of getNewLineModelGridMetadata method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNewLineModelGridMetadata() throws Exception {
        final List<Espece> especes = Arrays.asList(new Espece[] { m.espece });
        Mockito.when(m.especeDAO.getAll()).thenReturn(especes);
        Mockito.when(m.espece.getDefinition()).thenReturn("Définition");
        instance.initModelGridMetadata();
        LineModelGridMetadata result = instance.getNewLineModelGridMetadata(m.espece);
        Assert.assertTrue(3 == result.getColumnsModelGridMetadatas().size());
        Assert.assertEquals("espece", result.getColumnModelGridMetadataAt(0).getValue());
        Assert.assertEquals("Définition", result.getColumnModelGridMetadataAt(1).getValue());
        Assert.assertEquals("descriptionen", result.getColumnModelGridMetadataAt(2).getValue());
    }

    /**
     * Test of initModelGridMetadata method, of class Recorder.
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testInitModelGridMetadata() throws PersistenceException {
        MockUtils.localizationManager = Mockito.spy(MockUtils.localizationManager);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.initModelGridMetadata();
        Mockito.verify(m.localizationDAO).newProperties(Espece.NAME_ENTITY_JPA, "esp_definition",
                Locale.ENGLISH);
    }

    /**
     * Test of processRecord method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        Espece espece = Mockito.mock(Espece.class);
        Mockito.when(espece.getCode()).thenReturn("espece_1");
        Mockito.when(espece.getNom()).thenReturn("Espèce 1");
        Mockito.when(espece.getDefinition()).thenReturn("Définition");
        // Mockito.when(m.siteDAO.getByPath("parent/site")).thenReturn(m.site);
        String text = "esp_nom;esp_definition_fr;esp_definition_en\n"
                + "Espèce 1;Définition;definition_en";
        Mockito.when(m.especeDAO.getEspeceByCode(anyString())).thenReturn(Optional.empty());
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        ArgumentCaptor<Espece> ts = ArgumentCaptor.forClass(Espece.class);
        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.especeDAO).saveOrUpdate(ts.capture());
        Assert.assertEquals("Espèce 1", ts.getValue().getNom());
        Assert.assertEquals("espece_1", ts.getValue().getCode());
        Assert.assertEquals("Définition", ts.getValue().getDefinition());
        // with existing db
        Mockito.when(m.especeDAO.getEspeceByCode("espece_1")).thenReturn(Optional.of(m.espece));
        ts = ArgumentCaptor.forClass(Espece.class);
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.especeDAO, Mockito.times(2)).saveOrUpdate(ts.capture());
        Assert.assertEquals(m.espece, ts.getValue());
        Mockito.verify(m.espece).setDescription("Définition");

    }

    /**
     * Test of setTypeSiteDAO method, of class Recorder.
     */
    @Test
    public void testSetEspeceDAO() {
        instance = Mockito.spy(new Recorder());
        instance.setEspeceDAO(m.especeDAO);
        Mockito.verify(instance).setEspeceDAO(m.especeDAO);
    }

}

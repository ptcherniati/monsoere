/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.monsoere.dataset.impl;

import org.inra.ecoinfo.monsoere.test.utils.MockUtils;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class VariableValueTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    MockUtils m = MockUtils.getInstance();

    /**
     *
     */
    public VariableValueTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getDatatypeVariableUnite method, of class VariableValue.
     */
    @Test
    public void testGetDatatypeVariableUnite() {
        String value = "12";
        VariableValue instance = new VariableValue(m.datatypeVariableUnite, value);
        DatatypeVariableUnite result = instance.getDatatypeVariableUnite();
        Assert.assertEquals(m.datatypeVariableUnite, result);
    }

    /**
     * Test of getValue method, of class VariableValue.
     */
    @Test
    public void testGetValue() {
        String value = "12";
        VariableValue instance = new VariableValue(m.datatypeVariableUnite, value);
        String result = instance.getValue();
        Assert.assertEquals(value, result);
    }

    /**
     * Test of setDatatypeVariableUnite method, of class VariableValue.
     */
    @Test
    public void testSetDatatypeVariableUnite() {
        VariableValue instance = new VariableValue(null, null);
        instance.setDatatypeVariableUnite(m.datatypeVariableUnite);
        Assert.assertEquals(m.datatypeVariableUnite, instance.getDatatypeVariableUnite());
    }

    /**
     * Test of setValue method, of class VariableValue.
     */
    @Test
    public void testSetValue() {
        VariableValue instance = new VariableValue(null, null);
        String value = "12";
        instance.setValue(value);
        Assert.assertEquals(value, instance.getValue());
    }

}

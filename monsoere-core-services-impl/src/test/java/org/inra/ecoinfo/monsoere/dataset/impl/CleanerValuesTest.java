/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.monsoere.dataset.impl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class CleanerValuesTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    public CleanerValuesTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of currentToken method, of class CleanerValues.
     */
    @Test
    public void testCurrentToken() {
        String[] values = new String[] { "a", "2,3 ", " bonjour les enfants " };
        CleanerValues instance = new CleanerValues(values);
        Assert.assertEquals("a", instance.currentToken());
        instance.nextToken();
        Assert.assertEquals("2,3", instance.currentToken());
        instance.nextToken();
        Assert.assertEquals("bonjour les enfants", instance.currentToken());
    }

    /**
     * Test of currentTokenIndex method, of class CleanerValues.
     */
    @Test
    public void testCurrentTokenIndex() {
        String[] values = new String[] { "a", "2,3 ", " bonjour les enfants " };
        CleanerValues instance = new CleanerValues(values);
        Assert.assertTrue(0 == instance.currentTokenIndex());
        instance.nextToken();
        Assert.assertTrue(1 == instance.currentTokenIndex());
        instance.nextToken();
        Assert.assertTrue(2 == instance.currentTokenIndex());
    }

    /**
     * Test of nextToken method, of class CleanerValues.
     */
    @Test
    public void testNextToken() {
        String[] values = new String[] { "a", "2,3 ", " bonjour les enfants " };
        CleanerValues instance = new CleanerValues(values);
        Assert.assertEquals("a", instance.nextToken());
        Assert.assertEquals("2,3", instance.nextToken());
        Assert.assertEquals("bonjour les enfants", instance.nextToken());
    }

}

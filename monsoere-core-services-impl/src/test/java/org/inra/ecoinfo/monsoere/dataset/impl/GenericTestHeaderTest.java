/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.monsoere.dataset.impl;

import com.Ostermiller.util.CSVParser;
import java.io.StringReader;
import java.util.Arrays;
import java.util.List;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.monsoere.test.utils.MockUtils;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.slf4j.Logger;

/**
 *
 * @author ptcherniati
 */
public class GenericTestHeaderTest {


    static String[]                               EMPTY_LINE                  = new String[] {};
    static String[]                               LINE_WITH_EMPTY_ELEMENT     = new String[] { "",
            "", ""                                                           };

    static String[]                               LINE_WITH_NOT_EMPTY_ELEMENT = new String[] { "",
            "chien", ""                                                      };

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    GenericTestHeader                             instance;

    MockUtils                                     m;

    /**
     *
     */
    public GenericTestHeaderTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockUtils.initLocalization();
        m = MockUtils.getInstance();
        instance = new GenericTestHeader();
        instance.setLocalizationManager(MockUtils.localizationManager);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getLocalizationManager method, of class GenericTestHeader.
     */
    @Test
    public void testGetLocalizationManager() {
        ILocalizationManager result = instance.getLocalizationManager();
        Assert.assertEquals(MockUtils.localizationManager, result);
    }

    /**
     * Test of setLocalizationManager method, of class GenericTestHeader.
     */
    @Test
    public void testSetLocalizationManager() {
        Assert.assertEquals(MockUtils.localizationManager, instance.getLocalizationManager());
    }

    /**
     * Test of testHeaders method, of class GenericTestHeader.
     * @throws java.lang.Exception
     */
    @Test
    public void testTestHeaders() throws Exception {
        String text = "colonne1;colonne2;colonne3";
        instance = new GenericTestHeaderInstance();
        instance.setLocalizationManager(m.localizationManager);
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("Erreur1");
        CSVParser parser = new CSVParser(new StringReader(text),';');
        String encoding = "UTF-8";
        DatasetDescriptor datasetDescriptor = mock(DatasetDescriptor.class);
        Column column = mock(Column.class);
        List<Column> columns = Arrays.asList(new Column[]{column, column, column});
        when(datasetDescriptor.getColumns()).thenReturn(columns);
        when(column.getName()).thenReturn("colonne1","colonne2","colonne3");
        long expResult = 1L;
        long result = instance.testHeaders(parser, m.versionFile, encoding, badsFormatsReport,
                datasetDescriptor);
        Assert.assertEquals(expResult, result);
    }
    class GenericTestHeaderInstance extends GenericTestHeader{

        @Override
        protected byte[] sanitizeData(VersionFile versionFile, Logger logger, BadsFormatsReport badsFormatsReport) throws BadFormatException {
            return versionFile.getData();
        }
        
    }

}

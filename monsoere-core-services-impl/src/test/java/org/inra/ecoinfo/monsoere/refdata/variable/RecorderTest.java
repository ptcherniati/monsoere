/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.monsoere.refdata.variable;

import com.Ostermiller.util.CSVParser;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.monsoere.test.utils.MockUtils;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author ptcherniati
 */
public class RecorderTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    MockUtils  m        = MockUtils.getInstance();
    Recorder   instance;
    String     encoding = "UTF-8";

    @Mock
    Properties propertiesNomFR;

    @Mock
    Properties propertiesNomEN;

    @Mock
    Properties propertiesDefinitionEN;

    /**
     *
     */
    public RecorderTest() {
    }

    /**
     *
     * @throws PersistenceException
     */
    @Before
    public void setUp() throws PersistenceException {
        m = MockUtils.getInstance();
        instance = new Recorder();
        MockitoAnnotations.initMocks(this);
        instance.setVariableDAO(m.variableDAO);
        ((SpringLocalizationManager) MockUtils.localizationManager).setLocalizationDAO(m.localizationDAO);
        when(m.localizationDAO.getByNKey(anyString(), anyString(), anyString(), anyString()))
                .thenReturn(Optional.empty());
        instance.setLocalizationManager(MockUtils.localizationManager);
        Mockito.when(m.localizationDAO.newProperties(Nodeable.getLocalisationEntite(VariableMonSoere.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRANCE))
                .thenReturn(propertiesNomFR);
        Mockito.when(m.localizationDAO.newProperties(Nodeable.getLocalisationEntite(VariableMonSoere.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH))
                .thenReturn(propertiesNomEN);
        Mockito.when(m.localizationDAO.newProperties(Nodeable.getLocalisationEntite(VariableMonSoere.class), "definition", Locale.ENGLISH))
                .thenReturn(propertiesDefinitionEN);
        Mockito.when(propertiesNomFR.getProperty(MockUtils.VARIABLE_NOM)).thenReturn(
                "variableNomfr");
        Mockito.when(propertiesNomEN.getProperty(MockUtils.VARIABLE_NOM)).thenReturn(
                "variableNomen");
        Mockito.when(m.variable.getDefinition()).thenReturn("définition");
        Mockito.when(propertiesDefinitionEN.getProperty("définition")).thenReturn("définitionen");
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecord method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteRecord() throws Exception {
        String text = "variablenom;varable;variable;variable;false;";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        instance.deleteRecord(parser, file, encoding);
        Mockito.verify(m.variableDAO).remove(m.variable);
        // bad type site code
        text = "variablenombad;varable;variable;variable;false;";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        BusinessException error = null;
        when(m.variableDAO.getByCode("variablenombad")).thenReturn(Optional.empty());
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("can't get variable", error.getMessage());
        error = null;
        // peristence exception
        text = "variablenom;varable;variable;variable;false;";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.doThrow(new PersistenceException("error2")).when(m.variableDAO).remove(m.variable);
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("error2", error.getMessage());
    }

    /**
     * Test of getAllElements method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllElements() throws Exception {
        final List<Variable> variables = Arrays.asList(new Variable[] { m.variable });
        Mockito.when(m.variableDAO.getAll()).thenReturn(variables);
        List<VariableMonSoere> result = instance.getAllElements();
        Mockito.verify(m.variableDAO).getAll();
        Assert.assertTrue(1 == result.size());
        Assert.assertEquals(variables.get(0), result.get(0));
    }

    /**
     * Test of getNewLineModelGridMetadata method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetNewLineModelGridMetadata() throws Exception {
        final List<Variable> variables = Arrays.asList(new Variable[] { m.variable });
        Mockito.when(m.variableDAO.getAll()).thenReturn(variables);
        instance.initModelGridMetadata();
        LineModelGridMetadata result = instance.getNewLineModelGridMetadata(m.variable);
        Assert.assertTrue(6 == result.getColumnsModelGridMetadatas().size());
        Assert.assertEquals("variableNom", result.getColumnModelGridMetadataAt(0).getValue());
        Assert.assertEquals("variableNomfr", result.getColumnModelGridMetadataAt(1).getValue());
        Assert.assertEquals("variableNomen", result.getColumnModelGridMetadataAt(2).getValue());
        Assert.assertEquals("définition", result.getColumnModelGridMetadataAt(3).getValue());
        Assert.assertEquals("définitionen", result.getColumnModelGridMetadataAt(4).getValue());
        Assert.assertEquals(Boolean.FALSE.toString(), result.getColumnModelGridMetadataAt(5)
                .getValue());
    }

    /**
     * Test of initModelGridMetadata method, of class Recorder.
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testInitModelGridMetadata() throws PersistenceException {
        MockUtils.localizationManager = Mockito.spy(MockUtils.localizationManager);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.initModelGridMetadata();
        Mockito.verify(m.localizationDAO).newProperties(Nodeable.getLocalisationEntite(VariableMonSoere.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRANCE);
        Mockito.verify(m.localizationDAO).newProperties(Nodeable.getLocalisationEntite(VariableMonSoere.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        Mockito.verify(m.localizationDAO).newProperties(Nodeable.getLocalisationEntite(VariableMonSoere.class), "definition",
                Locale.ENGLISH);
    }

    /**
     * Test of processRecord method, of class Recorder.
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        // Mockito.when(m.siteDAO.getByPath("parent/site")).thenReturn(m.site);
        String text = "nom_key;nom_fr;nom_en;definition_fr;definition_en;isQualitative\n"
                + "variableNom;variableNom;Number of individuals;définition;Number of individuals;false";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        Mockito.when(m.variableDAO.getByCode(MockUtils.VARIABLE_CODE)).thenReturn(Optional.empty());
        ArgumentCaptor<VariableMonSoere> ts = ArgumentCaptor.forClass(VariableMonSoere.class);
        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.variableDAO).saveOrUpdate(ts.capture());
        Assert.assertEquals(MockUtils.VARIABLE_NOM, ts.getValue().getName());
        Assert.assertEquals(MockUtils.VARIABLE_NOM, ts.getValue().getCode());
        Assert.assertEquals(Boolean.FALSE, ts.getValue().getIsQualitative());
        Assert.assertEquals("définition", ts.getValue().getDefinition());
        // with existing db
        Mockito.when(m.variableDAO.getByCode(MockUtils.VARIABLE_CODE)).thenReturn(Optional.of(m.variable));
        ts = ArgumentCaptor.forClass(VariableMonSoere.class);
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.variableDAO, Mockito.times(2)).saveOrUpdate(ts.capture());
        Assert.assertEquals(m.variable, ts.getValue());
        Mockito.verify(m.variable).setDefinition("définition");
        Mockito.verify(m.variable).setIsQualitative(Boolean.FALSE);
    }

    /**
     * Test of setTypeSiteDAO method, of class Recorder.
     */
    @Test
    public void testSetVariableDAO() {
        instance = Mockito.spy(new Recorder());
        instance.setVariableDAO(m.variableDAO);
        Mockito.verify(instance).setVariableDAO(m.variableDAO);
    }

}

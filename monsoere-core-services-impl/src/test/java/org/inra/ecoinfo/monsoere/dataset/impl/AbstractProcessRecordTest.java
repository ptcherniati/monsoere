/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.monsoere.dataset.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.monsoere.refdata.variable.VariableMonSoere;
import org.inra.ecoinfo.monsoere.test.utils.MockUtils;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author ptcherniati
 */
public class AbstractProcessRecordTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    MockUtils                   m                      = MockUtils.getInstance();
    List<Column>                columns                = new LinkedList();
    AbstractProcessRecord       instance;
    DatasetDescriptor           datasetDescriptor;
    ErrorsReport                errorsReport           = new ErrorsReport("Error");

    List<String>                variablesHeaders       = new LinkedList();

    @Mock
    VariableMonSoere            variable1;

    @Mock
    VariableMonSoere            variable2;

    List<DatatypeVariableUnite> datatypeVariableUnites = Arrays.asList(new DatatypeVariableUnite[] { m.datatypeVariableUnite });

    /**
     *
     */
    public AbstractProcessRecordTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        instance = new AbstractProcessRecordImpl();
        instance.setVariableDAO(m.variableDAO);
        instance.setVersionFileDAO(m.versionFileDAO);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.setDatatypeVariableUniteDAO(m.datatypeVariableUniteDAO);
        datasetDescriptor = m.datasetDescriptor;
//        Mockito.when(variable1.getDatatypesUnitesVariables()).thenReturn(datatypeVariableUnites);
//        Mockito.when(variable2.getDatatypesUnitesVariables()).thenReturn(datatypeVariableUnites);
        Mockito.when(m.datatypeVariableUnite.getDatatype()).thenReturn(m.datatype);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of buildVariableHeaderAndSkipHeader method, of class AbstractProcessRecord.
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildVariableHeaderAndSkipHeader() throws Exception {
        variablesHeaders = Arrays.asList(new String[] { "variable1", "variable2" });
        Mockito.when(m.variableDAO.getByCode("variable1")).thenReturn(Optional.of(variable1));
        Mockito.when(m.variableDAO.getByCode("variable2")).thenReturn(Optional.of(variable2));
        DatatypeVariableUnite datatypeVariableUnites1 = mock(DatatypeVariableUnite.class);
        when(datatypeVariableUnites1.getVariable()).thenReturn(variable1);
        DatatypeVariableUnite datatypeVariableUnites2 = mock(DatatypeVariableUnite.class);
        when(datatypeVariableUnites2.getVariable()).thenReturn(variable2);
        when(variable1.getCode()).thenReturn("variable1");
        when(variable2.getCode()).thenReturn("variable2");
        CSVParser parser = new CSVParser(new StringReader("variable1,variable2"));
        parser.changeDelimiter(';');
        List<DatatypeVariableUnite> dvus = Arrays.asList(new DatatypeVariableUnite[]{datatypeVariableUnites1,datatypeVariableUnites2});
        when(m.datatypeVariableUniteDAO.getByDatatype(MockUtils.DATATYPE)).thenReturn(dvus);
        List<DatatypeVariableUnite> dbVariables = instance.buildVariableHeaderAndSkipHeader(
                MockUtils.DATATYPE, 1, errorsReport, parser);
        Assert.assertTrue(2 == dbVariables.size());
        Assert.assertEquals(datatypeVariableUnites1, dbVariables.get(0));
        Assert.assertEquals(datatypeVariableUnites2, dbVariables.get(1));
        Mockito.verify(m.variableDAO).getByCode("variable1");
        Mockito.verify(m.variableDAO).getByCode("variable2");
    }

    /**
     * Test of getLocalizationManager method, of class AbstractProcessRecord.
     */
    @Test
    public void testGetLocalizationManager() {
        Assert.assertEquals(MockUtils.localizationManager, instance.getLocalizationManager());
    }

    /**
     * Test of processRecord method, of class AbstractProcessRecord.
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        CSVParser parser = null;
        VersionFile versionFile = null;
        String fileEncoding = "";
        DatasetDescriptor datasetDescriptor = null;
        AbstractProcessRecord instance = new AbstractProcessRecordImpl();
        instance.processRecord(parser, versionFile, fileEncoding, datasetDescriptor);
    }

    /**
     * Test of setLocalizationManager method, of class AbstractProcessRecord.
     */
    @Test
    public void testSetLocalizationManager() {
        ILocalizationManager localizationManager = null;
        AbstractProcessRecord instance = new AbstractProcessRecordImpl();
        instance.setLocalizationManager(localizationManager);
    }

    /**
     * Test of setVariableDAO method, of class AbstractProcessRecord.
     */
    @Test
    public void testSetVariableDAO() {
        IVariableDAO variableDAO = null;
        AbstractProcessRecord instance = new AbstractProcessRecordImpl();
        instance.setVariableDAO(variableDAO);
    }

    /**
     * Test of setVersionFileDAO method, of class AbstractProcessRecord.
     */
    @Test
    public void testSetVersionFileDAO() {
        IVersionFileDAO versionFileDAO = null;
        AbstractProcessRecord instance = new AbstractProcessRecordImpl();
        instance.setVersionFileDAO(versionFileDAO);
    }

    /**
     * Test of skipHeaderAndRetrieveVariableName method, of class AbstractProcessRecord.
     * @throws java.lang.Exception
     */
    @Test
    public void testSkipHeaderAndRetrieveVariableName() throws Exception {
        int variableColumnIndex = 0;
        CSVParser parser = null;
        AbstractProcessRecord instance = new AbstractProcessRecordImpl();
        List<String> expResult = new LinkedList();
        List<String> result = instance.skipHeaderAndRetrieveVariableName(variableColumnIndex,
                parser);
        Assert.assertEquals(expResult, result);
    }

    /**
     *
     */
    public class AbstractProcessRecordImpl extends AbstractProcessRecord {
        
        /**
         *
         */
        private static final long serialVersionUID = 1L;
        
        @Override
        public DatasetDescriptor lookupDatasetDescriptor(String datasetDescriptorPath)
                throws IOException {
            return m.datasetDescriptor;
        }
        
        /**
         *
         * @param parser
         * @param versionFile
         * @param fileEncoding
         * @param datasetDescriptor
         * @throws BusinessException
         */
        @Override
        public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding,
                DatasetDescriptor datasetDescriptor) throws BusinessException {
        }
        
        @Override
        protected List<String> skipHeaderAndRetrieveVariableName(final int variableColumnIndex,
                final CSVParser parser) throws IOException {
            return variablesHeaders;
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.monsoere.dataset.impl;

import org.inra.ecoinfo.monsoere.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.monsoere.test.utils.MockUtils;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author ptcherniati
 */
public class DeleteRecordTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    MockUtils            m = MockUtils.getInstance();

    DeleteRecord         instance;

    @Mock
    ILocalPublicationDAO publicationDAO;

    /**
     *
     */
    public DeleteRecordTest() {
    }

    /**
     *
     * @throws PersistenceException
     */
    @Before
    public void setUp() throws PersistenceException {
        MockitoAnnotations.initMocks(this);
        instance = new DeleteRecord();
        instance.setPublicationDAO(publicationDAO);
        instance.setVersionFileDAO(m.versionFileDAO);
        Mockito.when(m.versionFileDAO.merge(m.versionFile)).thenReturn(m.versionFile);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteRecord method, of class DeleteRecord.
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteRecord() throws Exception {
        instance.deleteRecord(m.versionFile);
        Mockito.verify(m.versionFileDAO).merge(m.versionFile);
        Mockito.verify(publicationDAO).removeVersion(m.versionFile);
    }

}

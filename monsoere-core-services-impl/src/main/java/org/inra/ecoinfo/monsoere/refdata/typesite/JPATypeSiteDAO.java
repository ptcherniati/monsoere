/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.monsoere.refdata.typesite;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * The Class JPATypeSiteDAO.
 *
 * @author philippe
 * @author "Guillaume Enrico"
 */
public class JPATypeSiteDAO extends AbstractJPADAO<TypeSite> implements ITypeSiteDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.monsoere.refdata.typesite.ITypeSiteDAO#getAll()
     */
    @Override
    public List<TypeSite> getAll() {
        return getAll(TypeSite.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.monsoere.refdata.typesite.ITypeSiteDAO#getTypeSiteByCode(java.lang.String)
     */
    @Override
    public Optional<TypeSite> getTypeSiteByCode(final String code) {
        CriteriaQuery<TypeSite> query = builder.createQuery(TypeSite.class);
        Root<TypeSite> ts = query.from(TypeSite.class);
        query
                .select(ts)
                .where(builder.equal(ts.get(TypeSite_.code), code));
        return getOptional(query);
    }
}

package org.inra.ecoinfo.monsoere.extraction;

import java.io.Serializable;
import org.inra.ecoinfo.monsoere.refdata.espece.Espece;

/**
 * The Class EspeceVO.
 */
public class EspeceVO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The id.
     */
    private Long              id;
    /**
     * The code.
     */
    private String            code;
    /**
     * The nom.
     */
    private String            nom;
    /**
     * The is data available.
     */
    private Boolean           isDataAvailable  = false;

    /**
     * Instantiates a new espece vo.
     *
     * @param espece
     *            the espece
     */
    public EspeceVO(Espece espece) {
        this.id = espece.getId();
        this.code = espece.getCode();
        this.nom = espece.getNom();
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the checks if is data available.
     *
     * @return the checks if is data available
     */
    public Boolean getIsDataAvailable() {
        return isDataAvailable;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Sets the checks if is data available.
     *
     * @param isDataAvailable
     *            the new checks if is data available
     */
    public void setIsDataAvailable(Boolean isDataAvailable) {
        this.isDataAvailable = isDataAvailable;
    }

    /**
     * Sets the nom.
     *
     * @param nom
     *            the new nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
}

package org.inra.ecoinfo.monsoere.extraction;

import java.io.Serializable;
import org.inra.ecoinfo.monsoere.refdata.projet.Projet;

/**
 * The Class ProjetVO.
 */
public class ProjetVO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The id.
     */
    private Long              id;
    /**
     * The code.
     */
    private String            code;

    /**
     * Instantiates a new projet vo.
     *
     * @param projet
     *            the projet
     */
    public ProjetVO(Projet projet) {
        this.id = projet.getId();
        this.code = projet.getCode();
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }
}

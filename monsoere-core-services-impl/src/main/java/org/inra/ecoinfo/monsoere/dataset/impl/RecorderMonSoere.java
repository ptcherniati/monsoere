package org.inra.ecoinfo.monsoere.dataset.impl;

import com.Ostermiller.util.BadDelimiterException;
import com.Ostermiller.util.CSVParser;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import org.inra.ecoinfo.dataset.AbstractRecorder;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.monsoere.dataset.IDeleteRecord;
import org.inra.ecoinfo.monsoere.dataset.IProcessRecord;
import org.inra.ecoinfo.monsoere.dataset.IRecorderMonSoere;
import org.inra.ecoinfo.monsoere.dataset.ITestFormat;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class RecorderMonSoere.
 * <p>
 * This class is used to delete, test, upload and publish files
 *
 * @author Enrico Guillaume
 */
public class RecorderMonSoere extends AbstractRecorder implements IRecorderMonSoere {

    /**
     *
     */
    public static final String MONSOERE_DATASET_BUNDLE_NAME = "org.inra.ecoinfo.monsoere.dataset.messages";
    /**
     * The Constant CST_STRING_EMPTY.
     */
    public static final String CST_STRING_EMPTY = "";
    /**
     * The Constant CST_SPACE.
     */
    public static final String CST_SPACE = " ";
    /**
     * The Constant CST_DOT.
     */
    public static final String CST_DOT = ".";
    /**
     * The Constant CST_COMMA.
     */
    public static final String CST_COMMA = ",";
    // dataset-descripors_types
    /**
     * The Constant PROPERTY_CST_DATE_TYPE.
     */
    public static final String PROPERTY_CST_DATE_TYPE = "date";
    /**
     * The Constant PROPERTY_CST_INTEGER_TYPE.
     */
    public static final String PROPERTY_CST_INTEGER_TYPE = "integer";
    /**
     * The Constant PROPERTY_CST_FLOAT_TYPE.
     */
    public static final String PROPERTY_CST_FLOAT_TYPE = "float";
    /**
     * The Constant PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS.
     */
    public static final String PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";
    /**
     * The Constant PROPERTY_MSG_NO_DATA.
     */
    public static final String PROPERTY_MSG_NO_DATA = "PROPERTY_MSG_NO_DATA";
    /**
     * The Constant PROPERTY_MSG_INVALID_FLOAT_VALUE.
     */
    public static final String PROPERTY_MSG_INVALID_FLOAT_VALUE = "PROPERTY_MSG_INVALID_FLOAT_VALUE";
    /**
     * The Constant PROPERTY_MSG_INVALID_DATE.
     */
    public static final String PROPERTY_MSG_INVALID_DATE = "PROPERTY_MSG_INVALID_DATE";
    /**
     * The Constant PROPERTY_MSG_ERROR.
     */
    public static final String PROPERTY_MSG_ERROR = "PROPERTY_MSG_ERROR";

    /**
     * The Constant PROPERTY_MSG_DUPLICATE_SEQUENCE.
     */
    public static final String PROPERTY_MSG_DUPLICATE_SEQUENCE = "PROPERTY_MSG_DUPLICATE_SEQUENCE";

    /**
     * The Constant PROPERTY_MSG_VALUE_EXPECTED.
     */
    public static final String PROPERTY_MSG_VALUE_EXPECTED = "PROPERTY_MSG_VALUE_EXPECTED";

    /**
     * The Constant PROPERTY_MSG_ERROR_BAD_FORMAT.
     */
    public static final String PROPERTY_MSG_ERROR_BAD_FORMAT = "PROPERTY_MSG_ERROR_BAD_FORMAT";

    /**
     *
     */
    public static final String PROPERTY_MSG_MISSING_TYPE_SITE = "PROPERTY_MSG_MISSING_TYPE_SITE";

    /**
     *
     */
    public static final String PROPERTY_MSG_INVALID_TYPE_SITE = "PROPERTY_MSG_INVALID_TYPE_SITE";

    /**
     *
     */
    public static final String PROPERTY_MSG_MISSING_SITE = "PROPERTY_MSG_MISSING_SITE";

    /**
     *
     */
    public static final String PROPERTY_MSG_INVALID_SITE = "PROPERTY_MSG_INVALID_SITE";
    /**
     * The localization manager @link(ILocalizationManager).
     */
    private static volatile ILocalizationManager localizationManager;
    /**
     * Gets the monSoere message.
     *
     * @param key the key
     * @return the MONSOERE message from key {@link String} the key
     */
    public static final String getMonSoereMessage(final String key) {
        return RecorderMonSoere.getMonSoereMessageWithBundle(
                RecorderMonSoere.MONSOERE_DATASET_BUNDLE_NAME, key);
    }
    /**
     * Gets the monSoere message.
     *
     * @param bundlePath the bundle path
     * @param key the key
     * @return the MONSOERE message from key
     * @link(String) the bundle path {@link String} the key
     */
    public static final String getMonSoereMessageWithBundle(final String bundlePath,
            final String key) {
        return RecorderMonSoere.localizationManager == null ? key
                : RecorderMonSoere.localizationManager.getMessage(bundlePath, key);
    }

    /**
     *
     * @param localizationManager2
     */
    public static void setStaticLocalizationManager(ILocalizationManager localizationManager2) {
        RecorderMonSoere.localizationManager = localizationManager2;
    }
    /**
     * The delete record @link(IDeleteRecord).
     */
    private IDeleteRecord deleteRecord;
    /**
     * The test format @link(ITestFormat).
     */
    private ITestFormat testFormat;

    /**
     * The process record @link(IProcessRecord).
     */
    private IProcessRecord processRecord;

    /**
     * The dataset descriptor path @link(String).
     */
    private String datasetDescriptorPath;


    /**
     * Delete records.
     *
     * @param versionFile the version file
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     * @see org.inra.ecoinfo.dataset.IRecorder#deleteRecords(org.inra.ecoinfo.dataset
     *      .versioning.entity.VersionFile)
     */
    @Override
    public void deleteRecords(final VersionFile versionFile) throws BusinessException {
        deleteRecord.deleteRecord(versionFile);
    }

    /**
     * Process record.
     *
     * @param parser the parser
     * @param versionFile the version file
     * @param fileEncoding the file encoding
     * @throws BusinessException the business exception
     * @link(CSVParser) the parser
     * @link(VersionFile) the version file
     * @link(String) the file encoding
     * @see org.inra.ecoinfo.dataset.impl.AbstractRecorder#processRecord(com.Ostermiller
     *      .util.CSVParser, org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     *      java.lang.String) does nothing because of overload record ()
     */
    @Override
    protected void processRecord(final CSVParser parser, final VersionFile versionFile,
            final String fileEncoding) throws BusinessException {
    }

    /**
     * <p>
     * process the record of the versionFile.
     *
     * @param parser the parser
     * @param versionFile the version file
     * @param fileEncoding the file encoding
     * @param datasetDescriptorMonSoere the dataset descriptor mon soere
     * @throws BusinessException the business exception {@link VersionFile} {@link ISessionPropertiesMonSoere}
     *             {@link DatasetDescriptorMonSoere} the dataset descriptor acbb
     */
    private void processRecord(final CSVParser parser, final VersionFile versionFile,
            final String fileEncoding, final DatasetDescriptor datasetDescriptorMonSoere)
            throws BusinessException {
        processRecord.processRecord(parser, versionFile, fileEncoding, datasetDescriptorMonSoere);
    }

    /**
     * Record.
     *
     * @param versionFile the version file
     * @param fileEncoding the file encoding
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     * @link(String) the file encoding
     * @see org.inra.ecoinfo.dataset.impl.AbstractRecorder#record(org.inra.ecoinfo
     *      .dataset.versioning.entity.VersionFile, java.lang.String) test the format and process
     *      the record of the version
     */
    @Override
    public void record(final VersionFile versionFile, final String fileEncoding)
            throws BusinessException {
        try {
            byte[] datasSanitized = versionFile.getData();
            CSVParser parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(
                    datasSanitized), fileEncoding), AbstractRecorder.SEPARATOR);
            final DatasetDescriptor datasetDescriptorMonSoere = processRecord
                    .lookupDatasetDescriptor(datasetDescriptorPath);
            testFormat(parser, versionFile, fileEncoding, datasetDescriptorMonSoere);
            parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(
                    versionFile.getData()), fileEncoding), AbstractRecorder.SEPARATOR);
            processRecord(parser, versionFile, fileEncoding, datasetDescriptorMonSoere);
        } catch (final BadDelimiterException | IOException | BadFormatException ex) {
            throw new BusinessException(ex.getMessage(), ex);
        }
    }

    /**
     * Sets the dataset descriptor path.
     *
     * @param datasetDescriptorPath the new dataset descriptor path
     */
    public void setDatasetDescriptorPath(final String datasetDescriptorPath) {
        this.datasetDescriptorPath = datasetDescriptorPath;
    }

    /**
     * Sets the delete record.
     *
     * @param deleteRecord the new delete record
     * @see
     * org.inra.ecoinfo.monsoere.dataset.IRecorderMonSoere#setDeleteRecord(org.inra.
     * ecoinfo.acbb.dataset.IDeleteRecord)
     */
    @Override
    public void setDeleteRecord(final IDeleteRecord deleteRecord) {
        this.deleteRecord = deleteRecord;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     * @see
     * org.inra.ecoinfo.dataset.impl.AbstractRecorder#setLocalizationManager(org.inra.ecoinfo.localization.ILocalizationManager)
     */
    @Override
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        RecorderMonSoere.setStaticLocalizationManager(localizationManager);
        super.setLocalizationManager(localizationManager);
    }

    /**
     * Sets the process record.
     *
     * @param processRecord the new process record
     * @see
     * org.inra.ecoinfo.monsoere.dataset.IRecorderMonSoere#setProcessRecord(org.inra
     * .ecoinfo.acbb.dataset.IProcessRecord)
     */
    @Override
    public void setProcessRecord(final IProcessRecord processRecord) {
        this.processRecord = processRecord;
    }

    /**
     * Sets the test format.
     *
     * @param testFormat the new test format
     * @see
     * org.inra.ecoinfo.monsoere.dataset.IRecorderMonSoere#setTestFormat(org.inra.ecoinfo
     * .acbb.dataset.ITestFormat)
     */
    @Override
    public void setTestFormat(final ITestFormat testFormat) {
        this.testFormat = testFormat;
    }

    /**
     * Sets the version file dao.
     *
     * @param versionFileDAO the new version file dao {@link IVersionFileDAO}
     * the new version file dao
     */
    @Override
    public void setVersionFileDAO(final IVersionFileDAO versionFileDAO) {
        LOGGER.info("unused");
    }

    /**
     * Test format.
     *
     * @param parser the parser
     * @param versionFile the version file
     * @param encoding the encoding
     * @param datasetDescriptor the dataset descriptor
     * @throws BadFormatException the bad format exception
     * @throws BusinessException call test the format of a version file
     * @link(CSVParser) the parser {@link VersionFile} {@link ISessionPropertiesMonSoere}
     * @link(String) the encoding {@link DatasetDescriptorMonSoere}
     */
    private void testFormat(final CSVParser parser, final VersionFile versionFile,
            final String encoding, final DatasetDescriptor datasetDescriptor)
            throws BadFormatException, BusinessException {
        testFormat.testFormat(parser, versionFile, encoding, datasetDescriptor);
    }

    /**
     * Test format.
     *
     * @param versionFile the version file
     * @param encoding the encoding
     * @throws BusinessException the business exception
     * @link(VersionFile) the version file
     * @link(String) the encoding
     * @see org.inra.ecoinfo.dataset.impl.AbstractRecorder#testFormat(org.inra.ecoinfo
     *      .dataset.versioning.entity.VersionFile, java.lang.String)
     */
    @Override
    public void testFormat(final VersionFile versionFile, final String encoding)
            throws BusinessException {
        CSVParser parser = null;
        DatasetDescriptor datasetDescriptorMonSoere;
        byte[] datasSanitized = null;
        try {
            datasSanitized = versionFile.getData();
            parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(datasSanitized),
                    encoding), AbstractRecorder.SEPARATOR);
            datasetDescriptorMonSoere = processRecord
                    .lookupDatasetDescriptor(datasetDescriptorPath);
            testFormat(parser, versionFile, encoding, datasetDescriptorMonSoere);
        } catch (final BadDelimiterException | BadFormatException | IOException e) {
            LOGGER.debug("error in testFormat", e);
            throw new BusinessException(e.getMessage(), e);
        }
    }
}

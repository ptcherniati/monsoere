package org.inra.ecoinfo.monsoere.extraction;

import java.io.Serializable;
import org.inra.ecoinfo.monsoere.refdata.variable.VariableMonSoere;

/**
 * The Class VariableVO.
 */
public class VariableVO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The id.
     */
    private Long              id;
    /**
     * The nom.
     */
    private String            nom;
    /**
     * The code.
     */
    private String            code;
    /**
     * The affichage.
     */
    private String            affichage;
    /**
     * The ordre affichage groupe.
     */
    private Integer           ordreAffichageGroupe;
    /**
     * The is data available.
     */
    private Boolean           isDataAvailable  = false;

    /**
     * Instantiates a new variable vo.
     */
    public VariableVO() {
    }

    /**
     * Instantiates a new variable vo.
     *
     * @param variable
     *            the variable
     */
    public VariableVO(VariableMonSoere variable) {
        this.id = variable.getId();
        this.nom = variable.getName();
        this.code = variable.getCode();
        this.affichage = variable.getAffichage();
    }

    /**
     * Gets the affichage.
     *
     * @return the affichage
     */
    public String getAffichage() {
        return affichage;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the checks if is data available.
     *
     * @return the checks if is data available
     */
    public Boolean getIsDataAvailable() {
        return isDataAvailable;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Gets the ordre affichage groupe.
     *
     * @return the ordre affichage groupe
     */
    public Integer getOrdreAffichageGroupe() {
        return ordreAffichageGroupe;
    }

    /**
     * Sets the affichage.
     *
     * @param affichage
     *            the new affichage
     */
    public void setAffichage(String affichage) {
        this.affichage = affichage;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Sets the checks if is data available.
     *
     * @param isDataAvailable
     *            the new checks if is data available
     */
    public void setIsDataAvailable(Boolean isDataAvailable) {
        this.isDataAvailable = isDataAvailable;
    }

    /**
     * Sets the nom.
     *
     * @param nom
     *            the new nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Sets the ordre affichage groupe.
     *
     * @param ordreAffichageGroupe
     *            the new ordre affichage groupe
     */
    public void setOrdreAffichageGroupe(Integer ordreAffichageGroupe) {
        this.ordreAffichageGroupe = ordreAffichageGroupe;
    }
}

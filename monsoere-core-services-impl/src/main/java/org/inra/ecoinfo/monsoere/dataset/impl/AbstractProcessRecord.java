package org.inra.ecoinfo.monsoere.dataset.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.monsoere.dataset.IProcessRecord;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class AbstractProcessRecord.
 */
public abstract class AbstractProcessRecord implements IProcessRecord {


    /**
     * The Constant serialVersionUID.
     */
    private static final long      serialVersionUID = 1L;
    /**
     * The logger @link(Logger).
     */
    protected final Logger         LOGGER           = LoggerFactory.getLogger(this.getClass().getName());
    /**
     * The localization manager @link(ILocalizationManager).
     */
    protected ILocalizationManager localizationManager;
    /**
     * The variable dao.
     */
    protected IVariableDAO         variableDAO;
    /**
     * The version file dao.
     */
    protected IVersionFileDAO      versionFileDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteDAO datatypeVariableUniteDAO;

    /**
     * Instantiates a new abstract process record.
     */
    protected AbstractProcessRecord() {
        super();
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    /**
     * Builds the variables header and skip header.
     *
     * @param datatypeName
     *            the datatype name
     * @param variableHeaderIndex
     *            the variable header index
     * @param badsFormatsReport
     *            the bads formats report
     * @param parser
     *            the parser
     * @return the list
     * @throws PersistenceException
     *             the persistence exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws BusinessException
     *             the business exception
     * @link(DatasetDescriptor) the dataset descriptor
     */
    protected List<DatatypeVariableUnite> buildVariableHeaderAndSkipHeader(
            final String datatypeName, final int variableHeaderIndex,
            final ErrorsReport badsFormatsReport, final CSVParser parser)
            throws PersistenceException, IOException, BusinessException {

        final List<String> variablesHeaders = skipHeaderAndRetrieveVariableName(variableHeaderIndex, parser);
        final List<DatatypeVariableUnite> dbVariables = new LinkedList<>();
        for (final String variableHeader : variablesHeaders) {
            
            Optional<Variable> variableOpt = variableDAO.getByCode(Utils.createCodeFromString(variableHeader));
            
            
            if (!variableOpt.isPresent()) {
                badsFormatsReport
                        .addException(new PersistenceException(
                                String.format(
                                        RecorderMonSoere
                                                .getMonSoereMessage(RecorderMonSoere.PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS),
                                        variableHeader)));
                continue;
            }
            List<DatatypeVariableUnite> dvus = datatypeVariableUniteDAO.getByDatatype(datatypeName);
            for (final DatatypeVariableUnite datatypeVariableUnite : dvus) {
                if (datatypeVariableUnite.getVariable().getCode().equals(variableHeader)) {
                    dbVariables.add(datatypeVariableUnite);
                    break;
                }
            }
        }
        if (badsFormatsReport.hasErrors()) {
            throw new PersistenceException(badsFormatsReport.buildHTMLMessages());
        }
        return dbVariables;
    }

    /**
     * Gets the localization manager.
     *
     * @return the localization manager
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.monsoere.dataset.IProcessRecord#processRecord(com.Ostermiller.util.CSVParser
     * , org.inra.ecoinfo.dataset.versioning.entity.VersionFile, java.lang.String,
     * org.inra.ecoinfo.utils.DatasetDescriptor)
     */

    /**
     *
     * @param parser
     * @param versionFile
     * @param fileEncoding
     * @param datasetDescriptor
     * @throws BusinessException
     */

    @Override
    public abstract void processRecord(CSVParser parser, VersionFile versionFile,
            String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException;

    /**
     * Sets the localization manager.
     *
     * @param localizationManager
     *            the new localization manager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Sets the variable dao.
     *
     * @param variableDAO
     *            the new variable dao
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     * Sets the version file dao.
     *
     * @param versionFileDAO
     *            the new version file dao
     */
    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

    /**
     * Skip header and retrieve variable name.
     *
     * @param variableColumnIndex
     *            the variable column index
     * @param parser
     *            the parser
     * @return the list
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @link(int) the variable column index
     * @link(CSVParser) the parser
     */
    protected List<String> skipHeaderAndRetrieveVariableName(final int variableColumnIndex,
            final CSVParser parser) throws IOException {
        final String[] values = parser.getLine();
        final List<String> variablesNames = new LinkedList<>();
        for (int vci = variableColumnIndex; vci <= values.length; vci++) {
            variablesNames.add(Utils.createCodeFromString(values[vci - 1]));
        }
        return variablesNames;
    }
}

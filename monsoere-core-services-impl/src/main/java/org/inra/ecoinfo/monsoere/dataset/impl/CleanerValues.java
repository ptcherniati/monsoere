package org.inra.ecoinfo.monsoere.dataset.impl;

import java.util.Arrays;

/**
 * The Class CleanerValues.
 */
public class CleanerValues {

    /**
     * The value index @link(int).
     */
    private int      valueIndex = 0;
    /**
     * The values @link(String[]).
     */
    private final String[] values;

    /**
     * Instantiates a new cleaner values.
     *
     * @param val
     *            the val
     * @link(String[]) the val
     */
    public CleanerValues(final String[] val) {
        if (val == null) {
            this.values = new String[0];
        } else {
            this.values = Arrays.copyOf(val, val.length);
        }
    }

    /**
     * Current token.
     *
     * @return the string
     */
    public String currentToken() {
        return values[valueIndex].toLowerCase().trim();
    }

    /**
     * Current token index.
     *
     * @return the int
     */
    public int currentTokenIndex() {
        return valueIndex;
    }

    /**
     * Next token.
     *
     * @return the string
     */
    public String nextToken() {
        return values[valueIndex++].toLowerCase().trim();
    }
}

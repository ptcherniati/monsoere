/**
 * OREIForets project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.monsoere.refdata.espece;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class Recorder.
 *
 * @author philippe
 * @author "Guillaume Enrico"
 */
public class Recorder extends AbstractCSVMetadataRecorder<Espece> {

    /**
     * The espece dao.
     */
    protected IEspeceDAO especeDAO;
    /**
     * The properties description en.
     */
    private Properties   propertiesDescriptionEN;

    /**
     * Are espece equals.
     *
     * @param espece
     *            the espece
     * @param dbEspece
     *            the db espece
     * @return true, if successful
     */
    private boolean areEspeceEquals(Espece espece, Espece dbEspece) {
        return espece.getNom().equalsIgnoreCase(dbEspece.getNom())
                && espece.getDefinition().equalsIgnoreCase(dbEspece.getDefinition());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util
     * .CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final String code = Utils.createCodeFromString(values[0]);
                final Espece espece = especeDAO.getEspeceByCode(code)
                        .orElseThrow(() -> new BusinessException("can't get espece"));
                especeDAO.remove(espece);

                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Espece> getAllElements() throws BusinessException {
        return especeDAO.getAll();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Espece espece) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata
                .getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(
                        espece == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : espece.getNom(),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, true, false, true));
        lineModelGridMetadata
                .getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(
                        espece == null || espece.getDefinition() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : espece.getDefinition(),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, false, false, false));
        lineModelGridMetadata
                .getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(
                        espece == null || espece.getDefinition() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : propertiesDescriptionEN.getProperty(espece.getDefinition()),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<Espece> initModelGridMetadata() {
        propertiesDescriptionEN = localizationManager.newProperties(Espece.NAME_ENTITY_JPA,
                "esp_definition", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     * Process record.
     *
     * @param parser
     *            the parser
     * @param file
     *            the file
     * @param encoding
     *            the encoding
     * @throws BusinessException
     *             the business exception
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding)
            throws BusinessException {
        try {
            skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values,
                        Espece.NAME_ENTITY_JPA);
                // On parcourt chaque colonne d'une ligne
                final String nom = tokenizerValues.nextToken();
                Utils.createCodeFromString(nom);
                final String definition = tokenizerValues.nextToken();
                final Espece espece = new Espece(nom, definition);
                final Espece dbEspece = especeDAO.getEspeceByCode(espece.getCode()).orElse(null);
                if (dbEspece == null) {
                    especeDAO.saveOrUpdate(espece);
                } else if (!areEspeceEquals(espece, dbEspece)) {
                    dbEspece.setDescription(espece.getDefinition());
                    especeDAO.saveOrUpdate(dbEspece);
                }
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the espece dao.
     *
     * @param especeDAO
     *            the new espece dao
     */
    public void setEspeceDAO(IEspeceDAO especeDAO) {
        this.especeDAO = especeDAO;
    }
}

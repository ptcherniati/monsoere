package org.inra.ecoinfo.monsoere.dataset.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.monsoere.dataset.ITestValues;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.NullValueException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GenericTestValues.
 */
public class GenericTestValues implements ITestValues {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The logger.
     */
    protected final Logger    LOGGER           = LoggerFactory.getLogger(this.getClass().getName());

    /**
     * Instantiates a new generic test values.
     */
    public GenericTestValues() {
        super();
    }

    /**
     *
     * @param badsFormatsReport
     * @param lineNumber
     * @param index
     * @param value
     * @param column
     * @return
     */
    protected LocalDate checkDateTypeValue(final BadsFormatsReport badsFormatsReport,
            final long lineNumber, final int index, final String value, final Column column) {
        String format = Strings.isNullOrEmpty(column.getFormatType()) ? DateUtil.DD_MM_YYYY : column.getFormatType();
        try {
            final LocalDate date = DateUtil.readLocalDateTimeFromText(format, value).toLocalDate();
            if (!value.equals(DateUtil.getUTCDateTextFromLocalDateTime(date.atStartOfDay(), format))) {
                badsFormatsReport.addException(new NullValueException(String.format(
                        RecorderMonSoere
                                .getMonSoereMessage(RecorderMonSoere.PROPERTY_MSG_INVALID_DATE),
                        value, lineNumber, index + 1, column.getName(), format)));
                return null;
            }
            return date;
        } catch (final DateTimeException e) {
            badsFormatsReport.addException(new NullValueException(
                    String.format(RecorderMonSoere
                            .getMonSoereMessage(RecorderMonSoere.PROPERTY_MSG_INVALID_DATE), value,
                            lineNumber, index + 1, column.getName(), format)));
            LOGGER.info(badsFormatsReport.getMessages(), e);
            return null;
        }
    }

    /**
     * Check float type value.
     *
     * @param badsFormatsReport
     *            the bads formats report
     * @param lineNumber
     *            int the line number
     * @param index
     *            int the column index
     * @param value
     *            the value
     * @param column
     *            the column
     * @return the float {@link String[]} the values {@link BadsFormatsReport} the bads formats
     *         report {@link String} the value {@link Column}
     * @link(BadsFormatsReport) the bads formats report
     * @link(String) the value
     * @link(Column) the column
     */
    protected Float checkFloatTypeValue(final BadsFormatsReport badsFormatsReport,
            final long lineNumber, final int index, final String value, final Column column) {
        Float floatValue = null;
        try {
            floatValue = Float.parseFloat(value);
        } catch (final NumberFormatException e) {
            badsFormatsReport.addException(new BadValueTypeException(String.format(RecorderMonSoere
                    .getMonSoereMessage(RecorderMonSoere.PROPERTY_MSG_INVALID_FLOAT_VALUE),
                    lineNumber, index + 1, column.getName(), value)));
            LOGGER.info(badsFormatsReport.getMessages(), e);
        }
        return floatValue;
    }

    private void checkIsNullable(final BadsFormatsReport badsFormatsReport, final long lineNumber,
            final int i, String value, final Column column) {
        try {
            if (!column.isNullable() && (value == null || value.length() == 0)) {
                throw new NullValueException(String.format(RecorderMonSoere
                        .getMonSoereMessage(RecorderMonSoere.PROPERTY_MSG_VALUE_EXPECTED),
                        lineNumber, i + 1, column.getName()));
            }
        } catch (final NullValueException e) {
            badsFormatsReport.addException(e);
            LOGGER.info(badsFormatsReport.getMessages(), e);
        }
    }

    /**
     * Check other type value.
     *
     * @param badsFormatsReport
     *            the bads formats report
     * @param lineNumber
     *            int the line number
     * @param index
     *            the column index
     * @param value
     *            the value
     * @param column
     *            the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeUniteVariableMonSoere>) the variables types donnees
     */
    protected void checkOtherTypeValue(final BadsFormatsReport badsFormatsReport,
            final long lineNumber, final int index, final String value, final Column column) {
    }

    /**
     * Check value.
     *
     * @param badsFormatsReport
     *            the bads formats report
     * @param versionFile
     *            the version file
     * @param lineNumber
     *            int the line number
     * @param i
     *            the i
     * @param value
     *            the value
     * @param column
     *            the column
     * @link(BadsFormatsReport) the bads formats report
     * @link(String) the value
     * @link(Column) the column
     */
    protected void checkValue(final BadsFormatsReport badsFormatsReport,
            final VersionFile versionFile, final long lineNumber, final int i, String value,
            final Column column) {
        String cleanValue = value;
        checkIsNullable(badsFormatsReport, lineNumber, i, value, column);
        if (cleanValue != null) {
            cleanValue = cleanValue(cleanValue);
        }
        final String valueType = column.getValueType();
        if (valueType != null) {
            checkValueType(badsFormatsReport, lineNumber, i, column, cleanValue, valueType);
        }
    }

    private void checkValueType(final BadsFormatsReport badsFormatsReport, final long lineNumber,
            final int i, final Column column, String cleanValue, final String valueType) {
        assert valueType != null : "null valuetype";
        if (RecorderMonSoere.PROPERTY_CST_DATE_TYPE.equalsIgnoreCase(valueType)
                && cleanValue.length() > 0) {
            checkDateTypeValue(badsFormatsReport, lineNumber, i, cleanValue, column);
        } else if (RecorderMonSoere.PROPERTY_CST_FLOAT_TYPE.equalsIgnoreCase(valueType)
                && cleanValue.length() > 0) {
            checkFloatTypeValue(badsFormatsReport, lineNumber, i, cleanValue, column);
        } else {
            checkOtherTypeValue(badsFormatsReport, lineNumber, i, cleanValue, column);
        }
    }

    /**
     * Clean value.
     *
     * @param value
     *            the value
     * @return the string {@link String} the value
     * @link(String) the value
     */
    protected String cleanValue(final String value) {
        String returnValue = value;
        returnValue = returnValue.replaceAll(RecorderMonSoere.CST_COMMA, RecorderMonSoere.CST_DOT);
        returnValue = returnValue.replaceAll(RecorderMonSoere.CST_SPACE,
                RecorderMonSoere.CST_STRING_EMPTY);
        return returnValue;
    }

    /**
     *
     * @param startline
     * @param parser
     * @param versionFile
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @param datatypeName
     * @throws BusinessException
     */
    @Override
    public void testValues(long startline, CSVParser parser, VersionFile versionFile,
            String encoding, BadsFormatsReport badsFormatsReport,
            DatasetDescriptor datasetDescriptor, String datatypeName) throws BusinessException {
        long lineNumber = startline;
        final long headerCountLine = lineNumber;
        // On parcourt chaque ligne du fichier
        try {
            String[] values = parser.getLine();
            final List<Column> columns = datasetDescriptor.getColumns();
            while (values != null) {
                lineNumber++;
                // On parcourt chaque colonne d'une ligne
                for (int index = 0; index < values.length; index++) {
                    if (index > columns.size() - 1) {
                        break;
                    }
                    final String value = values[index];
                    final Column column = columns.get(index);
                    checkValue(badsFormatsReport, versionFile, lineNumber, index, value, column);
                }
                values = parser.getLine();
            }
        } catch (final IOException e) {
            badsFormatsReport.addException(e);
            LOGGER.info(badsFormatsReport.getMessages(), e);

        }
        if (lineNumber == headerCountLine) {
            badsFormatsReport.addException(new BusinessException(RecorderMonSoere
                    .getMonSoereMessage(RecorderMonSoere.PROPERTY_MSG_NO_DATA)));
        }
    }
}

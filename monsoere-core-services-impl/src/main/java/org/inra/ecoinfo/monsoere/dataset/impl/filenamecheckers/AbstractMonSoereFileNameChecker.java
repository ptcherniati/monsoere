package org.inra.ecoinfo.monsoere.dataset.impl.filenamecheckers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.monsoere.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.monsoere.refdata.projet.Projet;
import org.inra.ecoinfo.monsoere.refdata.site.SiteMonSoere;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class AbstractMonSoereFileNameChecker.
 */
public abstract class AbstractMonSoereFileNameChecker extends AbstractFileNameChecker {

    /**
     * The Constant INVALID_FILE_NAME.
     */
    protected static final String INVALID_FILE_NAME_MONSOERE      = "%s_%s_%s_%s_%s.csv";
    /**
     * The Constant PATTERN.
     */
    protected static final String PATTERN_MONSOERE                = "^(%s|.*?)_(%s|.*?)_(%s|.*?)_(.*)_(.*)\\.csv$";
    /**
     * The Constant PATTERN_FILE_NAME_PATH.
     */
    protected static final String PATTERN_FILE_NAME_PATH_MONSOERE = "%s_%s_%s_%s_%s#V%d#.csv";

    /**
     *
     */
    protected final static Logger LOGGER = LoggerFactory.getLogger(AbstractMonSoereFileNameChecker.class);
    /**
     * The projet dao.
     */
    protected IProjetDAO          projetDAO;

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker#isValidFileName(java
     * .lang.String, org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public boolean isValidFileName(final String fN, final VersionFile version)
            throws InvalidFileNameException {
        final String fileName = cleanFileName(fN);
        final String currentProject = version.getDataset().getRealNode().getNodeByNodeableTypeResource(Projet.class).getCode();
        final String currentSite = reWriteSitePathForFile(version
                .getDataset().getRealNode().getNodeByNodeableTypeResource(SiteMonSoere.class).getCode());
        final String currentDatatype = version.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getCode();
        final Matcher splitFilename = Pattern.compile(
                String.format(AbstractMonSoereFileNameChecker.PATTERN_MONSOERE, currentProject, currentSite,
                        currentDatatype)).matcher(fileName);
        testPath(currentProject, currentSite, currentDatatype, splitFilename);
        testDates(version, currentProject, currentSite, currentDatatype, splitFilename);
        return true;
    }

    /**
     * Re write site path for file.
     *
     * @param siteName
     *            the site name
     * @return the string
     */
    protected String reWriteSitePathForFile(String siteName) {
        String localSiteName = siteName;
        if (isConfigurationSeparatorDefined()) {
            localSiteName = localSiteName.replaceAll(
                    PatternConfigurator.ANCESTOR_SEPARATOR,
                    configuration.getSiteSeparatorForFileNames());
        }
        return localSiteName;
    }

    /**
     * Sets the projet dao.
     *
     * @param projetDAO
     *            the new projet dao
     */
    public void setProjetDAO(IProjetDAO projetDAO) {
        this.projetDAO = projetDAO;
    }

    /**
     * Test datatype.
     *
     * @param currentProject
     *            the current project
     * @param currentSite
     *            the current site
     * @param currentDatatype
     *            the current datatype
     * @param datatypeName
     *            the datatype name
     * @throws InvalidFileNameException
     *             the invalid file name exception
     */
    protected void testDatatype(String currentProject, String currentSite, String currentDatatype,
            String datatypeName) throws InvalidFileNameException {
        if (!datatypeDAO.getByCode(Utils.createCodeFromString(datatypeName)).isPresent()) {
            throw new InvalidFileNameException(String.format(
                    AbstractMonSoereFileNameChecker.INVALID_FILE_NAME_MONSOERE, currentProject,
                    currentSite, currentDatatype, getDatePattern(), getDatePattern()));
        }
    }

    /**
     * Test dates.
     *
     * @param version
     *            the version
     * @param currentProject
     *            the current project
     * @param currentSite
     *            the current site
     * @param currentDatatype
     *            the current datatype
     * @param splitFilename
     *            the split filename
     * @throws InvalidFileNameException
     *             the invalid file name exception
     */
    protected abstract void testDates(VersionFile version, String currentProject,
            String currentSite, String currentDatatype, Matcher splitFilename)
            throws InvalidFileNameException;

    /**
     * Test path.
     *
     * @param currentProject
     *            the current project
     * @param currentSite
     *            the current site
     * @param currentDatatype
     *            the current datatype
     * @param splitFilename
     *            the split filename
     * @throws InvalidFileNameException
     *             the invalid file name exception
     */
    protected void testPath(final String currentProject, final String currentSite,
            final String currentDatatype, final Matcher splitFilename)
            throws InvalidFileNameException {
        if (!splitFilename.find()) {
//            throw new InvalidFileNameException(String.format(
//                    AbstractMonSoereFileNameChecker.INVALID_FILE_NAME, currentProject, currentSite,
//                    currentDatatype, getDatePattern(), getDatePattern()));
        }
    }

    /**
     * Test project.
     *
     * @param currentProject
     *            the current project
     * @param currentSite
     *            the current site
     * @param currentDatatype
     *            the current datatype
     * @param projectName
     *            the project name
     * @throws InvalidFileNameException
     *             the invalid file name exception
     */
    protected void testProject(String currentProject, String currentSite, String currentDatatype,
            String projectName) throws InvalidFileNameException {
        if (!projetDAO.getByCode(Utils.createCodeFromString(projectName)).isPresent()) {
            throw new InvalidFileNameException(String.format(
                    AbstractMonSoereFileNameChecker.INVALID_FILE_NAME_MONSOERE, currentProject,
                    currentSite, currentDatatype, getDatePattern(), getDatePattern()));
        }
    }

    /**
     * Test site.
     *
     * @param version
     *            the version
     * @param currentProject
     *            the current project
     * @param currentSite
     *            the current site
     * @param currentDatatype
     *            the current datatype
     * @param siteName
     *            the site name
     * @throws InvalidFileNameException
     *             the invalid file name exception
     */
    protected void testSite(final VersionFile version, final String currentProject,
            final String currentSite, final String currentDatatype, final String siteName)
            throws InvalidFileNameException {
        if (!siteDAO.getByPath(Utils.createCodeFromString(siteName)).isPresent()
                || !( version.getDataset().getRealNode().getNodeByNodeableTypeResource(SiteMonSoere.class).getName().equals(siteName))) {
            throw new InvalidFileNameException(String.format(
                    AbstractMonSoereFileNameChecker.INVALID_FILE_NAME_MONSOERE, currentSite,
                    currentDatatype, getDatePattern(), getDatePattern(),
                    AbstractFileNameChecker.FILE_FORMAT));
        }
    }
}

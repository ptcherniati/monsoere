package org.inra.ecoinfo.monsoere.dataset.impl.filenamecheckers;

import com.google.common.base.Strings;
import java.util.regex.Matcher;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.monsoere.refdata.projet.Projet;
import org.inra.ecoinfo.monsoere.refdata.site.SiteMonSoere;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;

/**
 * The Class FileNameCheckeryyyymmdd.
 */
public class FileNameCheckeryyyymmdd extends AbstractMonSoereFileNameChecker {

    /**
     * The Constant DATE_PATTERN.
     */
    private static final String DATE_PATTERN = DateUtil.YYYY_MM_DD_FILE;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker#getDatePattern()
     */

    /**
     *
     * @return
     */

    @Override
    protected String getDatePattern() {
        return FileNameCheckeryyyymmdd.DATE_PATTERN;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.dataset.IFileNameChecker#getFilePath(org.inra.ecoinfo.dataset.versioning
     * .entity.VersionFile)
     */

    /**
     *
     * @param version
     * @return
     */

    @Override
    public String getFilePath(final VersionFile version) {
        final String currentProject = version.getDataset().getRealNode().getNodeByNodeableTypeResource(Projet.class).getCode();
        String currentSite = version.getDataset().getRealNode().getNodeByNodeableTypeResource(SiteMonSoere.class).getPath();
        if (configuration.getSiteSeparatorForFileNames() != null
                && !Strings.isNullOrEmpty(configuration.getSiteSeparatorForFileNames())) {
            currentSite = currentSite.replaceAll(
                    PatternConfigurator.ANCESTOR_SEPARATOR,
                    configuration.getSiteSeparatorForFileNames());
        }
        return String.format(
                AbstractMonSoereFileNameChecker.PATTERN_FILE_NAME_PATH,
                currentProject,
                currentSite,
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateDebutPeriode(), DateUtil.YYYY_MM_DD_FILE),
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateFinPeriode(), DateUtil.YYYY_MM_DD_FILE),
                version.getVersionNumber());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.dataset.IFileNameChecker#getFilePath(org.inra.ecoinfo.dataset.versioning
     * .entity.VersionFile)
     */

    /**
     *
     * @param dataset
     * @return
     */

    @Override
    public String getFilePath(final Dataset dataset) {
        final String currentProject = dataset.getRealNode().getNodeByNodeableTypeResource(Projet.class).getCode();
        String currentSite = dataset.getRealNode().getNodeByNodeableTypeResource(SiteMonSoere.class).getPath();
        if (configuration.getSiteSeparatorForFileNames() != null
                && !Strings.isNullOrEmpty(configuration.getSiteSeparatorForFileNames())) {
            currentSite = currentSite.replaceAll(
                    PatternConfigurator.ANCESTOR_SEPARATOR,
                    configuration.getSiteSeparatorForFileNames());
        }
        return String.format(
                AbstractMonSoereFileNameChecker.PATTERN_FILE_NAME_PATH,
                currentProject,
                currentSite,
                DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateDebutPeriode(), DateUtil.YYYY_MM_DD_FILE),
                DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateFinPeriode(), DateUtil.YYYY_MM_DD_FILE),
                dataset.getVersions().size());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker#testDates(org.inra
     * .ecoinfo.dataset.versioning.entity.VersionFile, java.lang.String, java.lang.String,
     * java.util.regex.Matcher)
     */

    /**
     *
     * @param version
     * @param currentSite
     * @param currentDatatype
     * @param splitFilename
     * @throws InvalidFileNameException
     */

    @Override
    protected void testDates(final VersionFile version, final String currentSite,
            final String currentDatatype, final Matcher splitFilename)
            throws InvalidFileNameException {
        IntervalDate intervalDate;
        assert version != null : "null version";
        final String currentProject = version.getDataset().getRealNode().getNodeByNodeableTypeResource(Projet.class).getCode();
        try {
            intervalDate = IntervalDate.getIntervalDateyyyyMMdd(splitFilename.group(4),
                    splitFilename.group(5));
        } catch (final BadExpectedValueException e1) {
            throw new InvalidFileNameException(String.format(
                    AbstractMonSoereFileNameChecker.INVALID_FILE_NAME_MONSOERE, currentProject, currentSite,
                    currentDatatype, FileNameCheckeryyyymmdd.DATE_PATTERN,
                    FileNameCheckeryyyymmdd.DATE_PATTERN), e1);
        }
        assert version != null : "null version";
        version.getDataset().setDateDebutPeriode(intervalDate.getBeginDate());
        version.getDataset().setDateFinPeriode(intervalDate.getEndDate());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.monsoere.dataset.impl.filenamecheckers.AbstractMonSoereFileNameChecker#testDates
     * (org.inra.ecoinfo.dataset.versioning.entity.VersionFile, java.lang.String, java.lang.String,
     * java.lang.String, java.util.regex.Matcher)
     */

    /**
     *
     * @param version
     * @param currentProject
     * @param currentSite
     * @param currentDatatype
     * @param splitFilename
     * @throws InvalidFileNameException
     */

    @Override
    protected void testDates(VersionFile version, String currentProject, String currentSite,
            String currentDatatype, Matcher splitFilename) throws InvalidFileNameException {
        IntervalDate intervalDate;
        try {
            intervalDate = IntervalDate.getIntervalDateyyyyMMdd(splitFilename.group(4),
                    splitFilename.group(5));
        } catch (final BadExpectedValueException e1) {
            throw new InvalidFileNameException(String.format(
                    AbstractMonSoereFileNameChecker.INVALID_FILE_NAME_MONSOERE, currentProject, currentSite,
                    currentDatatype, FileNameCheckeryyyymmdd.DATE_PATTERN,
                    FileNameCheckeryyyymmdd.DATE_PATTERN), e1);
        }
        if (version != null) {
            version.getDataset().setDateDebutPeriode(intervalDate.getBeginDate());
            version.getDataset().setDateFinPeriode(intervalDate.getEndDate());
        }
    }
}

/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.monsoere.refdata.typesite;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * The Interface ITypeSiteDAO.
 *
 * @author philippe
 * @author "Guillaume Enrico"
 */
public interface ITypeSiteDAO extends IDAO<TypeSite> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    List<TypeSite> getAll();

    /**
     * Gets the type site by code.
     *
     * @param code
     *            the code
     * @return the type site by code
     */
    Optional<TypeSite> getTypeSiteByCode(String code);
}

/**
 */
package org.inra.ecoinfo.monsoere.refdata.site;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.refdata.site.JPASiteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.site.Site_;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class JPASiteMonSoereDAO.
 *
 * @author philippe
 * @author "Guillaume Enrico"
 */
public class JPASiteMonSoereDAO extends JPASiteDAO implements ISiteMonSoereDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.monsoere.refdata.site.ISiteMonSoereDAO#getAllSitesMonSoere()
     */
    /**
     *
     * @return
     */
    @Override
    public List<SiteMonSoere> getAllSitesMonSoere() {
        return getAll().stream()
                .map(t -> (SiteMonSoere) t)
                .collect(Collectors.toList());
    }

    /*
     * *
     * 
     * @see org.inra.ore.forets.plugins.forets1.data.dao.ISiteDAO#getSiteByName(java .lang.String)
     */
 /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.site.JPASiteDAO#getByCode(java.lang.String)
     */
    @Override
    public Optional<Site> getByCodeAndParent(final String code, Site parent) {
        CriteriaQuery<SiteMonSoere> query = builder.createQuery(SiteMonSoere.class);
        Root<SiteMonSoere> sit = query.from(SiteMonSoere.class);
        query
                .select(sit)
                .where(
                        builder.equal(sit.get(Nodeable_.code), code),
                        builder.equal(sit.get(SiteMonSoere_.parent), parent));
        return getOptional(query).map(s -> (Site) s);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.monsoere.refdata.site.ISiteMonSoereDAO#getByNameAndParent(java.lang.String,
     * org.inra.ecoinfo.monsoere.refdata.site.SiteMonSoere)
     */
    /**
     *
     * @param name
     * @param parent
     * @return
     */
    @Override
    public Optional<SiteMonSoere> getByNameAndParent(final String name, final SiteMonSoere parent) {
        CriteriaQuery<SiteMonSoere> query = builder.createQuery(SiteMonSoere.class);
        Root<SiteMonSoere> sit = query.from(SiteMonSoere.class);
        if (parent == null) {
            query.where(
                    builder.equal(sit.get(Site_.name), name),
                    builder.isNull(sit.get(Site_.parent))
            );
        } else {
            query.where(
                    builder.equal(sit.get(Site_.name), name),
                    builder.equal(sit.get(Site_.parent), parent)
            );
        }
        query.select(sit);
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.monsoere.refdata.site.ISiteMonSoereDAO#getListSite()
     */
    /**
     *
     * @return @throws PersistenceException
     */
    @Override
    public Map<String, SiteMonSoere> getListSite() throws PersistenceException {
        return getAll().stream()
                .collect(Collectors.toMap(s -> s.getPath(), s -> (SiteMonSoere) s));
    }
}

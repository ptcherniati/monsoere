/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.monsoere.refdata.adaptableprojetsitethemedatatype;

import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.AbstractBranchNode;
import org.inra.ecoinfo.mga.business.composite.AbstractBranchNode_;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;

/**
 * The Class JPASiteThemeDatatypeDAO.
 *
 * @author "Antoine Schellenberger"
 */
public class JPAProjetSiteThemeDatatypeDAO extends AbstractJPADAO<INode> implements IProjetSiteThemeDatatypeDAO {

    /**
     *
     * @param projetCode
     * @param pathSite
     * @param themeCode
     * @param datatypeCode
     * @return
     */
    @Override
    public List<INode> getByProjetPathSiteThemeCodeAndDatatypeCode(String projetCode, String pathSite, String themeCode, String datatypeCode) {
        CriteriaQuery<Tuple> query = builder.createTupleQuery();
        Root<NodeDataSet> dtyNode = query.from(NodeDataSet.class);
        Join<RealNode, Nodeable> dty = dtyNode.join(AbstractBranchNode_.realNode).join(RealNode_.nodeable);
        final Join<NodeDataSet, AbstractBranchNode> theNode = dtyNode.join(AbstractBranchNode_.parent);
        Join<RealNode, Nodeable> the = theNode.join(AbstractBranchNode_.realNode).join(RealNode_.nodeable);
        final Join<AbstractBranchNode, AbstractBranchNode> sitNode = theNode.join(AbstractBranchNode_.parent);
        Join<RealNode, Nodeable> sit = sitNode.join(AbstractBranchNode_.realNode).join(RealNode_.nodeable);
        final Join<AbstractBranchNode, AbstractBranchNode> projNode = sitNode.join(AbstractBranchNode_.parent);
        Join<RealNode, Nodeable> pro = projNode.join(AbstractBranchNode_.realNode).join(RealNode_.nodeable);
        query.where(
                builder.equal(pro.get(Nodeable_.code), projetCode),
                builder.equal(sit.get(Nodeable_.code), pathSite),
                builder.equal(the.get(Nodeable_.code), themeCode),
                builder.equal(dty.get(Nodeable_.code), datatypeCode)
        );
        query.multiselect(dtyNode, theNode, sitNode);
        List<Tuple> result = getResultList(query);
        return org.apache.commons.collections.CollectionUtils.isEmpty(result)
                ? null
                : result.get(0)
                        .getElements().stream()
                        .map(t -> (INode) t)
                        .collect(Collectors.toList());
    }
}

/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.monsoere.refdata.site;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Interface ISiteMonSoereDAO.
 *
 * @author philippe
 * @author "Guillaume Enrico"
 */
public interface ISiteMonSoereDAO extends ISiteDAO {

    /**
     * Gets the all sites mon soere.
     *
     * @return the all sites mon soere
     */
    List<SiteMonSoere> getAllSitesMonSoere();

    /**
     * Gets the by name and parent.
     *
     * @param code
     *            the code
     * @param parent
     *            the parent
     * @return the by name and parent
     */
    Optional<SiteMonSoere> getByNameAndParent(String code, SiteMonSoere parent);

    /**
     * Gets the list site.
     *
     * @return the list site
     * @throws PersistenceException
     *             the persistence exception
     */
    Map<String, SiteMonSoere> getListSite() throws PersistenceException;
}

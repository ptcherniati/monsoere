/**
 * OREIForets project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.monsoere.refdata.typesite;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.monsoere.dataset.impl.RecorderMonSoere;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class Recorder.
 *
 * @author philippe
 * @author "Guillaume Enrico"
 */
public class Recorder extends AbstractCSVMetadataRecorder<TypeSite> {

    /**
     * The type site dao.
     */
    protected ITypeSiteDAO typeSiteDAO;
    /**
     * The properties nom fr.
     */
    private Properties     propertiesNomFR;
    /**
     * The properties nom en.
     */
    private Properties     propertiesNomEN;
    /**
     * The properties definition en.
     */
    private Properties     propertiesDefinitionEN;

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util
     * .CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final String code = values[0];
                typeSiteDAO.remove(typeSiteDAO.getTypeSiteByCode(code)
                        .orElseThrow(() -> new BusinessException("can't get type site")));

                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<TypeSite> getAllElements() throws BusinessException {
        return typeSiteDAO.getAll();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(TypeSite typeSite)
            throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        final String nom = typeSite == null ? "":  typeSite.getNom();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        typeSite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : nom,
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        typeSite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : propertiesNomFR.getProperty(nom),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        typeSite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : propertiesNomEN.getProperty(nom),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, false, false, false));
        final String definition = typeSite == null ? "": typeSite.getDefinition();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        typeSite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : definition,
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        typeSite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : propertiesDefinitionEN.getProperty(definition),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#initModelGridMetadata()
     */
    @Override
    protected ModelGridMetadata<TypeSite> initModelGridMetadata() {
        propertiesNomFR = localizationManager.newProperties(TypeSite.NAME_ENTITY_JPA, "name",
                Locale.FRANCE);
        propertiesNomEN = localizationManager.newProperties(TypeSite.NAME_ENTITY_JPA, "name",
                Locale.ENGLISH);
        propertiesDefinitionEN = localizationManager.newProperties(TypeSite.NAME_ENTITY_JPA,
                "definition", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     * Process record.
     *
     * @param parser
     *            the parser
     * @param file
     *            the file
     * @param encoding
     *            the encoding
     * @throws BusinessException
     *             the business exception
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding)
            throws BusinessException {
        try {
            skipHeader(parser);
            // On parcourt chaque ligne du fichier
            String[] values = parser.getLine();
            ErrorsReport errorsReport = new ErrorsReport();
            long lineNumber = 0;
            while (values != null) {
                lineNumber++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values,
                        TypeSite.NAME_ENTITY_JPA);
                // On parcourt chaque colonne d'une ligne
                final String code = tokenizerValues.nextToken();
                if (Strings.isNullOrEmpty(code)) {
                    String message = String.format(RecorderMonSoere
                            .getMonSoereMessage(RecorderMonSoere.PROPERTY_MSG_MISSING_TYPE_SITE),
                            lineNumber, tokenizerValues.currentTokenIndex() - 2);
                    errorsReport.addErrorMessage(message);
                    values = parser.getLine();
                    continue;
                }
                final String definition = tokenizerValues.nextToken();
                Optional<TypeSite> dbTypeSiteOpt = typeSiteDAO.getTypeSiteByCode(code);
                if (!dbTypeSiteOpt.isPresent()) {
                    final TypeSite typeSite = new TypeSite(code, definition);
                    typeSiteDAO.saveOrUpdate(typeSite);
                } else if (!errorsReport.hasErrors()) {
                    dbTypeSiteOpt.get().setDefinition(definition);
                    typeSiteDAO.saveOrUpdate(dbTypeSiteOpt.get());
                }
                values = parser.getLine();
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the type site dao.
     *
     * @param typeSiteDAO
     *            the new type site dao
     */
    public void setTypeSiteDAO(ITypeSiteDAO typeSiteDAO) {
        this.typeSiteDAO = typeSiteDAO;
    }
}

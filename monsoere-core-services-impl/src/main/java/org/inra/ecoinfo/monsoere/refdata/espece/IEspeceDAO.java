/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.monsoere.refdata.espece;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * The Interface IEspeceDAO.
 *
 * @author philippe
 * @author "Guillaume Enrico"
 */
public interface IEspeceDAO extends IDAO<Espece> {

    /**
     * Gets the all.
     *
     * @return the all
     */
    List<Espece> getAll();

    /**
     * Gets the espece by code.
     *
     * @param code
     *            the code
     * @return the espece by code
     */
    Optional<Espece> getEspeceByCode(String code);
}

package org.inra.ecoinfo.monsoere.dataset.impl;

import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.monsoere.dataset.IDeleteRecord;
import org.inra.ecoinfo.monsoere.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class DeleteRecord.
 */
public class DeleteRecord implements IDeleteRecord {

    /**
     * The Constant serialVersionUID.
     */
    private static final long    serialVersionUID = 1L;
    /**
     * The version file dao @link(IVersionFileDAO). {@link IVersionFileDAO} The version file dao.
     */
    private IVersionFileDAO      versionFileDAO;
    /**
     * The publication dao @link(ILocalPublicationDAO). {@link ILocalPublicationDAO} publication
     * dao.
     */
    private ILocalPublicationDAO publicationDAO;
    /**
     * The logger @link(Logger). {@link Logger} The logger.
     */
    protected final Logger       LOGGER           = LoggerFactory.getLogger(RecorderMonSoere.class
                                                          .getName());

    /**
     * Instantiates a new delete record.
     */
    public DeleteRecord() {
    }

    /**
     *
     * @param versionFile
     * @throws BusinessException
     */
    @Override
    public void deleteRecord(VersionFile versionFile) throws BusinessException {
        try {
            versionFile = versionFileDAO.merge(versionFile);
            publicationDAO.removeVersion(versionFile);
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the publication dao.
     *
     * @param publicationDAO
     *            the new publication dao {@link ILocalPublicationDAO} the new publication dao
     */
    public void setPublicationDAO(final ILocalPublicationDAO publicationDAO) {
        this.publicationDAO = publicationDAO;
    }

    /**
     * Sets the version file dao.
     *
     * @param versionFileDAO
     *            the new version file dao {@link IVersionFileDAO} the new version file dao
     */
    public void setVersionFileDAO(final IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }
}

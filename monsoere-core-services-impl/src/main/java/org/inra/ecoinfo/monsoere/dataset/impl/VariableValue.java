package org.inra.ecoinfo.monsoere.dataset.impl;

import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;

/**
 * The Class VariableValue.
 */
public class VariableValue {

    /**
     * The datatypeVariableUnite @link(DatatypeVariableUnite).
     */
    private DatatypeVariableUnite datatypeVariableUnite;
    /**
     * The value @link(String).
     */
    private String                value;

    /**
     * Instantiates a new variable value.
     *
     * @param datatypeVariableUnite
     *            the datatypeVariableUnite
     * @param value
     *            the value
     */
    public VariableValue(final DatatypeVariableUnite datatypeVariableUnite, final String value) {
        this.datatypeVariableUnite = datatypeVariableUnite;
        this.value = value;
    }

    /**
     * Gets the datatypeVariableUnite.
     *
     * @return the datatypeVariableUnite
     */
    public DatatypeVariableUnite getDatatypeVariableUnite() {
        return datatypeVariableUnite;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the datatypeVariableUnite.
     *
     * @param datatypeVariableUnite
     *            the new datatypeVariableUnite
     */
    public void setDatatypeVariableUnite(final DatatypeVariableUnite datatypeVariableUnite) {
        this.datatypeVariableUnite = datatypeVariableUnite;
    }

    /**
     * Sets the value.
     *
     * @param value
     *            the new value
     */
    public void setValue(final String value) {
        this.value = value;
    }
}

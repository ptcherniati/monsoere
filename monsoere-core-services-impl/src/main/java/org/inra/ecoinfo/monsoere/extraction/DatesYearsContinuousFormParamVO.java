package org.inra.ecoinfo.monsoere.extraction;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


import java.util.stream.Collector;
import java.util.stream.Collectors;
import javax.faces.component.ValueHolder;
import javax.faces.event.AjaxBehaviorEvent;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.slf4j.LoggerFactory;

/**
 * The Class DatesYearsContinuousFormParamVO.
 */
public class DatesYearsContinuousFormParamVO extends AbstractDatesFormParam {

    /**
     * The Constant LABEL.
     */
    public static final String LABEL = "DatesYearsContinuousFormParam";

    private LocalDate dateStart = null;

    private LocalDate dateEnd = null;

    /**
     * Instantiates a new dates years continuous form param vo.
     */
    public DatesYearsContinuousFormParamVO() {
        super();
    }

    /**
     * Instantiates a new dates years continuous form param vo.
     *
     * @param localizationManager the localization manager
     */
    public DatesYearsContinuousFormParamVO(ILocalizationManager localizationManager) {
        super(localizationManager);
        setPeriods(new LinkedList<>());
        getPeriods().add(buildNewMapPeriod());
    }

    /**
     * Adds the period years continuous.
     *
     * @return the string
     */
    public String addPeriodYearsContinuous() {
        getPeriods().add(buildNewMapPeriod());
        return null;
    }

    /**
     * Builds the continuous period.
     *
     * @param periodIndex the period index
     * @param periodStart the period start
     * @param periodEnd the period end
     * @param hQLAliasDate the h ql alias date
     * @throws DateTimeParseException the parse exception
     */
    protected void buildContinuousPeriod(int periodIndex, String periodStart, String periodEnd,
            String hQLAliasDate) throws DateTimeParseException {
        if (periodStart != null && periodEnd != null && periodStart.trim().length() > 0
                && periodEnd.trim().length() > 0) {
            final LocalDate formattedStartDate = DateUtil.readLocalDateTimeFromText(AbstractDatesFormParam.DATE_FORMAT, periodStart).toLocalDate();
            final String startParameterName = String.format("%s%d",
                    AbstractDatesFormParam.START_PREFIX, periodIndex);
            final String endParameterName = String.format("%s%d",
                    AbstractDatesFormParam.END_PREFIX, periodIndex);
            parametersMap.put(startParameterName, formattedStartDate);
            final LocalDate formattedEndDate = DateUtil.readLocalDateTimeFromText(AbstractDatesFormParam.DATE_FORMAT, periodEnd).toLocalDate();
            parametersMap.put(endParameterName, formattedEndDate);
            sQLCondition = sQLCondition.concat(String.format(AbstractDatesFormParam.SQL_CONDITION,
                    hQLAliasDate, startParameterName, endParameterName));
        }
    }

    /**
     * Builds the continuous period string.
     *
     * @param periodStart the period start
     * @param periodEnd the period end
     * @return the string
     * @throws DateTimeParseException the parse exception
     */
    protected String buildContinuousPeriodString(String periodStart, String periodEnd)
            throws DateTimeParseException {
        return String.format(
                "%s %s %s %s",
                getLocalizationManager().getMessage(AbstractDatesFormParam.BUNDLE_SOURCE_PATH,
                        AbstractDatesFormParam.MSG_FROM),
                periodStart,
                getLocalizationManager().getMessage(AbstractDatesFormParam.BUNDLE_SOURCE_PATH,
                        AbstractDatesFormParam.MSG_TO), periodEnd);
    }

    /**
     * Builds the new map period.
     *
     * @return the map
     */
    private Map<String, String> buildNewMapPeriod() {
        final Map<String, String> firstMap = new HashMap<>();
        firstMap.put(AbstractDatesFormParam.START_INDEX, "");
        firstMap.put(AbstractDatesFormParam.END_INDEX, "");
        return firstMap;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.monsoere.extraction.jsf.AbstractDatesFormParam#buildParameterMapAndSQLCondition
     * (java.lang.String)
     */
    @Override
    protected void buildParameterMapAndSQLCondition(final String hQLAliasDate)
            throws DateTimeParseException {
        int periodIndex = 0;
        for (final Map<String, String> periodsMap : periods) {
            buildContinuousPeriod(periodIndex++,
                    periodsMap.get(AbstractDatesFormParam.START_INDEX),
                    periodsMap.get(AbstractDatesFormParam.END_INDEX), hQLAliasDate);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.monsoere.extraction.jsf.AbstractDatesFormParam#buildSummary(java.io.PrintStream
     * )
     */
    @Override
    public void buildSummary(PrintStream printStream) throws DateTimeParseException {
        periods.forEach((periodsMap) -> {
            printStream.println(String.format(
                    "    %s",
                    buildContinuousPeriodString(periodsMap.get(AbstractDatesFormParam.START_INDEX),
                            periodsMap.get(AbstractDatesFormParam.END_INDEX))));
        });
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.monsoere.extraction.jsf.AbstractDatesFormParam#customValidate()
     */
    @Override
    public void customValidate() {
        LoggerFactory.getLogger(getClass()).error("unused");
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.monsoere.extraction.jsf.AbstractDatesFormParam#getPatternDate()
     */
    @Override
    public String getPatternDate() {
        return AbstractDatesFormParam.DATE_FORMAT;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.monsoere.extraction.jsf.IDateFormParameter#getPeriodsFromDateFormParameter()
     */
    @Override
    public List<Periode> getPeriodsFromDateFormParameter() {
        return getPeriods().stream()
                .map(period->new Periode(period.get(AbstractDatesFormParam.START_INDEX), period.get(AbstractDatesFormParam.END_INDEX)))
                .collect(Collectors.toList());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.monsoere.extraction.jsf.AbstractDatesFormParam#getSummaryHTML()
     */
    @Override
    public String getSummaryHTML() throws DateTimeParseException {
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try (PrintStream printStream = new PrintStream(bos, true, Utils.ENCODING_ISO_8859_15)) {
            if (!getIsValid()) {
                printValidityMessages(printStream);
                return bos.toString(StandardCharsets.ISO_8859_1.name());
            }
            periods.forEach((periodsMap) -> {
                printStream.println(String.format(
                        "    %s<br/>",
                        buildContinuousPeriodString(periodsMap.get(AbstractDatesFormParam.START_INDEX),
                                periodsMap.get(AbstractDatesFormParam.END_INDEX))));
            });
            return bos.toString(StandardCharsets.ISO_8859_1.name());
        } catch (UnsupportedEncodingException ex) {
            return "";
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.monsoere.extraction.jsf.AbstractDatesFormParam#isEmpty()
     */
    @Override
    public boolean isEmpty() {
        return periods.stream()
                .anyMatch((periodsMap) -> (periodsMap.entrySet().stream()
                        .anyMatch((entry) -> (testPeriodEmpty(periodsMap.get(entry.getKey()))))
                        ));
    }

    /**
     * Removes the period years continuous.
     *
     * @return the string
     */
    public String removePeriodYearsContinuous() {
        if (getPeriods().size() <= 1) {
            return null;
        }
        List<Map<String, String>> periods = getPeriods();
        periods = periods.subList(0, periods.size() - 1);
        setPeriods(periods);
        return null;
    }

    /**
     * Update date year continuous.
     *
     * @param index
     * @param value
     * @param key
     * @return the string
     */
    public String updateDateYearContinuous(int index, String key, String value) {
        getPeriods().get(index).put(key, value);
        return null;
    }

    /**
     *
     * @param event
     */
    public void changeDateStart(AjaxBehaviorEvent event) {
        LocalDate date = (LocalDate) ((ValueHolder) event.getSource()).getValue();
        setDateStart(date);
        periods.get(0).put(
                "start",
                date == null ? "" : DateUtil.getUTCDateTextFromLocalDateTime(date.atStartOfDay(), DateUtil.DD_MM_YYYY)
        );
    }

    /**
     *
     * @param event
     */
    public void changeDateEnd(AjaxBehaviorEvent event) {
        LocalDate date = (LocalDate) ((ValueHolder) event.getSource()).getValue();
        setDateEnd(date);

        periods.get(0).put(
                "end", 
                date == null ? "" : DateUtil.getUTCDateTextFromLocalDateTime(date.atStartOfDay(), DateUtil.DD_MM_YYYY)
        );
    }

    /**
     *
     * @return @throws DateTimeParseException
     */
    public LocalDate getDateEnd() throws DateTimeParseException {
        return this.dateEnd;
    }

    /**
     *
     * @param dateEnd
     */
    public void setDateEnd(LocalDate dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     *
     * @return @throws DateTimeParseException
     */
    public LocalDate getDateStart() throws DateTimeParseException {
        return this.dateStart;
    }

    /**
     *
     * @param dateStart
     */
    public void setDateStart(LocalDate dateStart) {
        this.dateStart = dateStart;
    }
}

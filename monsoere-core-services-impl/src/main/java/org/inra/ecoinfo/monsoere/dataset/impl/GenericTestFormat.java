package org.inra.ecoinfo.monsoere.dataset.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.monsoere.dataset.ITestFormat;
import org.inra.ecoinfo.monsoere.dataset.ITestHeaders;
import org.inra.ecoinfo.monsoere.dataset.ITestValues;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GenericTestFormat.
 */
public class GenericTestFormat implements ITestFormat {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The test headers @link(ITestHeaders).
     */
    private ITestHeaders      testHeaders;
    /**
     * The test values @link(ITestValues).
     */
    private ITestValues       testValues;
    /**
     * The datatype name @link(String).
     */
    private String            datatypeName;

    /**
     * Instantiates a new abstract generic test format.
     */
    public GenericTestFormat() {
        super();
    }

    /**
     * Gets the datatype name.
     *
     * @return {@link String} the datatype name
     */
    protected String getDatatypeName() {
        return datatypeName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.monsoere.dataset.ITestFormat#setDatatypeName(java.lang.String)
     */
    @Override
    public void setDatatypeName(final String datatypeName) {
        this.datatypeName = datatypeName;
    }

    /**
     *
     * @param testHeaders
     */
    @Override
    public void setTestHeaders(final ITestHeaders testHeaders) {
        this.testHeaders = testHeaders;
    }

    /**
     *
     * @param testValues
     */
    @Override
    public void setTestValues(final ITestValues testValues) {
        this.testValues = testValues;
    }

    /**
     *
     * @param parser
     * @param versionFile
     * @param encoding
     * @param datasetDescriptor
     * @throws BadFormatException
     */
    @Override
    public void testFormat(CSVParser parser, VersionFile versionFile, String encoding,
            DatasetDescriptor datasetDescriptor) throws BadFormatException {
        final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
        final BadsFormatsReport badsFormatsReport = new BadsFormatsReport(
                RecorderMonSoere.getMonSoereMessage(ITestFormat.PROPERTY_MSG_ERROR_BAD_FORMAT));
        logger.debug(RecorderMonSoere
                .getMonSoereMessage(ITestFormat.PROPERTY_MSG_CHECKING_FORMAT_FILE));
        long lineNumber = -1;
        try {
            lineNumber = testHeaders.testHeaders(parser, versionFile, encoding, badsFormatsReport,
                    datasetDescriptor);
            testValues.testValues(lineNumber, parser, versionFile, encoding, badsFormatsReport,
                    datasetDescriptor, getDatatypeName());
        } catch (final BusinessException e) {
            badsFormatsReport.addException(e);
            logger.debug(badsFormatsReport.getMessages(), e);
            throw new BadFormatException(badsFormatsReport);
        }
        if (badsFormatsReport.hasErrors()) {
            logger.debug(badsFormatsReport.getMessages());
            throw new BadFormatException(badsFormatsReport);
        }
    }
}

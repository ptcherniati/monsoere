package org.inra.ecoinfo.monsoere.extraction;

import java.io.Serializable;
import java.util.List;

/**
 * The Interface IDateFormParameter.
 */
public interface IDateFormParameter extends Serializable{

    /**
     * Gets the periods from date form parameter.
     *
     * @return the periods from date form parameter
     */
    List<Periode> getPeriodsFromDateFormParameter();
}

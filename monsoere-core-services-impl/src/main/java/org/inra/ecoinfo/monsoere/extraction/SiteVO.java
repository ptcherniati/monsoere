package org.inra.ecoinfo.monsoere.extraction;

import java.io.Serializable;
import org.inra.ecoinfo.monsoere.refdata.site.SiteMonSoere;

/**
 * The Class SiteVO.
 */
public class SiteVO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The id.
     */
    private Long              id;
    /**
     * The projet site id.
     */
    private Long              projetSiteId;
    /**
     * The nom site parent.
     */
    private String            nomSiteParent;
    /**
     * The nom.
     */
    private String            nom;
    /**
     * The code.
     */
    private String            code;

    /**
     * Instantiates a new site vo.
     *
     * @param site
     *            the site
     * @param projetSiteId
     *            the projet site id
     */
    public SiteVO(SiteMonSoere site, Long projetSiteId) {
        this.id = site.getId();
        this.nom = site.getName();
        this.code = site.getCode();
        this.projetSiteId = projetSiteId;
        this.nomSiteParent = site.getParent() == null ? null : site.getParent().getName();
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Gets the nom site parent.
     *
     * @return the nom site parent
     */
    public String getNomSiteParent() {
        return nomSiteParent;
    }

    /**
     * Gets the projet site id.
     *
     * @return the projet site id
     */
    public Long getProjetSiteId() {
        return projetSiteId;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Sets the nom.
     *
     * @param nom
     *            the new nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Sets the nom site parent.
     *
     * @param nomSiteParent
     *            the new nom site parent
     */
    public void setNomSiteParent(String nomSiteParent) {
        this.nomSiteParent = nomSiteParent;
    }

    /**
     * Sets the projet site id.
     *
     * @param projetSiteId
     *            the new projet site id
     */
    public void setProjetSiteId(Long projetSiteId) {
        this.projetSiteId = projetSiteId;
    }
}

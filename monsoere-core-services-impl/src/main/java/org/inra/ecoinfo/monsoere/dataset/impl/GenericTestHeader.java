package org.inra.ecoinfo.monsoere.dataset.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.monsoere.dataset.ITestHeaders;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GenericTestHeader.
 */
public class GenericTestHeader implements ITestHeaders {

    /**
     * The Constant KERNEL_BUNDLE_SOURCE_PATH @link(String).
     */
    public static final String    KERNEL_BUNDLE_SOURCE_PATH           = "org.inra.ecoinfo.dataset.impl.messages";
    /**
     * The Constant PROPERTY_MSG_MISMATCH_COLUMN_HEADER @link(String).
     */
    protected static final String PROPERTY_MSG_MISMATCH_COLUMN_HEADER = "PROPERTY_MSG_MISMATCH_COLUMN_HEADER";
    /**
     * The Constant serialVersionUID.
     */
    private static final long     serialVersionUID                    = 1L;
    /**
     * The logger.
     */
    final protected Logger        LOGGER                              = LoggerFactory.getLogger(this
                                                                              .getClass().getName());
    /**
     * The localization manager @link(ILocalizationManager).
     */
    private ILocalizationManager  localizationManager;

    /**
     * Instantiates a new generic test header.
     */
    public GenericTestHeader() {
        super();
    }

    /**
     * Gets the localization manager.
     *
     * @return the localization manager
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     * <p>
     * sanitize the data of the file.
     *
     * @param versionFile
     *            the version file
     * @param logger
     *            the logger
     * @param badsFormatsReport
     *            the bads formats report
     * @return the byte[]
     * @throws BadFormatException
     *             the bad format exception {@link VersionFile} the version file {@link Logger} the
     *             logger {@link BadsFormatsReport} the bads formats report
     * @link(VersionFile) the version file
     * @link(Logger) the logger
     * @link(BadsFormatsReport) the bads formats report
     * @see org.inra.ecoinfo.Utils$sanitizeData(org.inra.ecoinfo.dataset.versioning
     *      .entity.VersionFile)
     */
    protected byte[] sanitizeData(final VersionFile versionFile, final Logger logger,
            final BadsFormatsReport badsFormatsReport) throws BadFormatException {
        byte[] datasSanitized = null;
        try {
            datasSanitized = Utils.sanitizeData(versionFile.getData(), localizationManager);
        } catch (final BusinessException e) {
            logger.debug(badsFormatsReport.getMessages(), e);
            badsFormatsReport.addException(e);
            throw new BadFormatException(badsFormatsReport);
        }
        return datasSanitized;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager
     *            the new localization manager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * Test headers.
     *
     * @param parser
     *            the parser
     * @param versionFile
     *            the version file
     * @param encoding
     *            the encoding
     * @param badsFormatsReport
     *            the bads formats report
     * @param datasetDescriptor
     *            the dataset descriptor
     * @return the long
     * @throws BusinessException
     *             the business exception {@link VersionFile} the version file
     *             {@link ISessionPropertiesMonSoere} the session properties
     *             {@link BadsFormatsReport} the bads formats report {@link DatasetDescriptor} the
     *             dataset descriptor
     * @link(VersionFile) the version file
     * @link(ISessionPropertiesMonSoere) the session properties
     * @link(BadsFormatsReport) the bads formats report
     * @link(DatasetDescriptor) the dataset descriptor
     * @see org.inra.ecoinfo.monsoere.dataset.ITestHeaders#testHeaders(com.Ostermiller.util.CSVParser,
     *      org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     *      org.inra.ecoinfo.monsoere.dataset.ISessionPropertiesMonSoere, java.lang.String)
     */
    @Override
    public long testHeaders(final CSVParser parser, final VersionFile versionFile,
            final String encoding, final BadsFormatsReport badsFormatsReport,
            final DatasetDescriptor datasetDescriptor) throws BusinessException {
        try {
            sanitizeData(versionFile, LOGGER, badsFormatsReport);
        } catch (final BadFormatException e) {
            badsFormatsReport.addException(e);
            LOGGER.info(badsFormatsReport.getMessages(), e);
        }
        int lineNumber = 0;
        String[] values;
        try {
            values = parser.getLine();
            lineNumber++;
            // On parcourt chaque colonne d'une ligne
            for (int index = 0; index < values.length; index++) {
                if (index > datasetDescriptor.getColumns().size() - 1) {
                    break;
                }
                final String value = values[index].trim();
                final Column column = datasetDescriptor.getColumns().get(index);
                final String codifiedConfigColumnNameHeader = Utils.createCodeFromString(column
                        .getName());
                final String codifiedFileColumnNameHeader = Utils.createCodeFromString(value);
                if (!codifiedConfigColumnNameHeader.equals(codifiedFileColumnNameHeader)) {
                    badsFormatsReport.addException(new BadExpectedValueException(String.format(
                            localizationManager.getMessage(
                                    GenericTestHeader.KERNEL_BUNDLE_SOURCE_PATH,
                                    GenericTestHeader.PROPERTY_MSG_MISMATCH_COLUMN_HEADER),
                            index + 1, value, column.getName())));
                }
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        return lineNumber;
    }
}

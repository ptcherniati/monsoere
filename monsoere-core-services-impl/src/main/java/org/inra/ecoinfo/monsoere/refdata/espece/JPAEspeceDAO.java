/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.monsoere.refdata.espece;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * The Class JPAEspeceDAO.
 *
 * @author philippe
 * @author "Guillaume Enrico"
 */
public class JPAEspeceDAO extends AbstractJPADAO<Espece> implements IEspeceDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.monsoere.refdata.espece.IEspeceDAO#getAll()
     */
    /**
     *
     * @return
     */
    @Override
    public List<Espece> getAll() {
        return getAll(Espece.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.monsoere.refdata.espece.IEspeceDAO#getEspeceByCode(java.lang.String)
     */
    /**
     *
     * @param code
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Optional< Espece> getEspeceByCode(final String code) {
        CriteriaQuery<Espece> query = builder.createQuery(Espece.class);
        Root<Espece> esp = query.from(Espece.class);
        query.where(builder.equal(esp.get(Espece_.code), code));
        query.select(esp);
        return getOptional(query);
    }
}

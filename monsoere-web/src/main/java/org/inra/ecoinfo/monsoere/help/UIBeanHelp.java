package org.inra.ecoinfo.monsoere.help;

import java.io.File;
import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author tcherniatinsky
 */
@ManagedBean(name = "uiHelp")
@ViewScoped
public class UIBeanHelp implements Serializable {

    private static final String REFDATAS = "refdatas";
    private static final String PEM = "pem";
    private static final String PATH_PATTERN = "/../../../../../../../resources/fichiers/%s";
    private static final String UI_BEAN_HELP_CLASS = "UIBeanHelp.class";
    private static final String CST_TEST_FILES = "testFiles";

    /**
     *
     * @return
     */
    public String testFiles() {
        return CST_TEST_FILES;
    }

    /**
     *
     * @return
     */
    public List<String> getRefdatasFiles() {
        return Optional.ofNullable(getFile(REFDATAS))
                .map(refdatas->Arrays.asList(refdatas.list()))
                .orElseGet(LinkedList::new );
    }

    private File getFile(String file) {
        return new File(getClass().getResource(UI_BEAN_HELP_CLASS).getFile().replaceAll(UI_BEAN_HELP_CLASS, "").concat(String.format(PATH_PATTERN, file)));
    }

    /**
     *
     * @return
     */
    public List<String> getDatasetFiles() {
        return Optional.ofNullable(getFile(PEM))
                .map(refdatas->Arrays.asList(refdatas.list()))
                .orElseGet(LinkedList::new );
    }
}

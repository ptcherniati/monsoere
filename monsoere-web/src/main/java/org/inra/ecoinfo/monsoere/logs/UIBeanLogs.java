package org.inra.ecoinfo.acbb.logs;

import com.google.common.io.Files;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ptcherniati
 */
@ManagedBean(name = "uiLogs")
@ViewScoped
public class UIBeanLogs implements Serializable {

    private static final String LOGS_FOLDER = "%slogs";
    private static final String LOGS_FILE = "%slogs/%s";
    private static final String COPY_PATTERN = "../../../../../../../resources/logs/%s";
    private static final String UI_BEAN_HELP_CLASS = "UIBeanLogs.class";
    private static final String CST_TEST_FILES = "testFiles";
    @ManagedProperty(value = "#{coreConfiguration}")
    private ICoreConfiguration configuration;

    /**
     *
     */
    public UIBeanLogs() {
    }

    /**
     * @param configuration
     */
    public void setConfiguration(ICoreConfiguration configuration) {
        this.configuration = configuration;
        copyFiles();
    }

    /**
     * @return
     */
    public String testFiles() {
        return CST_TEST_FILES;
    }

    /**
     * @return
     */
    public List<File> getLogsFiles() {
        File logs = getFile(String.format(LOGS_FOLDER, configuration.getRepositoryURI()));
        if (!logs.exists()) {
            logs.mkdirs();
        }
        final File[] logsList = logs.listFiles();
        return Arrays.asList(logsList).stream()
                .filter(f -> !f.isDirectory())
                .sorted((f1, f2) -> f1.lastModified() > f2.lastModified() ? -1 : 1)
                .collect(Collectors.toList());
    }

    private File getFile(String file) {
        return FileWithFolderCreator.createFile(file);
    }

    private void copyFiles() {
        getLogsFiles().stream()
                .map(File::getName)
                .forEach(fileName -> {
                    try {
                        File file = getFile(String.format(LOGS_FILE, configuration.getRepositoryURI(), fileName));
                        String path = getClass().getResource(UI_BEAN_HELP_CLASS).getFile().replaceAll(UI_BEAN_HELP_CLASS, "").concat(String.format(COPY_PATTERN, file.getName()));
                        File copyFile = getFile(path);
                        Files.copy(file, copyFile);
                    }
                    catch (IOException ex) {
                        LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).error(ex.getMessage(), ex);
                    }
                });
    }

    public String getDateFromLong(long date) {
        return DateUtil.getUTCDateTextFromLocalDateTime(
                Instant
                        .ofEpochMilli(date)
                        .atZone(DateUtil.getLocaleZoneId().systemDefault())
                        .toLocalDate(),
                DateUtil.DD_MM_YYYY);
    }

    /**
     * @return
     */
    public String navigate() {
        return "logsFiles";
    }
}

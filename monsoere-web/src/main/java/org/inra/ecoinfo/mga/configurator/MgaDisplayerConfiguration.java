package org.inra.ecoinfo.mga.configurator;

import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.monsoere.refdata.projet.Projet;
import org.inra.ecoinfo.monsoere.refdata.site.SiteMonSoere;
import org.inra.ecoinfo.monsoere.refdata.variable.VariableMonSoere;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.inra.ecoinfo.refdata.theme.Theme;

/**
 *
 * @author ryahiaoui
 */
public class MgaDisplayerConfiguration extends AbstractMgaDisplayerConfiguration {

    /**
     *
     */
    public MgaDisplayerConfiguration() {

        this.addColumnNamesForInstanceType(SiteMonSoere.class, "PROPERTY_COLUMN_LOCALISATION_NAME", "PROPERTY_COLUMN_PLATEFORM_NAME", "PROPERTY_COLUMN_SITE_DEFAULT","PROPERTY_COLUMN_LOCALISATION_NAME", "PROPERTY_COLUMN_PLATEFORM_NAME", "PROPERTY_COLUMN_SITE_DEFAULT");

        this.addColumnNamesForInstanceType(Theme.class, "PROPERTY_COLUMN_THEME_NAME", "PROPERTY_COLUMN_THEME_NAME", "PROPERTY_COLUMN_THEME_NAME", "PROPERTY_COLUMN_THEME_NAME", "PROPERTY_COLUMN_THEME_NAME", "PROPERTY_COLUMN_THEME_NAME");

        this.addColumnNamesForInstanceType(DataType.class, "PROPERTY_COLUMN_DATATYPE_NAME", "PROPERTY_COLUMN_DATATYPE_NAME", "PROPERTY_COLUMN_DATATYPE_NAME", "PROPERTY_COLUMN_DATATYPE_NAME", "PROPERTY_COLUMN_DATATYPE_NAME", "PROPERTY_COLUMN_DATATYPE_NAME");

        this.addColumnNamesForInstanceType(Projet.class, "PROPERTY_COLUMN_PROJET_NAME", "PROPERTY_COLUMN_PROJET_NAME", "PROPERTY_COLUMN_PROJET_NAME", "PROPERTY_COLUMN_PROJET_NAME", "PROPERTY_COLUMN_PROJET_NAME", "PROPERTY_COLUMN_PROJET_NAME");

        this.addColumnNamesForInstanceType(VariableMonSoere.class, "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME", "VariableMonSoere_3", "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME");

        this.addColumnNamesForInstanceType(DatatypeVariableUnite.class, "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME");

        this.addColumnNamesForInstanceType(Refdata.class, "PROPERTY_COLUMN_REFDATA_NAME", "RefDate_1", "RefDate_2", "RefDate_3", "RefDate_4", "RefDate_5");

    }

    /**
     *
     * @param <T>
     * @param instanceType
     * @param columnNames
     */
    @Override
    public final <T extends INodeable> void addColumnNamesForInstanceType(Class<T> instanceType, String... columnNames) {
        super.addColumnNamesForInstanceType(instanceType, columnNames); //To change body of generated methods, choose Tools | Templates.
    }

}

package org.inra.ecoinfo.mga.configurator;

import org.inra.ecoinfo.mga.business.composite.INodeable;
import static org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator.*;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.monsoere.extraction.pem.impl.PemDatasetManager;
import org.inra.ecoinfo.monsoere.refdata.projet.Projet;
import org.inra.ecoinfo.monsoere.refdata.site.SiteMonSoere;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.inra.ecoinfo.refdata.theme.Theme;

/**
 *
 * @author yahiaoui
 */
public class MgaIOConfigurator extends AbstractMgaIOConfigurator {

    private static final Integer DATASET_CONFIGURATION_RIGHTS_100 = 100;
    private static final Integer DATASET_CONFIGURATION_200 = 200;
    private static final Integer SYNTHESIS_CONFIGURATION_500 = 500;
    private static final Integer ASSOCIATE_CONFIGURATION_400 = 400;
    private static final Integer GENERIC_DATATYPE_CONFIGURATION_600 = 600;
    private static final Integer[] NORMAL_ORDER_0_1_2_3 = new Integer[]{0, 1, 2, 3};
    private static final Integer[] INVERSE_ORDER_3_0_2_1 = new Integer[]{3, 0, 2, 1};
    private static final Integer[] REFDATA_ORDER_0 = new Integer[]{0};
    private static final Integer[] EXTRACTION_ORDER_0_1 = new Integer[]{0, 1};
    private static final Class<INodeable>[] ENTRY_INSTANCE_PSTD = new Class[]{
        Projet.class,
        SiteMonSoere.class,
        Theme.class,
        DataType.class
    };
    private static final Class<INodeable>[] ENTRY_INSTANCE_DTSP = new Class[]{
        DataType.class,
        Theme.class,
        SiteMonSoere.class,
        Projet.class
    };
    private static final Class<INodeable>[] ENTRY_INSTANCE_PSTDV = new Class[]{
        Projet.class,
        SiteMonSoere.class,
        Theme.class,
        DataType.class,
        DatatypeVariableUnite.class
    };
    private static final Class<INodeable>[] ENTRY_INSTANCE_DTSPV = new Class[]{
        DataType.class,
        Theme.class,
        SiteMonSoere.class,
        Projet.class,
        DatatypeVariableUnite.class
    };
    private static final Class<INodeable>[] ENTRY_INSTANCE_REF = new Class[]{
        Refdata.class
    };
    private static final Activities[] ACTIVITIES_SAPDSE
            = new Activities[]{
                Activities.synthese,
                Activities.administration,
                Activities.publication,
                Activities.depot,
                Activities.suppression,
                Activities.extraction};
    private static final Activities[] ACTIVITIES_ESTA
            = new Activities[]{
                Activities.edition,
                Activities.suppression,
                Activities.telechargement,
                Activities.administration};
    private static final Activities[] ACTIVITIES_A
            = new Activities[]{
                Activities.associate
            };

    /**
     *
     */
    public MgaIOConfigurator() {
        super(new MgaDisplayerConfiguration() {
        });
    }

    /**
     *
     */
    @Override
    protected void initConfigurations() {
        /**
         * Configuration Zero
         */
        
        boolean includeAncestor = true;

        WhichTree whichTreeDataSet = WhichTree.TREEDATASET;

        Class stickyLeafDatasetRights = DatatypeVariableUnite.class;

        Configuration configDatasetRights_0
                = new Configuration(
                        DATASET_CONFIGURATION_RIGHTS,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_PSTDV,
                        ACTIVITIES_SAPDSE,
                        INVERSE_ORDER_3_0_2_1,
                        includeAncestor,
                        whichTreeDataSet,
                        stickyLeafDatasetRights,
                        false);

        Configuration configDatasetRights_100
                = new Configuration(
                        DATASET_CONFIGURATION_RIGHTS_100,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_DTSPV,
                        ACTIVITIES_SAPDSE,
                        NORMAL_ORDER_0_1_2_3,
                        includeAncestor,
                        whichTreeDataSet,
                        stickyLeafDatasetRights,
                        false);
        Configuration configDataset_2
                = new Configuration(
                        DATASET_CONFIGURATION,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_PSTD,
                        ACTIVITIES_SAPDSE,
                        INVERSE_ORDER_3_0_2_1,
                        includeAncestor,
                        whichTreeDataSet,
                        null,
                        true);
        Configuration configDataset_200
                = new Configuration(
                        DATASET_CONFIGURATION_200,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_DTSP,
                        ACTIVITIES_SAPDSE,
                        NORMAL_ORDER_0_1_2_3,
                        includeAncestor,
                        whichTreeDataSet,
                        null,
                        true);

        /**
         * Configuration Zero
         */
        Integer[] entryOrderRefData = REFDATA_ORDER_0;
        Integer[] sortOrderRefdata = REFDATA_ORDER_0;

        Class<INodeable>[] entryInstanceRefdata = ENTRY_INSTANCE_REF;

        boolean includeAncestorRefdata = true;

        WhichTree whichTreeRefdata = WhichTree.TREEREFDATA;

        Activities[] activitiesRefdata = ACTIVITIES_ESTA;

        AbstractMgaIOConfiguration configRefdataRights_1
                = new Configuration(REFDATA_CONFIGURATION_RIGHTS,
                        Refdata.class,
                        entryOrderRefData,
                        entryInstanceRefdata,
                        activitiesRefdata,
                        sortOrderRefdata,
                        includeAncestor,
                        whichTreeRefdata,
                        null,
                        false);

        AbstractMgaIOConfiguration configRefdata_3
                = new Configuration(REFDATA_CONFIGURATION,
                        Refdata.class,
                        entryOrderRefData,
                        entryInstanceRefdata,
                        activitiesRefdata,
                        sortOrderRefdata,
                        includeAncestor,
                        whichTreeRefdata,
                        null,
                        true);

        AbstractMgaIOConfiguration configSynthesis_5
                = new Configuration(SYNTHESIS_CONFIGURATION,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_PSTD,
                        ACTIVITIES_SAPDSE,
                        INVERSE_ORDER_3_0_2_1,
                        includeAncestor,
                        whichTreeDataSet,
                        null,
                        true);

        AbstractMgaIOConfiguration configSynthesis_500
                = new Configuration(SYNTHESIS_CONFIGURATION_500,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_DTSP,
                        ACTIVITIES_SAPDSE,
                        NORMAL_ORDER_0_1_2_3,
                        includeAncestor,
                        whichTreeDataSet,
                        null,
                        true);
        configSynthesis_5.setSkeletonBuilder("synthesisManager");

        /**
         * Configuration Associate // For RefData Insertion
         */
        Activities[] activitiesAssociate = ACTIVITIES_A;

        AbstractMgaIOConfiguration configAssociate_4
                = new Configuration(
                        ASSOCIATE_CONFIGURATION,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_PSTD,
                        activitiesAssociate,
                        INVERSE_ORDER_3_0_2_1,
                        includeAncestor,
                        whichTreeDataSet,
                        null,
                        false);
        AbstractMgaIOConfiguration configAssociate_400
                = new Configuration(
                        ASSOCIATE_CONFIGURATION_400,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_DTSP,
                        activitiesAssociate,
                        NORMAL_ORDER_0_1_2_3,
                        includeAncestor,
                        whichTreeDataSet,
                        null,
                        false);
        AbstractMgaIOConfiguration configExtraction_10
                = new Configuration(
                        PemDatasetManager.EXTRACTION_CONFIGURATION,
                        DatatypeVariableUnite.class,
                        EXTRACTION_ORDER_0_1,
                        new Class[]{
                            Projet.class,
                            SiteMonSoere.class
                        },
                        new Activities[]{Activities.extraction},
                        EXTRACTION_ORDER_0_1,
                        includeAncestor,
                        whichTreeDataSet,
                        null,
                        true);
        /**
         * cONFIGURATION GÉNÉRIQUE
         */

        Activities[] activitiesGenericDataset
                = new Activities[]{
                    Activities.extraction
                };

        Configuration configGenericDataset_6
                = new Configuration(
                        GENERIC_DATATYPE_CONFIGURATION,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_PSTD,
                        activitiesGenericDataset,
                        INVERSE_ORDER_3_0_2_1,
                        includeAncestor,
                        whichTreeDataSet,
                        null,
                        true);

        Configuration configGenericDataset_600
                = new Configuration(
                        GENERIC_DATATYPE_CONFIGURATION_600,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_DTSP,
                        activitiesGenericDataset,
                        NORMAL_ORDER_0_1_2_3,
                        includeAncestor,
                        whichTreeDataSet,
                        null,
                        true);
        /**
         * ------------------------------------------------------------------------------------
         * *
         */
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION_RIGHTS, k -> configDatasetRights_0);
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION_RIGHTS_100, k -> configDatasetRights_100);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION_RIGHTS, k -> configRefdataRights_1);
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION, k -> configDataset_2);
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION_200, k -> configDataset_200);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION, k -> configRefdata_3);
        getConfigurations().computeIfAbsent(ASSOCIATE_CONFIGURATION, k -> configAssociate_4);
        getConfigurations().computeIfAbsent(ASSOCIATE_CONFIGURATION_400, k -> configAssociate_400);
        getConfigurations().computeIfAbsent(SYNTHESIS_CONFIGURATION, k -> configSynthesis_5);
        getConfigurations().computeIfAbsent(SYNTHESIS_CONFIGURATION_500, k -> configSynthesis_500);
        getConfigurations().computeIfAbsent(PemDatasetManager.EXTRACTION_CONFIGURATION, k -> configExtraction_10);
        getConfigurations().computeIfAbsent(GENERIC_DATATYPE_CONFIGURATION, k -> configGenericDataset_6);
        getConfigurations().computeIfAbsent(GENERIC_DATATYPE_CONFIGURATION_600, k -> configGenericDataset_600);

        /**
         * ------------------------------------------------------------------------------------
         * *
         */
    }

    static class Configuration extends AbstractMgaIOConfiguration {

        public Configuration(Integer codeConfiguration, Class<? extends INodeable> leafType, Integer[] entryOrder, Class<INodeable>[] entryTypes, Activities[] activities, Integer[] sortOrder, boolean includeAncestor, WhichTree whichTree, Class<? extends INodeable> stickyLeafType, boolean displayColumnNames) {
            super(codeConfiguration, leafType, entryOrder, entryTypes, activities, sortOrder, includeAncestor, whichTree, stickyLeafType, displayColumnNames);
        }

    }

}

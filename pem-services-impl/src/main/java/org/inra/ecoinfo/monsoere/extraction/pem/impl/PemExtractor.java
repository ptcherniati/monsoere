package org.inra.ecoinfo.monsoere.extraction.pem.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.monsoere.dataset.pem.IMesurePemDAO;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.MesurePem;
import org.inra.ecoinfo.monsoere.extraction.DatesYearsContinuousFormParamVO;
import org.inra.ecoinfo.monsoere.refdata.espece.Espece;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class PemExtractor.
 */
public class PemExtractor extends AbstractExtractor {

    /**
     * The Constant EXTRACT_PEM.
     */
    public static final String EXTRACT_PEM = "extractPem";
    /**
     * The Constant CST_RESULT_EXTRACTION_CODE.
     */
    public static final String CST_RESULT_EXTRACTION_CODE = "PemDatas";
    /**
     * The Constant MAP_INDEX_0.
     */
    protected static final String MAP_INDEX_0 = "0";
    private static List<Long> selectedPlateforms(Map<String, Object> requestMetadatasMap) {
        return (List<Long>) requestMetadatasMap.get(Site.class.getSimpleName() + "_Id");
    }
    /**
     * The mesure pem dao.
     */
    private IMesurePemDAO mesurePemDAO;

    /**
     * Builds the espece ids.
     *
     * @param selectedEspeces the selected especes
     * @return the list
     */
    private List<Long> buildEspeceIds(List<Espece> selectedEspeces) {
        final List<Long> especeIds = new LinkedList<>();
        selectedEspeces.stream().forEach((espece) -> {
            especeIds.add(espece.getId());
        });
        return especeIds;
    }

    /**
     * Builds the projet site ids.
     *
     * @throws org.inra.ecoinfo.extraction.exception.NoExtractionResultException
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
//    private List<Long> buildProjetIds(List<Projet> projets) {
//        final List<Long> projetIds = new LinkedList<>();
//        for (final Projet projet : projets) {
//            projetIds.add(projet.getId());
//        }
//        return projetIds;
//    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.extraction.impl.AbstractExtractor#extract(org.inra.ecoinfo.extraction.IParameter
     * )
     */
    @SuppressWarnings("rawtypes")
    @Override
    public void extract(IParameter parameters) throws NoExtractionResultException,
            BusinessException {
        prepareRequestMetadatas(parameters.getParameters());
        final Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        ((DefaultParameter) parameters).getResults().put(PemExtractor.CST_RESULT_EXTRACTION_CODE,
                resultsDatasMap);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.extraction.impl.AbstractExtractor#extractDatas(java.util.Map)
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatasMap)
            throws NoExtractionResultException, BusinessException {

        /* List Selected PlateFomres by User */
        List<Long> plateforeIds = selectedPlateforms(requestMetadatasMap);

        final Map<String, List> extractedDatasMap = new TreeMap();

        List<Long> especeIds = selectedSpicies(requestMetadatasMap);
        final DatesYearsContinuousFormParamVO datesRequestParamVO = getDateRequestParam(requestMetadatasMap);
        List<MesurePem> mesuresPem = null;
        mesuresPem = mesurePemDAO.extractDatas(plateforeIds, especeIds, datesRequestParamVO, policyManager.getCurrentUser());
        if (mesuresPem == null || mesuresPem.isEmpty()) {
            throw new NoExtractionResultException(getLocalizationManager().getMessage(
                    NoExtractionResultException.BUNDLE_SOURCE_PATH,
                    NoExtractionResultException.PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
        extractedDatasMap.put(PemExtractor.MAP_INDEX_0, mesuresPem);
        return extractedDatasMap;
    }

    private DatesYearsContinuousFormParamVO getDateRequestParam(Map<String, Object> requestMetadatasMap) {
        return (DatesYearsContinuousFormParamVO) requestMetadatasMap
                .get(DatesYearsContinuousFormParamVO.class.getSimpleName());
    }

    private List<Long> selectedSpicies(Map<String, Object> requestMetadatasMap) {
        final List<Espece> selectedEspeces = (List<Espece>) requestMetadatasMap.get(Espece.class
                .getSimpleName());
        final List<Long> especeIds = buildEspeceIds(selectedEspeces);
        return especeIds;
    }


    @Override
    public long getExtractionSize(IParameter parameters) {
        return mesurePemDAO.getExtractionSize(
                selectedPlateforms(parameters.getParameters()),
                selectedSpicies(parameters.getParameters()),
                getDateRequestParam(parameters.getParameters()),
                policyManager.getCurrentUser());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.extraction.impl.AbstractExtractor#prepareRequestMetadatas(java.util.Map)
     */
    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap)
            throws BusinessException {
        // on peut éventuellement trier les variables par ordre alphabétique ou selon un ordre
        // d'affichage défini
    }

    /**
     * Sets the mesure pem dao.
     *
     * @param mesurePemDAO the new mesure pem dao
     */
    public void setMesurePemDAO(IMesurePemDAO mesurePemDAO) {
        this.mesurePemDAO = mesurePemDAO;
    }
}

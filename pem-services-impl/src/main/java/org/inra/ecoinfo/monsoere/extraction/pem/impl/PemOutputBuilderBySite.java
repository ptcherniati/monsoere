package org.inra.ecoinfo.monsoere.extraction.pem.impl;

import com.google.common.base.Strings;
import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;


import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.MesurePem;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.ValeurPem;
import org.inra.ecoinfo.monsoere.refdata.espece.Espece;
import org.inra.ecoinfo.monsoere.refdata.projet.Projet;
import org.inra.ecoinfo.monsoere.refdata.site.SiteMonSoere;
import org.inra.ecoinfo.monsoere.refdata.variable.VariableMonSoere;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.LoggerFactory;

/**
 * The Class PemOutputBuilderBySite.
 */
public class PemOutputBuilderBySite extends AbstractOutputBuilder {

    /**
     * The Constant BUNDLE_SOURCE_PATH_PEM.
     */
    protected static final String BUNDLE_SOURCE_PATH_PEM = "org.inra.ecoinfo.monsoere.extraction.pem.messages";
    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.monsoere.dataset.pem.messages";
    /**
     * The Constant MAP_INDEX_0.
     */
    protected static final String MAP_INDEX_0 = "0";
    /**
     * The Constant HEADER_PROJECT.
     */
    private static final String HEADER_PROJECT = "PROPERTY_MSG_HEADER_PROJECT";
    /**
     * The Constant PROPERTY_MSG_ERROR.
     */
    private static final String PROPERTY_MSG_ERROR = "PROPERTY_MSG_ERROR";
    /**
     * The Constant MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS.
     */
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE";
    /**
     * The Constant CODE_DATATYPE_PEM.
     */
    private static final String CODE_DATATYPE_PEM = "piegeage_en_montee";
    /**
     * The datatype variable unite dao.
     */
    private IDatatypeVariableUniteDAO datatypeVariableUniteDAO;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildBody(java.lang.String,
     * java.util.Map, java.util.Map)
     */

    /**
     *
     * @param headers
     * @param resultsDatasMap
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    
    @SuppressWarnings({"rawtypes", "unchecked", "unused"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap,
            Map<String, Object> requestMetadatasMap) throws BusinessException {
        final List<MesurePem> mesuresPem = resultsDatasMap.get(PemOutputBuilderBySite.MAP_INDEX_0);
        final Properties propertiesValeurQualitativeValue = localizationManager.newProperties(
                IValeurQualitative.NAME_ENTITY_JPA, "valeur");
        final List<IFlatNode> sites = (List<IFlatNode>) requestMetadatasMap
                .get(Site.class.getSimpleName());
        final Set<VariableMonSoere> selectedVariables = new TreeSet(
                (Collection) requestMetadatasMap.get(Variable.class
                        .getSimpleName()));
        final Set<Espece> selectedEspeces = new TreeSet(
                (Collection) requestMetadatasMap.get(Espece.class.getSimpleName()));
        final Set<String> sitesNames = retrieveSitesCodes(sites);
        final Map<String, File> filesMap = buildOutputsFiles(sitesNames,
                AbstractOutputBuilder.EXTENSION_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        outputPrintStreamMap.entrySet().stream().forEach((siteNameEntry) -> {
            outputPrintStreamMap.get(siteNameEntry.getKey()).println(headers);
        });
        final Map<LocalDate, Map<String, Map<String, MesurePem>>> sortedDatasMap = new HashMap<>();
        parseMesures(mesuresPem, sortedDatasMap);
        final List<LocalDate> dates = new LinkedList<>(sortedDatasMap.keySet());
        Collections.sort(dates);
        dates.stream().forEach((date) -> {
            parseEspeces(selectedEspeces, sortedDatasMap, date, sites, outputPrintStreamMap, selectedVariables, propertiesValeurQualitativeValue);
        });
        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private void parseEspeces(final Set<Espece> selectedEspeces, final Map<LocalDate, Map<String, Map<String, MesurePem>>> sortedDatasMap, final LocalDate date, final List<IFlatNode> sites, final Map<String, PrintStream> outputPrintStreamMap, final Set<VariableMonSoere> selectedVariables, final Properties propertiesValeurQualitativeValue) {
        selectedEspeces.stream().forEach((espece) -> {
            final Map<String, MesurePem> mesurePemMap = sortedDatasMap.get(date).get(
                    espece.getCode());
            if (mesurePemMap != null) {
                String line;
                line = String.format("%s;%s", DateUtil.getUTCDateTextFromLocalDateTime(date.atStartOfDay(), DateUtil.DD_MM_YYYY), espece.getCode());
                parseProjects(sites, outputPrintStreamMap, line, mesurePemMap, selectedVariables, propertiesValeurQualitativeValue);
            }
        });
    }

    private void parseProjects(final List<IFlatNode> sites, final Map<String, PrintStream> outputPrintStreamMap, String line, final Map<String, MesurePem> mesurePemMap, final Set<VariableMonSoere> selectedVariables, final Properties propertiesValeurQualitativeValue) {
        sites.stream().forEach((siteNode) -> {
            final PrintStream rawDataPrintStream = outputPrintStreamMap.get(Utils
                    .createCodeFromString(siteNode.getName()));
            rawDataPrintStream.print(line);
            final MesurePem mesurePem = mesurePemMap
                    .get(siteNode.getCode());
            if (mesurePem != null) {
                final List<ValeurPem> valeursPem = mesurePem.getValeursPem();
                parseVariables(selectedVariables, rawDataPrintStream, valeursPem, propertiesValeurQualitativeValue);
            } else {
                for (int i = 0; i < selectedVariables.size(); i++) {
                    rawDataPrintStream.print(";");
                }
            }
            if (sites.indexOf(siteNode) == sites.size() - 1) {
                rawDataPrintStream.println();
            }
        });
    }

    private void parseVariables(final Set<VariableMonSoere> selectedVariables, final PrintStream rawDataPrintStream, final List<ValeurPem> valeursPem, final Properties propertiesValeurQualitativeValue) {
        selectedVariables.stream().forEach((variable) -> {
            rawDataPrintStream.print(";");
            parseValeurs(valeursPem, variable, propertiesValeurQualitativeValue, rawDataPrintStream);
        });
    }

    private void parseValeurs(final List<ValeurPem> valeursPem, final VariableMonSoere variable, final Properties propertiesValeurQualitativeValue, final PrintStream rawDataPrintStream) {
        valeursPem.stream().filter((valeurPem) -> (getDatatypeVariableunite(valeurPem).getVariable().getId()
                .equals(variable.getId()))).forEach((valeurPem) -> {
                    if (getDatatypeVariableunite(valeurPem).getVariable()
                            .getIsQualitative()) {
                        if (valeurPem.getValeurQualitative() != null) {
                            String localizedValue = propertiesValeurQualitativeValue
                                    .getProperty(valeurPem
                                            .getValeurQualitative().getValeur());
                            if (Strings.isNullOrEmpty(localizedValue)) {
                                localizedValue = valeurPem
                                        .getValeurQualitative().getValeur();
                            }
                            rawDataPrintStream.print(String.format("%s",
                                    localizedValue));
                        } else {
                            rawDataPrintStream.print(String.format("%s", ""));
                        }
                    } else {
                        if (valeurPem.getValue() != null) {
                            rawDataPrintStream.print(String.format("%s",
                                    valeurPem.getValue()));
                        } else {
                            rawDataPrintStream.print(String.format("%s", ""));
                        }
                    }
        });
    }

    private DatatypeVariableUnite getDatatypeVariableunite(ValeurPem valeurPem) {
        return (DatatypeVariableUnite)valeurPem.getRealNode().getNodeable();
    }

    private void parseMesures(final List<MesurePem> mesuresPem, final Map<LocalDate, Map<String, Map<String, MesurePem>>> sortedDatasMap) {
        mesuresPem.stream().forEach((mesurePem) -> {
            final LocalDate date = mesurePem.getSequencePem().getDate();
            final String siteCode = mesurePem.getSequencePem().getVersion().getDataset().getRealNode().getNodeByNodeableTypeResource(Site.class).getCode();
            final String especeCode = mesurePem.getEspece().getCode();
            if (sortedDatasMap.get(date) == null) {
                final Map<String, MesurePem> sortedMesuresBySite = new HashMap<>();
                sortedMesuresBySite.put(siteCode, mesurePem);
                final Map<String, Map<String, MesurePem>> sortedMesuresPem = new HashMap<>();
                sortedMesuresPem.put(especeCode, sortedMesuresBySite);
                sortedDatasMap.put(date, sortedMesuresPem);
            } else {
                if (sortedDatasMap.get(date).get(especeCode) == null) {
                    final Map<String, MesurePem> sortedMesuresBySite = new HashMap<>();
                    sortedMesuresBySite.put(siteCode, mesurePem);
                    sortedDatasMap.get(date).put(especeCode, sortedMesuresBySite);
                } else {
                    sortedDatasMap.get(date).get(especeCode).put(siteCode, mesurePem);
                }
            }
        });
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildHeader(java.util.Map)
     */
    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport(getLocalizationManager().getMessage(
                    PemOutputBuilderBySite.BUNDLE_SOURCE_PATH,
                    PemOutputBuilderBySite.PROPERTY_MSG_ERROR));
            final Properties propertiesVariableName = localizationManager.newProperties(
                    VariableMonSoere.NAME_ENTITY_JPA, "nom");
            final Properties propertiesProjectName = localizationManager.newProperties(
                    Projet.TABLE_NAME, "nom");
            final Properties propertiesSiteName = localizationManager.newProperties(
                    SiteMonSoere.TABLE_NAME, "nom");
        final Set<VariableMonSoere> selectedVariables = new TreeSet(
                (Collection) requestMetadatasMap.get(Variable.class
                        .getSimpleName()));
            final List<IFlatNode> plateformes = (List<IFlatNode>) requestMetadatasMap
                    .get(Site.class.getSimpleName());
            String localizedProjectName = propertiesProjectName
                    .getProperty(plateformes.get(0).getCode(), plateformes.get(0).getName());
            return printProjetSite(errorsReport, propertiesVariableName, propertiesSiteName,
                    selectedVariables, plateformes, localizedProjectName);
        } catch (PersistenceException ex) {
            LoggerFactory.getLogger(PemOutputBuilderBySite.class.getName()).error(ex.getMessage(), ex);
            return "";
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo.extraction.IParameter
     * )
     */

    /**
     *
     * @param parameters
     * @return 
     * @throws BusinessException
     */
    
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        ((DefaultParameter) parameters).getFilesMaps().add(
                super.buildOutput(parameters, PemExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    private String printProjetSite(final ErrorsReport errorsReport,
            final Properties propertiesVariableName, final Properties propertiesSiteName,
            final Set<VariableMonSoere> selectedVariables, final List<IFlatNode> sites, String localizedProjectName)
            throws PersistenceException {
        final StringBuilder stringBuilder1 = new StringBuilder();
        final StringBuilder stringBuilder2 = new StringBuilder();
        final StringBuilder stringBuilder3 = new StringBuilder();
        final StringBuilder stringBuilder4 = new StringBuilder();
        final StringBuilder stringBuilder5 = new StringBuilder();
        stringBuilder1.append(String.format(
                "%s;%s",
                getLocalizationManager().getMessage(
                        PemOutputBuilderBySite.BUNDLE_SOURCE_PATH_PEM,
                        PemOutputBuilderBySite.HEADER_PROJECT), localizedProjectName));
        stringBuilder2.append(";");
        stringBuilder3.append(";");
        stringBuilder4.append(String.format("%s;%s", "date", "espece"));
        stringBuilder5.append(";");

        for (final IFlatNode siteNode : sites) {
            String localizedSiteName = propertiesSiteName.getProperty(siteNode.getName());
            if (Strings.isNullOrEmpty(localizedSiteName)) {
                localizedSiteName = siteNode.getName();
            }
            String localizedPlateformeName = propertiesSiteName.getProperty(siteNode.getName());
            if (Strings.isNullOrEmpty(localizedPlateformeName)) {
                localizedPlateformeName = siteNode.getName();
            }
            stringBuilder2.append(String.format(";%s", localizedSiteName));
            stringBuilder3.append(String.format(";%s", localizedPlateformeName));
            printVariables(errorsReport, propertiesVariableName, selectedVariables, stringBuilder2,
                    stringBuilder3, stringBuilder4, stringBuilder5);
        }
        return stringBuilder1.append("\n").append(stringBuilder2).append("\n")
                .append(stringBuilder3).append("\n").append(stringBuilder4).append("\n")
                .append(stringBuilder5).toString();
    }

    private void printVariables(final ErrorsReport errorsReport,
            final Properties propertiesVariableName, final Set<VariableMonSoere> selectedVariables,
            final StringBuilder stringBuilder2, final StringBuilder stringBuilder3,
            final StringBuilder stringBuilder4, final StringBuilder stringBuilder5)
            throws PersistenceException {

        for (final VariableMonSoere variable : selectedVariables) {
            stringBuilder2.append(";");
            stringBuilder3.append(";");
            if (variable == null) {
                errorsReport.addException(new BusinessException(String.format(
                        getLocalizationManager().getMessage(
                                PemOutputBuilderBySite.BUNDLE_SOURCE_PATH_PEM,
                                PemOutputBuilderBySite.MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS),
                        "null variable")));
                continue;
            }
            String localizedVariableName = propertiesVariableName.getProperty(variable.getName());
            if (Strings.isNullOrEmpty(localizedVariableName)) {
                localizedVariableName = variable.getName();
            }
            final Unite unite = datatypeVariableUniteDAO.getUnite(
                    PemOutputBuilderBySite.CODE_DATATYPE_PEM, variable.getCode()).orElse(null);
            String uniteNom;
            if (unite == null) {
                uniteNom = "nounit";
            } else {
                uniteNom = unite.getCode();
            }
            stringBuilder4.append(String.format(";%s", localizedVariableName));
            stringBuilder5.append(String.format(";%s", uniteNom));
        }
    }

    /**
     * Retrieve sites codes.
     * <p>
     *
     * @param selectedPlateformes the selected plateformes
     * <p>
     * @return the sets the
     */
    private Set<String> retrieveSitesCodes(List<IFlatNode> sitesNodes) {
        final Set<String> sitesNames = new TreeSet(Collections.reverseOrder());
        sitesNodes.stream().forEach((siteNode) -> {
            sitesNames.add(Utils.createCodeFromString(siteNode.getName()));
        });
        return sitesNames;
    }

    /**
     * Sets the datatype variable unite dao.
     * <p>
     *
     * @param datatypeVariableUniteDAO the new datatype variable unite dao
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}

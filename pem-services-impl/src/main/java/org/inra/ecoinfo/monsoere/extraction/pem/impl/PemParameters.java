package org.inra.ecoinfo.monsoere.extraction.pem.impl;

import java.util.Map;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;

/**
 * The Class PemParameters.
 *
 * @author philippe
 * @author "Guillaume Enrico"
 */
public class PemParameters extends DefaultParameter implements IParameter {

    /**
     * The Constant PEM_EXTRACTION_TYPE_CODE.
     */
    private static final String PEM_EXTRACTION_TYPE_CODE = "pem";

    /**
     * Instantiates a new pem parameters.
     *
     * @param metadatasMap
     *            the metadatas map
     */
    public PemParameters(Map<String, Object> metadatasMap) {
        setParameters(metadatasMap);
        setCommentaire((String) metadatasMap.get(IExtractionManager.KEYMAP_COMMENTS));
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.extraction.IParameter#getExtractionTypeCode()
     */
    @Override
    public String getExtractionTypeCode() {
        return PemParameters.PEM_EXTRACTION_TYPE_CODE;
    }
}

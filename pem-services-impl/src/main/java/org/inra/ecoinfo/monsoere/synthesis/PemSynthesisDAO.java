
package org.inra.ecoinfo.monsoere.synthesis;

import java.lang.reflect.Constructor;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.stream.Stream;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.MesurePem;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.MesurePem_;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.SequencePem;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.SequencePem_;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.ValeurPem;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.ValeurPem_;
import org.inra.ecoinfo.monsoere.synthesis.pem.SynthesisDatatype;
import org.inra.ecoinfo.monsoere.synthesis.pem.SynthesisValue;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative_;
import org.inra.ecoinfo.synthesis.IJPASynthesisValueDAO;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue_;

/**
 *
 * @author tcherniatinsky
 */
public class PemSynthesisDAO extends AbstractJPADAO implements IJPASynthesisValueDAO<SynthesisValue, SynthesisDatatype> {

    /**
     *
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        CriteriaQuery<SynthesisValue> query = builder.createQuery(SynthesisValue.class);
        Root<ValeurPem> valeur = query.from(ValeurPem.class);
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        Join<ValeurPem, ValeurQualitative> vq = valeur.join(ValeurPem_.valeurQualitative, JoinType.LEFT);
        Join<MesurePem, SequencePem> s = valeur.join(ValeurPem_.mesurePem).join(MesurePem_.sequencePem);
        Join<ValeurPem, RealNode> varRn = valeur.join(ValeurPem_.realNode);
        Join<RealNode, RealNode> dtyRn = varRn.join(RealNode_.parent);
        Join<RealNode, RealNode> siteRn = dtyRn.join(RealNode_.parent).join(RealNode_.parent);
        Path<Long> idNode = node.get(NodeDataSet_.id);
        query.distinct(true)
                .select(
                        builder.construct(
                                SynthesisValue.class,
                                s.get(SequencePem_.date),
                                siteRn.get(RealNode_.path),
                                varRn.get(RealNode_.nodeable).get(Nodeable_.code),
                                builder.avg(valeur.get(ValeurPem_.value)),
                                vq.get(ValeurQualitative_.valeur),
                                idNode
                        )
                )
                .groupBy(
                        siteRn.get(RealNode_.path),
                        varRn.get(RealNode_.nodeable).get(Nodeable_.code),
                        s.get(SequencePem_.date),
                        vq.get(ValeurQualitative_.valeur),
                        idNode
                )
                .orderBy(
                        builder.asc(siteRn.get(RealNode_.path)),
                        builder.asc(varRn.get(RealNode_.nodeable).get(Nodeable_.code)),
                        builder.asc(s.get(SequencePem_.date))
                )
                .where(
                        builder.and(
                                builder.equal(node.get(NodeDataSet_.realNode), varRn),
                                builder.equal(valeur.get(ValeurPem_.value), -9_999).not()
                        )
                );
        return getResultAsStream(query);
    }

    @Override
    public Stream<SynthesisDatatype> getSynthesisDatatype() {

        Root<SynthesisDatatype> sv = builder.createQuery(SynthesisDatatype.class).from(SynthesisDatatype.class);
        String sql = "select site, min(date), max(date), string_agg(distinct(cnp.branch_node_id)::::text, ',') idnode\n"
                + "from PemSynthesisValue sv\n"
                + "join composite_node_data_set nd on nd.branch_node_id=sv.idnode\n"
                + "join composite_node_data_set cnp on nd.id_parent_node=cnp.branch_node_id\n"
                + "group by site";
        final org.hibernate.query.Query query = (org.hibernate.query.Query) entityManager.createNativeQuery(String.format(sql, "PemSynthesisDatatype"));
        return query
                .stream()
                .map(
                        sd -> {
                            try {
                                final String site = (String) ((Object[]) sd)[0];
                                final LocalDateTime minDate = ((Timestamp) ((Object[]) sd)[1]).toLocalDateTime();
                                final LocalDateTime maxdate = ((Timestamp) ((Object[]) sd)[2]).toLocalDateTime();
                                final String idNodes = (String) ((Object[]) sd)[3];
                                return new SynthesisDatatype(site, minDate, maxdate, idNodes);
                            } catch (Exception e) {
                                return null;
                            }
                        })
                .filter(sd->sd!=null);
    }

}

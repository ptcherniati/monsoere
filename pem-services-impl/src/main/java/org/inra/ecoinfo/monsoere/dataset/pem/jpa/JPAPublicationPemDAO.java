package org.inra.ecoinfo.monsoere.dataset.pem.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.monsoere.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.MesurePem;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.MesurePem_;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.SequencePem;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.SequencePem_;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.ValeurPem;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.ValeurPem_;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class JPAPublicationPemDAO.
 */
public class JPAPublicationPemDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     * Removes the version.
     *
     * @param version the version
     * @throws PersistenceException the persistence exception
     * @see
     * org.inra.ecoinfo.monsoere.dataset.pem.ILocalPublicationDAO#removeVersion(org.
     * inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public void removeVersion(final VersionFile version) throws PersistenceException {
        try {
            deleteSequence(version);
        } catch (final Exception e) {
            throw new PersistenceException("can't delete version", e);
        }
    }

    private void deleteSequence(VersionFile versionFile) {
        deleteMeasures(versionFile);
        CriteriaDelete<SequencePem> deleteSequences = builder.createCriteriaDelete(SequencePem.class);
        Root<SequencePem> sequences = deleteSequences.from(SequencePem.class);
        deleteSequences.where(
                builder.equal(sequences.get(SequencePem_.version), versionFile)
        );
        entityManager.createQuery(deleteSequences).executeUpdate();
    }

    private void deleteMeasures(VersionFile versionFile) {
        deleteValues(versionFile);
        CriteriaDelete<MesurePem> deleteMeasures = builder.createCriteriaDelete(MesurePem.class);
        Root<MesurePem> measures = deleteMeasures.from(MesurePem.class);
        Subquery<SequencePem> subquery = deleteMeasures.subquery(SequencePem.class);
        Root<SequencePem> sequence = subquery.from(SequencePem.class);
        subquery.where(builder.equal(sequence.get(SequencePem_.version), versionFile));
        subquery.select(sequence);
        deleteMeasures.where(
                measures.get(MesurePem_.sequencePem).in(subquery)
        );
        entityManager.createQuery(deleteMeasures).executeUpdate();
    }

    private void deleteValues(VersionFile versionFile) {
        CriteriaDelete<ValeurPem> deleteValues = builder.createCriteriaDelete(ValeurPem.class);
        Root<ValeurPem> valeur = deleteValues.from(ValeurPem.class);
        Subquery<MesurePem> subquery = deleteValues.subquery(MesurePem.class);
        Root<MesurePem> measures = subquery.from(MesurePem.class);
        subquery.where(
                builder.equal(measures.get(MesurePem_.sequencePem).get(SequencePem_.version), versionFile)
        );
        subquery.select(measures);
        deleteValues.where(valeur.get(ValeurPem_.mesurePem).in(subquery)
        );
        entityManager.createQuery(deleteValues).executeUpdate();
    }
}

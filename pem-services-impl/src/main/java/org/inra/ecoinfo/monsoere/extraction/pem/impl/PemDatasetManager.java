package org.inra.ecoinfo.monsoere.extraction.pem.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import org.inra.ecoinfo.dataset.impl.DefaultDatasetManager;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.monsoere.dataset.pem.IMesurePemDAO;
import org.inra.ecoinfo.monsoere.dataset.pem.ISequencePemDAO;
import org.inra.ecoinfo.monsoere.dataset.pem.IValeurPemDAO;
import org.inra.ecoinfo.monsoere.extraction.pem.IPemDatasetManager;
import org.inra.ecoinfo.monsoere.refdata.espece.Espece;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class PemDatasetManager.
 */
public class PemDatasetManager extends DefaultDatasetManager implements IPemDatasetManager {

    /**
     *
     */
    public static final int EXTRACTION_CONFIGURATION     = 10;
    private static final long   serialVersionUID = 1L;
    /**
     * The sequence pem dao.
     */
    protected ISequencePemDAO     sequencePemDAO;
    /**
     * The mesure pem dao.
     */
    private IMesurePemDAO       mesurePemDAO;
    /**
     * The valeur pem dao.
     */
    private IValeurPemDAO       valeurPemDAO;

//    private IFileCompDAO        fileCompDAO;
    private IPolicyManager    policyManager;

    /**
     * Instantiates a new pem dataset manager.
     */
    public PemDatasetManager() {
        super();
    }

    /**
     * Retrieve list especes available.
     *
     * @param plateformeId
     *            the plateforme id
     * @param dateStart
     *            the date start
     * @param dateEnd
     *            the date end
     * @return the list
     * @throws BusinessException
     *             the business exception
     */
    private List<Espece> retrieveListEspecesAvailable( List<Long> plateformeId, LocalDate dateStart,
            LocalDate dateEnd) throws BusinessException {
        return mesurePemDAO.retrievePemAvailablesEspeces( plateformeId, dateStart, dateEnd);
    }

    /*
     * (non-Javadoc) SiteMonSoere
     * 
     * @see
     * org.inra.ecoinfo.monsoere.extraction.pem.IPemDatasetManager#retrieveListsEspeces(java.util
     * .List, java.time.LocalDateTime, java.time.LocalDateTime)
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Espece> retrieveListEspeces( List<Long> plateformeId, LocalDate dateStart, LocalDate dateEnd)
            throws BusinessException {
        return retrieveListEspecesAvailable( plateformeId, dateStart, dateEnd);
    }

    /**
     * Sets the mesure pem dao.
     *
     * @param mesurePemDAO
     *            the new mesure pem dao
     */
    public void setMesurePemDAO(IMesurePemDAO mesurePemDAO) {
        this.mesurePemDAO = mesurePemDAO;
    }

    /**
     *
     * @param policyManager
     */
    @Override
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     * Sets the sequence pem dao.
     *
     * @param sequencePemDAO
     *            the new sequence pem dao
     */
    public void setSequencePemDAO(ISequencePemDAO sequencePemDAO) {
        this.sequencePemDAO = sequencePemDAO;
    }

    /**
     * Sets the valeur pem dao.
     *
     * @param valeurPemDAO
     *            the new valeur pem dao
     */
    public void setValeurPemDAO(IValeurPemDAO valeurPemDAO) {
        this.valeurPemDAO = valeurPemDAO;
    }

    @Override
    public List<INode> getAvailablesVariablesFilteredByrights(Set<String> sites, LocalDate dateStart, LocalDate dateEnd) {
        return valeurPemDAO.getAvailablesVariablesFilteredByRights(sites, dateStart,dateEnd, policyManager.getCurrentUser());
    }

    /**
     *
     * @param currentUser
     * @return
     */
    @Override
    public List<INode> getSitesAvailables(IUser currentUser) {
        return valeurPemDAO.getSitesAvailables(currentUser);
    }
}

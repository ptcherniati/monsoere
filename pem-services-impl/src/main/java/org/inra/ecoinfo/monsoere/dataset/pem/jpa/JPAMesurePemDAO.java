package org.inra.ecoinfo.monsoere.dataset.pem.jpa;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.AbstractBranchNode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity_;
import org.inra.ecoinfo.monsoere.dataset.pem.IMesurePemDAO;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.MesurePem;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.MesurePem_;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.SequencePem;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.SequencePem_;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.ValeurPem;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.ValeurPem_;
import org.inra.ecoinfo.monsoere.extraction.DatesYearsContinuousFormParamVO;
import org.inra.ecoinfo.monsoere.refdata.espece.Espece;
import org.inra.ecoinfo.monsoere.refdata.espece.Espece_;

/**
 * The Class JPAMesurePemDAO.
 */
public class JPAMesurePemDAO extends AbstractJPADAO<MesurePem> implements
        IMesurePemDAO<DatesYearsContinuousFormParamVO> {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.monsoere.dataset.pem.IMesurePemDAO#extractDatas(java.util.List,
     * java.util.List, org.inra.ecoinfo.monsoere.extraction.jsf.DatesYearsContinuousFormParamVO)
     */
    @Override
    public List<MesurePem> extractDatas(List<Long> selectedPlateformesNodesIds, List<Long> especeIds, DatesYearsContinuousFormParamVO dateRequestParamVO, IUser user) {
        List<Tuple> tuples = buildQuery(false, selectedPlateformesNodesIds, especeIds, dateRequestParamVO, user);
        return tuples.stream()
                .map(t -> (MesurePem) t.<MesurePem>get(0))
                .collect(Collectors.toList());
    }

    private List<Tuple> buildQuery(boolean count, List<Long> selectedPlateformesIds, List<Long> especeIds, DatesYearsContinuousFormParamVO dateRequestParamVO, IUser user) {
        CriteriaQuery<Tuple> criteriaQuery = builder.createTupleQuery();
        Root<ValeurPem> v = criteriaQuery.from(ValeurPem.class);
        Join<ValeurPem, MesurePem> m = v.join(ValeurPem_.mesurePem);
        Join<MesurePem, SequencePem> s = m.join(MesurePem_.sequencePem);
        Join<MesurePem, Espece> espece = m.join(MesurePem_.espece);
        Join<ValeurPem, RealNode> rnVariable = v.join(ValeurPem_.realNode);
        Root<NodeDataSet> ndsVariable = criteriaQuery.from(NodeDataSet.class);
        Join<NodeDataSet, AbstractBranchNode> ndsDatatype = ndsVariable.join(NodeDataSet_.parent);
        Join<AbstractBranchNode, AbstractBranchNode> ndsTheme = ndsDatatype.join(NodeDataSet_.parent);
        Join<AbstractBranchNode, AbstractBranchNode> ndsSite = ndsTheme.join(NodeDataSet_.parent);
        Join<AbstractBranchNode, RealNode> rnSite = ndsSite.join(NodeDataSet_.realNode);
        Join<RealNode, Nodeable> site = rnSite.join(RealNode_.nodeable);
        Join<RealNode, Nodeable> bv = rnSite.join(RealNode_.ancestor).join(RealNode_.nodeable);
        List<Predicate> and = new LinkedList();
        final Map<String, LocalDate> datesMap = dateRequestParamVO.retrieveParametersMap();
        and.add(builder.between(
                s.<LocalDate>get(SequencePem_.date),
                datesMap.get(DatesYearsContinuousFormParamVO.START_INDEX + "0"),
                datesMap.get(DatesYearsContinuousFormParamVO.END_INDEX + "0"))
        );
        and.add(builder.equal(ndsVariable.get(NodeDataSet_.realNode), rnVariable));
        and.add(espece.get(Espece_.id).in(especeIds));
        and.add(site.get(Nodeable_.id).in(selectedPlateformesIds));
        if (!user.getIsRoot()) {
            addRestrictiveRequestOnRoles(user, criteriaQuery, and, builder, ndsVariable, s.<LocalDate>get(SequencePem_.date));
        }
        criteriaQuery.where(builder.and(and.toArray(new Predicate[and.size()])));
        criteriaQuery.distinct(true);
        if (count) {
            criteriaQuery.select(builder.tuple(builder.count(m)));
        } else {
            criteriaQuery.select(
                    builder.tuple(
                            m, site.get(Nodeable_.code),
                            bv.get(Nodeable_.code),
                            s.get(SequencePem_.date),
                            espece.get(Espece_.code)
                    ));
            criteriaQuery.orderBy(
                    builder.asc(site.get(Nodeable_.code)),
                    builder.asc(bv.get(Nodeable_.code)),
                    builder.asc(s.get(SequencePem_.date)),
                    builder.asc(espece.get(Espece_.code))
            );
        }

        return getResultList(criteriaQuery);
    }

    /**
     *
     * @param user
     * @param criteria
     * @param predicatesAnd
     * @param builder
     * @param vns
     * @param dateMesure
     */
    protected void addRestrictiveRequestOnRoles(IUser user, CriteriaQuery criteria, List<Predicate> predicatesAnd, CriteriaBuilder builder, Path<NodeDataSet> vns, final Path<LocalDate> dateMesure) {
        if (!user.getIsRoot()) {
            Root<ExtractActivity> er = criteria.from(ExtractActivity.class);
            predicatesAnd.add(builder.equal(er.get(ExtractActivity_.login), user.getLogin()));
            predicatesAnd.add(builder.equal(er.get(ExtractActivity_.idNode), vns.get(NodeDataSet_.id)));
            predicatesAnd.add(builder.between(dateMesure, er.<LocalDate>get(ExtractActivity_.dateStart), er.<LocalDate>get(ExtractActivity_.dateEnd)));
        }
    }

    /**
     *
     * @param plateformeId
     * @param dateStart
     * @param dateEnd
     * @return
     */
    @Override
    public List<Espece> retrievePemAvailablesEspeces(List<Long> plateformeId, LocalDate dateStart, LocalDate dateEnd) {
        CriteriaQuery<Espece> criteriaQuery = builder.createQuery(Espece.class);
        Root<ValeurPem> v = criteriaQuery.from(ValeurPem.class);
        Join<ValeurPem, MesurePem> m = v.join(ValeurPem_.mesurePem);
        m.join(MesurePem_.sequencePem);
        Join<MesurePem, Espece> espece = m.join(MesurePem_.espece);
        criteriaQuery
                .distinct(true)
                .select(espece);
        return getResultList(criteriaQuery);
    }

    /**
     *
     * @param selectedPlateformesNodesIds
     * @param especeIds
     * @param dateRequestParamVO
     * @param user
     * @return
     */
    @Override
    public Long getExtractionSize(List<Long> selectedPlateformesNodesIds, List<Long> especeIds, DatesYearsContinuousFormParamVO dateRequestParamVO, IUser user) {
        List<Tuple> tuples = buildQuery(true, selectedPlateformesNodesIds, especeIds, dateRequestParamVO, user);
        if (tuples.isEmpty()) {
            LOGGER.error("can't find result size");
            return 1_000_000L;
        } else {
            return (Long) tuples.get(0).get(0);
        }
    }
}

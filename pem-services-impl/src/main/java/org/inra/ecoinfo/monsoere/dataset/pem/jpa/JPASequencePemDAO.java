package org.inra.ecoinfo.monsoere.dataset.pem.jpa;

import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.monsoere.dataset.pem.ISequencePemDAO;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.SequencePem;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.SequencePem_;

/**
 * The Class JPASequencePemDAO.
 */
public class JPASequencePemDAO extends AbstractJPADAO<SequencePem> implements ISequencePemDAO {

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.monsoere.dataset.pem.ISequencePemDAO#getByDateAndProjetSite(java.time.LocalDateTime,
     * org.inra.ecoinfo.monsoere.refdata.projetsite.ProjetSite)
     */
    /**
     *
     * @param date
     * @param version
     * @return
     */
    @Override
    public Optional<SequencePem> getByDateAndVersion(LocalDate date, VersionFile version) {
        CriteriaQuery<SequencePem> query = builder.createQuery(SequencePem.class);
        Root<SequencePem> sequence = query.from(SequencePem.class);
        query
                .select(sequence)
                .where(
                        builder.equal(sequence.get(SequencePem_.date), date),
                        builder.equal(sequence.get(SequencePem_.version), version)
                );
        return getOptional(query);
    }

}

package org.inra.ecoinfo.monsoere.dataset.pem.jpa;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity_;
import org.inra.ecoinfo.monsoere.dataset.pem.IValeurPemDAO;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.ValeurPem;
import org.inra.ecoinfo.monsoere.synthesis.pem.SynthesisValue;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite_;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue_;

/**
 * The Class JPAValeurPemDAO.
 */
public class JPAValeurPemDAO extends AbstractJPADAO<ValeurPem> implements IValeurPemDAO {

    /**
     *
     * @param sites
     * @param dateStart
     * @param dateEnd
     * @param user
     * @return
     */
    @Override
    public List<INode> getAvailablesVariablesFilteredByRights(Set<String> sites, LocalDate dateStart, LocalDate dateEnd, IUser user) {
        if (CollectionUtils.isEmpty(sites)) {
            return null;
        }

        ArrayList<Predicate> and = new ArrayList();
        
        CriteriaQuery<INode> query = builder.createQuery(INode.class);
        Root<NodeDataSet> nds = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> variableRn = nds.join(NodeDataSet_.realNode);
        Join<RealNode, RealNode> datatypeRn = variableRn.join(RealNode_.parent);
        Join<RealNode, RealNode> themeRn = datatypeRn.join(RealNode_.parent);
        Join<RealNode, RealNode> siteRn = themeRn.join(RealNode_.parent);
        Root<DatatypeVariableUnite> dvu = query.from(DatatypeVariableUnite.class);
        Root<SynthesisValue> sv = query.from(SynthesisValue.class);
        and.add(siteRn.get(RealNode_.path).in(sites));
        and.add(sv.get(GenericSynthesisValue_.site).in(sites));
        and.add(builder.between(sv.<LocalDateTime>get(GenericSynthesisValue_.date), dateStart.atStartOfDay(), dateEnd.atStartOfDay()));
        and.add(builder.equal(variableRn.get(RealNode_.nodeable), dvu));
        and.add(builder.like(sv.get(GenericSynthesisValue_.variable), dvu.get(DatatypeVariableUnite_.code)));
        if (!user.getIsRoot()) {
            addRestrictiveRequestOnRoles(user, query, and, builder, nds, sv.<LocalDateTime>get(GenericSynthesisValue_.date));
        }
        query.where(builder.and(and.toArray(new Predicate[]{})));
        query.select(nds);
        query.distinct(true);
        return getResultList(query);
    }

    /**
     *
     * @param user
     * @param criteria
     * @param predicatesAnd
     * @param builder
     * @param vns
     * @param dateMesure
     */
    @SuppressWarnings("unchecked")
    protected void addRestrictiveRequestOnRoles(IUser user, CriteriaQuery criteria, List<Predicate> predicatesAnd, CriteriaBuilder builder, Path<NodeDataSet> vns, final Path dateMesure) {
        if (!user.getIsRoot()) {
            Root<ExtractActivity> er = criteria.from(ExtractActivity.class);
            predicatesAnd.add(builder.equal(er.get(ExtractActivity_.login), user.getLogin()));
            predicatesAnd.add(builder.equal(er.get(ExtractActivity_.idNode), vns.get(NodeDataSet_.id)));
            predicatesAnd.add(builder.between(dateMesure, er.<LocalDate>get(ExtractActivity_.dateStart), er.<LocalDate>get(ExtractActivity_.dateEnd)));
        }
    }

    /**
     *
     * @param user
     * @return
     */
    @Override
    public List<INode> getSitesAvailables(IUser user) {
        ArrayList<Predicate> and = new ArrayList();
        CriteriaQuery<INode> query = builder.createQuery(INode.class);
        Root<NodeDataSet> nds = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> siteRn = nds.join(NodeDataSet_.realNode);
        Root<SynthesisValue> sv = query.from(SynthesisValue.class);
        and.add(builder.equal(sv.get(GenericSynthesisValue_.site), siteRn.get(RealNode_.path)));
        if (!user.getIsRoot()) {
            addRestrictiveRequestOnRoles(user, query, and, builder, nds, sv.<LocalDateTime>get(GenericSynthesisValue_.date));
        }
        query.where(builder.and(and.toArray(new Predicate[]{})));
        query.select(nds);
        query.distinct(true);
        return getResultList(query);
    }
}

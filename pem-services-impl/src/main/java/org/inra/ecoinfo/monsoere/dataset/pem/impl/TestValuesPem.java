package org.inra.ecoinfo.monsoere.dataset.pem.impl;

import com.google.common.base.Strings;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.monsoere.dataset.impl.GenericTestValues;
import org.inra.ecoinfo.monsoere.dataset.impl.RecorderMonSoere;
import org.inra.ecoinfo.monsoere.refdata.projet.Projet;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class TestValuesPem.
 */
public class TestValuesPem extends GenericTestValues {

    /**
     * The property msg not a project.
     */
    protected static final String PROPERTY_MSG_NOT_A_PROJECT = "PROPERTY_MSG_NOT_A_PROJECT";
    /**
     * The property msg not a site.
     */
    protected static final String PROPERTY_MSG_NOT_A_SITE = "PROPERTY_MSG_NOT_A_SITE";
    /**
     * The property msg bad interval.
     */
    protected static final String PROPERTY_MSG_BAD_INTERVAL = "PROPERTY_MSG_BAD_INTERVAL";
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    private void checkDate(final BadsFormatsReport badsFormatsReport,
            final VersionFile versionFile, String value, final Column column) {
        String format = Strings.isNullOrEmpty(column.getFormatType()) ? DateUtil.DD_MM_YYYY_FILE : column.getFormatType();
        try {
            final LocalDateTime date = DateUtil.readLocalDateTimeFromText(format, value);
            final LocalDateTime dateStart = versionFile.getDataset().getDateDebutPeriode();
            final LocalDateTime dateEnd = versionFile.getDataset().getDateFinPeriode();
            if (date.isBefore(dateStart) || date.isAfter(dateEnd)) {
                badsFormatsReport.addException(new BusinessException(String.format(
                        RecorderMonSoere
                                .getMonSoereMessage(TestValuesPem.PROPERTY_MSG_BAD_INTERVAL), value)));
            }
        } catch (final DateTimeException e) {
            LOGGER.info("can't check value", e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.monsoere.dataset.impl.GenericTestValues#checkValue(org.inra.ecoinfo.utils
     * .exceptions.BadsFormatsReport, org.inra.ecoinfo.dataset.versioning.entity.VersionFile, long,
     * int, java.lang.String, org.inra.ecoinfo.utils.Column)
     */
    @Override
    protected void checkValue(final BadsFormatsReport badsFormatsReport,
            final VersionFile versionFile, final long lineNumber, final int i, String value,
            final Column column) {
        super.checkValue(badsFormatsReport, versionFile, lineNumber, i, value, column);
        final String name = column.getName();
        final RealNode pstd = versionFile.getDataset()
                .getRealNode().getNodeByNodeableTypeResource(DataType.class);
        switch (name) {
            case "projet":
                if (!pstd.getNodeByNodeableTypeResource(Projet.class).getCode().equals(Utils.createCodeFromString(value))) {
                    badsFormatsReport.addException(new BusinessException(String.format(
                            RecorderMonSoere.getMonSoereMessage(TestValuesPem.PROPERTY_MSG_NOT_A_PROJECT),
                            value)));
                }
                break;
            case "site":
                if (!pstd.getNodeByNodeableTypeResource(Site.class).getCode().replaceAll("/.*$", "").equals(Utils.createCodeFromString(value))) {
                    badsFormatsReport.addException(new BusinessException(String.format(
                            RecorderMonSoere.getMonSoereMessage(TestValuesPem.PROPERTY_MSG_NOT_A_SITE),
                            value)));
                }
                break;
            case "plateforme":
                if (!pstd.getNodeByNodeableTypeResource(Site.class).getCode().replaceAll("^.*?/", "").equals(Utils.createCodeFromString(value))) {
                    badsFormatsReport.addException(new BusinessException("no platform"));
                }
                break;
            case "date":
                checkDate(badsFormatsReport, versionFile, value, column);
                break;
            default:
                break;
        }
    }
}

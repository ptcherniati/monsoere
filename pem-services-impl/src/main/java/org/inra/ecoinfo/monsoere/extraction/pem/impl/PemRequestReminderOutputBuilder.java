package org.inra.ecoinfo.monsoere.extraction.pem.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.IMotivation;
import org.inra.ecoinfo.extraction.IMotivationFormatter;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.filecomp.config.impl.FileCompConfiguration;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.monsoere.extraction.AbstractDatesFormParam;
import org.inra.ecoinfo.monsoere.extraction.DatesYearsContinuousFormParamVO;
import org.inra.ecoinfo.monsoere.refdata.espece.Espece;
import org.inra.ecoinfo.monsoere.refdata.projet.Projet;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.MDC;

/**
 * The Class PemRequestReminderOutputBuilder.
 */
public class PemRequestReminderOutputBuilder extends AbstractOutputBuilder  implements IMotivationFormatter{

    /**
     * The Constant CST_RESULT_EXTRACTION_CODE.
     */
    public static final String CST_RESULT_EXTRACTION_CODE = "pemRequestReminder";
    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.monsoere.extraction.pem.messages";
    /**
     * The Constant MSG_EXTRACTION_COMMENTS.
     */
    private static final String MSG_EXTRACTION_COMMENTS = "PROPERTY_MSG_EXTRACTION_COMMENTS";
    /**
     * The Constant MSG_HEADER.
     */
    private static final String MSG_HEADER = "PROPERTY_MSG_HEADER";
    /**
     * The Constant PATTERN_STRING_PLATEFORMS_SUMMARY.
     */
    private static final String PATTERN_STRING_PLATEFORMS_SUMMARY = "   %s (%s: %s)";
    /**
     * The Constant PATTERN_STRING_SPECIESS_SUMMARY.
     */
    private static final String PATTERN_STRING_SPECIESS_SUMMARY = "   %s";
    /**
     * The Constant PATTERN_STRING_HEADER_PLATEFORMS_SUMMARY.
     */
    private static final String PATTERN_STRING_HEADER_PLATEFORMS_SUMMARY = "%s%n   %s";
    /**
     * The Constant PATTERN_STRING_COMMENTS_SUMMARY.
     */
    private static final String PATTERN_STRING_COMMENTS_SUMMARY = "   %s";
    /**
     * The Constant MSG_SELECTED_PERIODS.
     */
    private static final String MSG_SELECTED_PERIODS = "PROPERTY_MSG_SELECTED_PERIODS";
    /**
     * The Constant MSG_SELECTED_SPECIESS.
     */
    private static final String MSG_SELECTED_SPECIESS = "PROPERTY_MSG_SELECTED_SPECIESS";
    /**
     * The Constant MSG_SITE.
     */
    private static final String MSG_SITE = "PROPERTY_MSG_SITE";
    /**
     * The Constant MSG_SELECTED_PROJECT.
     */
    private static final String MSG_SELECTED_PROJECT = "PROPERTY_MSG_SELECTED_PROJECT";
    /**
     * The Constant MSG_SELECTED_PLATEFORMS.
     */
    private static final String MSG_SELECTED_PLATEFORMS = "PROPERTY_MSG_SELECTED_PLATEFORMS";
    /**
     * The Constant KEYMAP_COMMENTS.
     */
    private static final String KEYMAP_COMMENTS = "comments";
    private static final String MSG_SELECTED_FILES = "PROPERTY_MSG_SELECTED_FILES";
    private static final String PROPERTY_MSG_PIEGEAGE_EN_MONTEE = "piegeage_en_montee";
    private static final String PATTERN_STRING_FILES_SUMMARY = "   %s";

    /**
     *
     */
    protected FileCompConfiguration fileCompConfiguration;

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildBody(java.lang.String,
     * java.util.Map, java.util.Map)
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap,
            Map<String, Object> requestMetadatasMap) throws BusinessException {
        final Map<String, File> reminderMap = new HashMap<>();
        final File reminderFile = buildOutputFile(AbstractOutputBuilder.FILENAME_REMINDER,
                AbstractOutputBuilder.EXTENSION_TXT);
        PrintStream reminderPrintStream;
        try {
            reminderPrintStream = new PrintStream(reminderFile,
                    Utils.ENCODING_ISO_8859_15);
            reminderPrintStream.println(headers);
            reminderPrintStream.println();
            final List<IFlatNode> plateformes = (List<IFlatNode>) requestMetadatasMap.get(Site.class.getSimpleName());
            final IFlatNode projet = (IFlatNode) requestMetadatasMap.get(Projet.class.getSimpleName());
            printPlateformesSummary(projet, plateformes,
                    reminderPrintStream);
            printDatesSummary(
                    (DatesYearsContinuousFormParamVO) requestMetadatasMap.get(DatesYearsContinuousFormParamVO.class
                            .getSimpleName()), reminderPrintStream);
            printEspecesSummary(
                    (List<Espece>) requestMetadatasMap.get(Espece.class.getSimpleName()),
                    reminderPrintStream);

            printFilecompSummary(
                    (List<FileComp>) requestMetadatasMap.get(FileComp.class.getSimpleName()),
                    reminderPrintStream);
            reminderPrintStream.println(getLocalizationManager().getMessage(
                    PemRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                    PemRequestReminderOutputBuilder.MSG_EXTRACTION_COMMENTS));
            reminderPrintStream.println(String.format(
                    PemRequestReminderOutputBuilder.PATTERN_STRING_COMMENTS_SUMMARY,
                    requestMetadatasMap.get(PemRequestReminderOutputBuilder.KEYMAP_COMMENTS)));
        } catch (final FileNotFoundException e) {
            throw new BusinessException("file not found ", e);
        } catch (final UnsupportedEncodingException e) {
            throw new BusinessException("unsupporting encoding", e);
        }
        reminderMap.put(AbstractOutputBuilder.FILENAME_REMINDER, reminderFile);
        reminderPrintStream.flush();
        reminderPrintStream.close();
        return reminderMap;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildHeader(java.util.Map)
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        return getLocalizationManager().getMessage(
                PemRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                PemRequestReminderOutputBuilder.MSG_HEADER);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo.extraction.IParameter
     * )
     */
    /**
     *
     * @param parameters
     * @return
     * @throws BusinessException
     */
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        ((DefaultParameter) parameters).getFilesMaps().add(
                super.buildOutput(parameters,
                        PemRequestReminderOutputBuilder.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    /**
     * Prints the dates summary.
     * <p>
     *
     * @param datesRequestParamVO the dates request param vo
     * @param reminderPrintStream the reminder print stream
     * <p>
     * @throws BusinessException the business exception
     */
    private void printDatesSummary(DatesYearsContinuousFormParamVO datesRequestParamVO,
            PrintStream reminderPrintStream) throws BusinessException {
        reminderPrintStream.println(getLocalizationManager().getMessage(
                PemRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                PemRequestReminderOutputBuilder.MSG_SELECTED_PERIODS));
        try {
            MDC.put("startDate", datesRequestParamVO.getPeriods().get(0).get(AbstractDatesFormParam.START_INDEX));
            MDC.put("endDate", datesRequestParamVO.getPeriods().get(0).get(AbstractDatesFormParam.END_INDEX));
            datesRequestParamVO.buildSummary(reminderPrintStream);
        } catch (final DateTimeParseException e) {
            throw new BusinessException("can't build summary", e);
        }
        reminderPrintStream.println();
    }

    /**
     * Prints the especes summary.
     * <p>
     *
     * @param list the list
     * @param reminderPrintStream the reminder print stream
     */
    private void printEspecesSummary(List<Espece> list, PrintStream reminderPrintStream) {
        final Properties propertiesNomEspece = getLocalizationManager().newProperties(
                Espece.NAME_ENTITY_JPA, "nom");
        final Properties propertiesNomDatatype = getLocalizationManager().newProperties(
                DataType.NAME_ENTITY_JPA, "name");
        reminderPrintStream.println(String.format(
                getLocalizationManager().getMessage(
                        PemRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                        PemRequestReminderOutputBuilder.MSG_SELECTED_SPECIESS),
                propertiesNomDatatype.getProperty(
                        PemRequestReminderOutputBuilder.PROPERTY_MSG_PIEGEAGE_EN_MONTEE,
                        PemRequestReminderOutputBuilder.PROPERTY_MSG_PIEGEAGE_EN_MONTEE)));
        MDC.put("especes", list.stream()
                .map(e->String.format(
                    PemRequestReminderOutputBuilder.PATTERN_STRING_SPECIESS_SUMMARY,
                    propertiesNomEspece.getProperty(e.getNom(), e.getNom())))
                .peek(m-> reminderPrintStream.println(m))
                .collect(Collectors.joining(",")));
        reminderPrintStream.println();
    }

    private void printFilecompSummary(List<FileComp> list, PrintStream reminderPrintStream) {
        if (list == null || list.isEmpty()) {
            return;
        }
        final Properties propertiesNomFileComp = getLocalizationManager().newProperties(
                FileComp.NAME_ENTITY_JPA, "fileName");
        reminderPrintStream.println(getLocalizationManager().getMessage(
                PemRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                PemRequestReminderOutputBuilder.MSG_SELECTED_FILES));
        MDC.put("files", list.stream()
                .map(f->String.format(
                    PemRequestReminderOutputBuilder.PATTERN_STRING_FILES_SUMMARY,
                    propertiesNomFileComp.getProperty(f.getFileName(), f.getFileName())))
                .peek(m->reminderPrintStream.println(m))
                .collect(Collectors.joining(",")));
        reminderPrintStream.println();
    }

    /**
     * Prints the plateformes summary.
     * <p>
     *
     * @param projet the projet
     * @param listPlateforme the list
     * @param reminderPrintStream the reminder print stream
     */
    private void printPlateformesSummary(IFlatNode projet, List<IFlatNode> listPlateforme,
            PrintStream reminderPrintStream) {

        final Properties propertiesNomProjet = getLocalizationManager().newProperties(
                Projet.TABLE_NAME, "nom");
        final Properties propertiesNomSite = getLocalizationManager().newProperties(
                Site.NAME_ENTITY_JPA, "nom");
        reminderPrintStream.println(String.format(
                PemRequestReminderOutputBuilder.PATTERN_STRING_HEADER_PLATEFORMS_SUMMARY,
                getLocalizationManager().getMessage(
                        PemRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                        PemRequestReminderOutputBuilder.MSG_SELECTED_PROJECT),
                propertiesNomProjet.getProperty(projet.getName(), projet.getName())));
        reminderPrintStream.println();
        reminderPrintStream.println(getLocalizationManager().getMessage(
                PemRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                PemRequestReminderOutputBuilder.MSG_SELECTED_PLATEFORMS));
        MDC.put("plateform",listPlateforme.stream()
                .map(p->String.format(
                        PemRequestReminderOutputBuilder.PATTERN_STRING_PLATEFORMS_SUMMARY,
                        propertiesNomSite.getProperty(p.getName(), p.getName()),
                        getLocalizationManager().getMessage(
                                PemRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                                PemRequestReminderOutputBuilder.MSG_SITE),
                        propertiesNomSite.getProperty(p.getName(), p.getName())))
                .peek(m->reminderPrintStream.println(String.format(m)))
                .collect(Collectors.joining(",")));
        reminderPrintStream.println();
    }

    /**
     *
     * @param fileCompConfiguration
     */
    public void setFileCompConfiguration(FileCompConfiguration fileCompConfiguration) {
        this.fileCompConfiguration = fileCompConfiguration;
    }

    @Override
    public void formatMotivation(IParameter parameters, IMotivation motivation, ILocalizationManager localizationManager, Utilisateur user) {

        MDC.put("date", DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.DD_MM_YYYY_HH_MM_SS));
        MDC.put("login", user.getLogin());
        MDC.put("name", user.getNom());
        MDC.put("prenom", user.getPrenom());
        MDC.put("email", user.getEmail());
        MDC.put("motivation", motivation.getMotivation());
        MDC.put("extractionType", parameters.getExtractionTypeCode());
    }
}

package org.inra.ecoinfo.monsoere.extraction.pem.impl;

import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class PemOutputDisplayBySite.
 */
public class PemOutputDisplayBySite extends PemOutputBuilderResolver {

    /**
     * The pem output builder by site.
     */
    private IOutputBuilder pemOutputBuilderBySite;

    /**
     * Instantiates a new pem output display by site.
     */
    public PemOutputDisplayBySite() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.monsoere.extraction.pem.impl.PemOutputBuilderResolver#buildOutput(org.inra
     * .ecoinfo.extraction.IParameter)
     */

    /**
     *
     * @param parameters
     * @return
     * @throws BusinessException
     */

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        super.setPemOutputBuilder(pemOutputBuilderBySite);
        return super.buildOutput(parameters);
    }

    /**
     * Sets the pem output builder by site.
     * <p>
     *
     * @param pemOutputBuilderByRow
     *            the new pem output builder by site
     */
    public void setPemOutputBuilderBySite(IOutputBuilder pemOutputBuilderByRow) {
        this.pemOutputBuilderBySite = pemOutputBuilderByRow;
    }
}

package org.inra.ecoinfo.monsoere.extraction.pem.impl;

import com.google.common.base.Strings;
import java.io.File;
import java.io.PrintStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.MesurePem;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.ValeurPem;
import org.inra.ecoinfo.monsoere.refdata.projet.Projet;
import org.inra.ecoinfo.monsoere.refdata.site.SiteMonSoere;
import org.inra.ecoinfo.monsoere.refdata.variable.VariableMonSoere;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class PemOutputBuilderByRow.
 */
public class PemOutputBuilderByRow extends AbstractOutputBuilder {

    /**
     * The Constant BUNDLE_SOURCE_PATH_PEM.
     */
    protected static final String BUNDLE_SOURCE_PATH_PEM = "org.inra.ecoinfo.monsoere.extraction.pem.messages";
    /**
     * The Constant MAP_INDEX_0.
     */
    protected static final String MAP_INDEX_0 = "0";
    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.monsoere.dataset.pem.messages";
    /**
     * The Constant PROPERTY_MSG_ERROR.
     */
    private static final String PROPERTY_MSG_ERROR = "PROPERTY_MSG_ERROR";
    /**
     * The Constant HEADER_DATA.
     */
    private static final String HEADER_DATA = "PROPERTY_MSG_HEADER_RAW_PEM_ROW";
    /**
     * The Constant MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS.
     */
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE";
    /**
     * The Constant CODE_DATATYPE_PEM.
     */
    private static final String CODE_DATATYPE_PEM = "piegeage_en_montee";
    /**
     * The datatype variable unite dao.
     */
    private IDatatypeVariableUniteDAO datatypeVariableUniteDAO;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildBody(java.lang.String,
     * java.util.Map, java.util.Map)
     */
    /**
     *
     * @param headers
     * @param resultsDatasMap
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap,
            Map<String, Object> requestMetadatasMap) throws BusinessException {
        final List<MesurePem> mesuresPem = resultsDatasMap.get(PemOutputBuilderByRow.MAP_INDEX_0);
        final Properties propertiesValeurQualitativeValue = localizationManager.newProperties(
                IValeurQualitative.NAME_ENTITY_JPA, "valeur");
        final Properties propertiesProjetName = localizationManager.newProperties(
                Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        final Properties propertiesSiteName = localizationManager.newProperties(
                Nodeable.getLocalisationEntite(SiteMonSoere.class), Nodeable.ENTITE_COLUMN_NAME);
        final List<IFlatNode> siteNodes = (List<IFlatNode>) requestMetadatasMap.get(Site.class.getSimpleName());
        final Set<VariableMonSoere> selectedVariables = new TreeSet(
                (Collection) requestMetadatasMap.get(Variable.class
                        .getSimpleName()));
        final Set<String> sitesNames = retrieveSitesCodes(siteNodes);
        final Map<String, File> filesMap = buildOutputsFiles(sitesNames, AbstractOutputBuilder.EXTENSION_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        outputPrintStreamMap.entrySet().stream().forEach((siteNameEntry) -> {
            outputPrintStreamMap.get(siteNameEntry.getKey()).println(headers);
        });
        writeMesures(mesuresPem, propertiesValeurQualitativeValue, propertiesProjetName,
                propertiesSiteName, selectedVariables, outputPrintStreamMap);
        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildHeader(java.util.Map)
     */
    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport(getLocalizationManager().getMessage(
                PemOutputBuilderByRow.BUNDLE_SOURCE_PATH, PemOutputBuilderByRow.PROPERTY_MSG_ERROR));
        final Properties propertiesVariableName = localizationManager.newProperties(
                Nodeable.getLocalisationEntite(VariableMonSoere.class), Nodeable.ENTITE_COLUMN_NAME);
        final Set<VariableMonSoere> selectedVariables = new TreeSet(
                (Collection) requestMetadatasMap.get(Variable.class
                        .getSimpleName()));
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format(
                "%s",
                getLocalizationManager().getMessage(
                        PemOutputBuilderByRow.BUNDLE_SOURCE_PATH_PEM,
                        PemOutputBuilderByRow.HEADER_DATA)));
        for (final VariableMonSoere variable : selectedVariables) {
            if (variable == null) {
                errorsReport
                        .addException(new BusinessException(
                                String.format(
                                        getLocalizationManager()
                                                .getMessage(
                                                        PemOutputBuilderByRow.BUNDLE_SOURCE_PATH_PEM,
                                                        PemOutputBuilderByRow.MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS),
                                        "null variable")));
                continue;
            }
            String localizedVariableName = propertiesVariableName
                    .getProperty(variable.getName());
            if (Strings.isNullOrEmpty(localizedVariableName)) {
                localizedVariableName = variable.getName();
            }
            final Unite unite = datatypeVariableUniteDAO.getUnite(
                    PemOutputBuilderByRow.CODE_DATATYPE_PEM, variable.getCode()).orElse(null);
            String uniteNom;
            if (unite == null) {
                uniteNom = "nounit";
            } else {
                uniteNom = unite.getCode();
            }
            stringBuilder.append(String.format(";%s (%s)", localizedVariableName, uniteNom));
        }
        return stringBuilder.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo.extraction.IParameter
     * )
     */
    /**
     *
     * @param parameters
     * @return
     * @throws BusinessException
     */
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        ((DefaultParameter) parameters).getFilesMaps().add(
                super.buildOutput(parameters, PemExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    /**
     * Retrieve sites codes.
     * <p>
     *
     * @param selectedPlateformes the selected plateformes
     * <p>
     * @return the sets the
     */
    private Set<String> retrieveSitesCodes(List<IFlatNode> siteNodes) {
        final Set<String> sitesNames = new HashSet<>();
        siteNodes.stream().forEach((siteNode) -> {
            sitesNames
                    .add(Utils.createCodeFromString(siteNode.getCode().replace("/", "_")));
        });
        return sitesNames;
    }

    /**
     * Sets the datatype variable unite dao.
     * <p>
     *
     * @param datatypeVariableUniteDAO the new datatype variable unite dao
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    private void writeMesures(final List<MesurePem> mesuresPem,
            final Properties propertiesValeurQualitativeValue,
            final Properties propertiesProjetName, final Properties propertiesSiteName,
            final Set<VariableMonSoere> selectedVariables,
            final Map<String, PrintStream> outputPrintStreamMap) {

        mesuresPem.stream().map((mesurePem) -> {
            final RealNode nodeDataset = mesurePem.getSequencePem().getVersion().getDataset().getRealNode();
            SiteMonSoere site = (SiteMonSoere) nodeDataset.getNodeByNodeableTypeResource(SiteMonSoere.class).getNodeable();
            Projet projet = (Projet) nodeDataset.getNodeByNodeableTypeResource(Projet.class).getNodeable();
            final PrintStream rawDataPrintStream = outputPrintStreamMap.get(site.getCode().replace("/", "_"));
            String line;
            String localizedProjetName = propertiesProjetName.getProperty(projet.getCode(), projet.getCode());
            String localizedSiteName = propertiesSiteName.getProperty(site.getRootSite().getName(), site.getRootSite().getName());
            String localizedPlateformeName = propertiesSiteName.getProperty(site.getName(), site.getName());
            line = String.format(
                    "%s;%s;%s;%s;%s",
                    localizedProjetName,
                    localizedSiteName,
                    localizedPlateformeName,
                    DateUtil.getUTCDateTextFromLocalDateTime(mesurePem.getSequencePem().getDate().atStartOfDay(), DateUtil.DD_MM_YYYY),
                    mesurePem.getEspece().getNom());
            rawDataPrintStream.print(line);
            final List<ValeurPem> valeursPem = mesurePem.getValeursPem();
            writeVariables(propertiesValeurQualitativeValue, selectedVariables, rawDataPrintStream,
                    valeursPem);
            return rawDataPrintStream;
        }).forEach((rawDataPrintStream) -> {
            rawDataPrintStream.println();
        });
    }

    private void writeNonQualitativeValue(final PrintStream rawDataPrintStream,
            final ValeurPem valeurPem) {
        if (valeurPem.getValue() != null) {
            rawDataPrintStream.print(String.format("%s", valeurPem.getValue()));
        } else {
            rawDataPrintStream.print(String.format("%s", ""));
        }
    }

    private void writeQualitativeValue(final Properties propertiesValeurQualitativeValue,
            final PrintStream rawDataPrintStream, final ValeurPem valeurPem) {
        if (valeurPem.getValeurQualitative() != null) {
            String localizedValue = propertiesValeurQualitativeValue.getProperty(valeurPem
                    .getValeurQualitative().getValeur());
            if (Strings.isNullOrEmpty(localizedValue)) {
                localizedValue = valeurPem.getValeurQualitative().getValeur();
            }
            rawDataPrintStream.print(String.format("%s", localizedValue));
        } else {
            rawDataPrintStream.print(String.format("%s", ""));
        }
    }

    private void writeValues(final Properties propertiesValeurQualitativeValue,
            final PrintStream rawDataPrintStream, final List<ValeurPem> valeursPem,
            final VariableMonSoere variable) {
        valeursPem.stream().filter((valeurPem) -> {
            final DatatypeVariableUnite dvu = getDatatypeVariableunite(valeurPem);
            return dvu.getVariable().getId().equals(variable.getId());
        }).forEach((valeurPem) -> {
            if (getDatatypeVariableunite(valeurPem).getVariable().getIsQualitative()) {
                writeQualitativeValue(propertiesValeurQualitativeValue, rawDataPrintStream,
                        valeurPem);
            } else {
                writeNonQualitativeValue(rawDataPrintStream, valeurPem);
            }
        });
    }

    private DatatypeVariableUnite getDatatypeVariableunite(ValeurPem valeurPem) {
        return (DatatypeVariableUnite) valeurPem.getRealNode().getNodeable();
    }

    private void writeVariables(final Properties propertiesValeurQualitativeValue,
            final Set<VariableMonSoere> selectedVariables, final PrintStream rawDataPrintStream,
            final List<ValeurPem> valeursPem) {
        selectedVariables.stream().forEach((variable) -> {
            rawDataPrintStream.print(";");
            writeValues(propertiesValeurQualitativeValue, rawDataPrintStream, valeursPem, variable);
        });
    }
}

package org.inra.ecoinfo.monsoere.dataset.pem.impl;

import java.time.LocalDate;
import java.util.List;
import org.inra.ecoinfo.monsoere.dataset.impl.VariableValue;

/**
 * The Class LineRecord.
 */
public class LineRecord {

    /**
     * The projet code.
     */
    private String              projetCode;
    /**
     * The site code.
     */
    private String              siteCode;
    /**
     * The plateforme code.
     */
    private String              plateformeCode;
    /**
     * The date.
     */
    private LocalDate                date;
    /**
     * The espece.
     */
    private String              espece;
    /**
     * The variable values.
     */
    private List<VariableValue> variableValues;
    /**
     * The original line number.
     */
    private int                 originalLineNumber;

    /**
     * Instantiates a new line record.
     *
     * @param projetCode
     *            the projet code
     * @param siteCode
     *            the site code
     * @param plateformeCode
     *            the plateforme code
     * @param date
     *            the date
     * @param espece
     *            the espece
     * @param variableValues
     *            the variable values
     * @param originalLineNumber
     *            the original line number
     */
    public LineRecord(final String projetCode, final String siteCode, final String plateformeCode,
            final LocalDate date, final String espece, final List<VariableValue> variableValues,
            final int originalLineNumber) {
        this.projetCode = projetCode;
        this.siteCode = siteCode;
        this.plateformeCode = plateformeCode;
        this.date = date;
        this.espece = espece;
        this.variableValues = variableValues;
        this.originalLineNumber = originalLineNumber;
    }

    /**
     * Copy.
     *
     * @param line
     *            the line
     */
    public void copy(final LineRecord line) {
        this.projetCode = line.getProjetCode();
        this.siteCode = line.getSiteCode();
        this.plateformeCode = line.getPlateformeCode();
        this.date = line.getDate();
        this.espece = line.getEspece();
        this.variableValues = line.getVariableValues();
        this.originalLineNumber = line.getOriginalLineNumber();
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * Gets the espece.
     *
     * @return the espece
     */
    public String getEspece() {
        return espece;
    }

    /**
     * Gets the original line number.
     *
     * @return the original line number
     */
    public int getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     * Gets the plateforme code.
     *
     * @return the plateforme code
     */
    public String getPlateformeCode() {
        return plateformeCode;
    }

    /**
     * Gets the projet code.
     *
     * @return the projet code
     */
    public String getProjetCode() {
        return projetCode;
    }

    /**
     * Gets the site code.
     *
     * @return the site code
     */
    public String getSiteCode() {
        return siteCode;
    }

    /**
     * Gets the variable values.
     *
     * @return the variable values
     */
    public List<VariableValue> getVariableValues() {
        return variableValues;
    }

    /**
     * Sets the date.
     *
     * @param date
     *            the new date
     */
    public void setDate(final LocalDate date) {
        this.date = date;
    }

    /**
     * Sets the espece.
     *
     * @param espece
     *            the new espece
     */
    public void setEspece(final String espece) {
        this.espece = espece;
    }

    /**
     * Sets the original line number.
     *
     * @param originalLineNumber
     *            the new original line number
     */
    public void setOriginalLineNumber(final int originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     * Sets the plateforme code.
     *
     * @param plateformeCode
     *            the new plateforme code
     */
    public void setPlateformeCode(final String plateformeCode) {
        this.plateformeCode = plateformeCode;
    }

    /**
     * Sets the projet code.
     *
     * @param projetCode
     *            the new projet code
     */
    public void setProjetCode(final String projetCode) {
        this.projetCode = projetCode;
    }

    /**
     * Sets the site code.
     *
     * @param siteCode
     *            the new site code
     */
    public void setSiteCode(final String siteCode) {
        this.siteCode = siteCode;
    }

    /**
     * Sets the variable values.
     *
     * @param variableValues
     *            the new variable values
     */
    public void setVariableValues(final List<VariableValue> variableValues) {
        this.variableValues = variableValues;
    }
}

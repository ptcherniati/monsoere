package org.inra.ecoinfo.monsoere.extraction.pem.impl;

import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class PemOutputDisplayByRow.
 */
public class PemOutputDisplayByRow extends PemOutputBuilderResolver {

    /**
     * The pem output builder by row.
     */
    private IOutputBuilder pemOutputBuilderByRow;

    /**
     * Instantiates a new pem output display by row.
     */
    public PemOutputDisplayByRow() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.monsoere.extraction.pem.impl.PemOutputBuilderResolver#buildOutput(org.inra
     * .ecoinfo.extraction.IParameter)
     */

    /**
     *
     * @param parameters
     * @return 
     * @throws BusinessException
     */
    
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        super.setPemOutputBuilder(pemOutputBuilderByRow);
        return super.buildOutput(parameters);
    }

    /**
     * Sets the pem output builder by row.
     * <p>
     *
     * @param pemOutputBuilderByRow the new pem output builder by row
     */
    public void setPemOutputBuilderByRow(IOutputBuilder pemOutputBuilderByRow) {
        this.pemOutputBuilderByRow = pemOutputBuilderByRow;
    }
}

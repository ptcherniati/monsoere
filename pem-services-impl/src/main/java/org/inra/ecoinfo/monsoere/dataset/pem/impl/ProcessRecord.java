package org.inra.ecoinfo.monsoere.dataset.pem.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.monsoere.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.monsoere.dataset.impl.CleanerValues;
import org.inra.ecoinfo.monsoere.dataset.impl.RecorderMonSoere;
import org.inra.ecoinfo.monsoere.dataset.impl.VariableValue;
import org.inra.ecoinfo.monsoere.dataset.pem.ISequencePemDAO;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.MesurePem;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.SequencePem;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.ValeurPem;
import org.inra.ecoinfo.monsoere.refdata.espece.Espece;
import org.inra.ecoinfo.monsoere.refdata.espece.IEspeceDAO;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitativeDAO;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.system.Allocator;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class ProcessRecord.
 */
public class ProcessRecord extends AbstractProcessRecord {

    /**
     * The Constant VARIABLE_HEADER_INDEX.
     */
    protected static final int VARIABLE_HEADER_INDEX = 6;
    /**
     * The Constant PEM_DATATYPE.
     */
    private static final String PEM_DATATYPE = "piegeage_en_montee";
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The dataset descriptor.
     */
    private DatasetDescriptor datasetDescriptor;
    /**
     * The sequence pem dao.
     */
    private ISequencePemDAO sequencePemDAO;
    /**
     * The espece dao.
     */
    private IEspeceDAO especeDAO;
    /**
     * The valeur qualitative dao.
     */
    private IValeurQualitativeDAO valeurQualitativeDAO;

    /**
     * Builds the mesure.
     *
     * @param especeString the espece string
     * @param lines the lines
     * @param sequencePem the sequence pem
     * @param errorsReport the errors report
     * @throws InsertionDatabaseException the insertion database exception
     * @throws PersistenceException the persistence exception
     */
    private void buildMesure(SequencePem sequencePem, LineRecord line, Map<Variable, RealNode> dvus) throws BusinessException {
        final Espece espece = especeDAO.getEspeceByCode(Utils.createCodeFromString(line.getEspece()))
                .orElseThrow(()->new BusinessException("can't find espece"));
        final MesurePem mesurePEM = new MesurePem();
        mesurePEM.setEspece(espece);
        mesurePEM.setSequencePem(sequencePem);
        sequencePem.getMesuresPem().add(mesurePEM);
        for (final VariableValue variableValue : line.getVariableValues()) {
            final ValeurPem valeurPem = new ValeurPem(mesurePEM);
            final Variable variable = variableValue.getDatatypeVariableUnite().getVariable();
            final RealNode realNode = datatypeVariableUniteDAO.mergeRealNode(dvus.get(variable));
            valeurPem.setRealNode(realNode);
            if (variable.getIsQualitative()) {
                final ValeurQualitative valeurQualitative = (ValeurQualitative) valeurQualitativeDAO
                        .getByCodeAndValue(variable.getCode(), Utils.createCodeFromString(variableValue.getValue()))
                        .orElseThrow(()->new BusinessException("can't find valeur qualitative"));
                valeurPem.setValue(valeurQualitative);
            } else {
                final Float value = Float.parseFloat(variableValue.getValue());
                valeurPem.setValue(value);
            }
        }
    }

    /**
     * Builds the sequence.
     *
     * @param date the date
     * @param site the site
     * @param plateforme the plateforme
     * @param lines the lines
     * @param versionFile the version file
     * @param errorsReport the errors report
     * @throws InsertionDatabaseException the insertion database exception
     * @throws PersistenceException the persistence exception
     */
    private void buildSequence(final LocalDate date, final String plateforme,
            final List<LineRecord> lines, final VersionFile versionFile,
            final ErrorsReport errorsReport, Map<Variable, RealNode> dvus) throws BusinessException {
        SequencePem sequencePem = sequencePemDAO.getByDateAndVersion(date, versionFile)
                .orElse(null);
        if (sequencePem == null) {
            sequencePem = new SequencePem();
            sequencePem.setDate(date);
            sequencePem.setVersion(versionFile);
        }
        for (final LineRecord line : lines) {
            buildMesure(sequencePem, line, dvus);
        }
        if (!errorsReport.hasErrors()) {
            try {
                sequencePemDAO.saveOrUpdate(sequencePem);
            } catch (final PersistenceException e) {
                final String message = String.format(RecorderMonSoere
                        .getMonSoereMessage(RecorderMonSoere.PROPERTY_MSG_DUPLICATE_SEQUENCE),
                        DateUtil.getUTCDateTextFromLocalDateTime(date.atStartOfDay(), DateUtil.YYYY_MM_DD), 
                        plateforme);
                errorsReport.addException(new BusinessException(message, e));
            }
        }
    }

    /**
     * Fill lines map.
     *
     * @param linesMap the lines map
     * @param line the line
     * @param keyLine the key line
     */
    private void fillLinesMap(final Map<String, List<LineRecord>> linesMap, final LineRecord line,
            final String keyLine) {
        List<LineRecord> lines = null;
        if (keyLine != null && keyLine.length() > 0) {
            if (linesMap.containsKey(keyLine)) {
                linesMap.get(keyLine).add(line);
            } else {
                lines = new LinkedList<>();
                lines.add(line);
                linesMap.put(keyLine, lines);
            }
        }
    }

    /**
     * Gets the dataset descriptor.
     *
     * @return the dataset descriptor
     */
    public DatasetDescriptor getDatasetDescriptor() {
        return datasetDescriptor;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.monsoere.dataset.IProcessRecord#lookupDatasetDescriptor(java.lang.String)
     */
    @Override
    /**
     * @param datasetDescriptor : The path of "dataset-descriptor.xml" relative
     * to the current classpathLoader
     *
     */
    public DatasetDescriptor lookupDatasetDescriptor(String datasetDescriptorPath)
            throws IOException {
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(this.getClass()
                    .getResource(datasetDescriptorPath).openStream());
        }
        return datasetDescriptor;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.monsoere.dataset.impl.AbstractProcessRecord#processRecord(com.Ostermiller
     * .util.CSVParser, org.inra.ecoinfo.dataset.versioning.entity.VersionFile, java.lang.String,
     * org.inra.ecoinfo.utils.DatasetDescriptor)
     */

    /**
     *
     * @param parser
     * @param versionFile
     * @param fileEncoding
     * @param datasetDescriptor
     * @throws BusinessException
     */

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding,
            DatasetDescriptor datasetDescriptor) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport(
                RecorderMonSoere.getMonSoereMessage(RecorderMonSoere.PROPERTY_MSG_ERROR));
        Allocator allocator = Allocator.getInstance();
        try {
            int lineCount = 1;
            final List<DatatypeVariableUnite> dVU = buildVariableHeaderAndSkipHeader(
                    ProcessRecord.PEM_DATATYPE, ProcessRecord.VARIABLE_HEADER_INDEX, errorsReport,
                    parser);
            final Map<String, List<LineRecord>> sequenceMapLines = new ConcurrentHashMap<>();
            String[] values = parser.getLine();
            while (values != null) {
                final CleanerValues cleanerValues = new CleanerValues(values);
                final List<VariableValue> variableValues = new LinkedList<>();
                lineCount++;
                final String projetCode = Utils.createCodeFromString(cleanerValues.nextToken());
                final String siteCode = Utils.createCodeFromString(cleanerValues.nextToken());
                final String plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());
                final String dateString = cleanerValues.nextToken();
                final LocalDate date = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateString).toLocalDate();
                final String espece = cleanerValues.nextToken();
                for (int actualVariableArray = ProcessRecord.VARIABLE_HEADER_INDEX - 1; actualVariableArray < values.length; actualVariableArray++) {
                    variableValues.add(new VariableValue(dVU.get(actualVariableArray
                            - ProcessRecord.VARIABLE_HEADER_INDEX + 1), values[actualVariableArray]
                            .replaceAll(" ", "")));
                }
                final LineRecord line = new LineRecord(projetCode, siteCode, plateformeCode, date,
                        espece, variableValues, lineCount);
                final String key = siteCode.concat(plateformeCode).concat(dateString);
                fillLinesMap(sequenceMapLines, line, key);
                values = parser.getLine();
            }
            int count = 0;
            allocator.allocate("publish", versionFile.getFileSize());
            Iterator<Entry<String, List<LineRecord>>> iterator = sequenceMapLines.entrySet()
                    .iterator();
            Map<Variable, RealNode> dvus = datatypeVariableUniteDAO.getRealNodesVariables(versionFile.getDataset().getRealNode());
            while (iterator.hasNext()) {
                final Entry<String, List<LineRecord>> entry = iterator.next();
                final List<LineRecord> lines = sequenceMapLines.get(entry.getKey());
                if (!lines.isEmpty()) {
                    final LocalDate date = lines.get(0).getDate();
                    final String plateforme = lines.get(0).getPlateformeCode();
                    buildSequence(date, plateforme, lines, versionFile, errorsReport, dvus);
                    LOGGER.debug(String.format("%d - %s", count++, date));
                    sequencePemDAO.flush();
                    versionFile = versionFileDAO.merge(versionFile);
                }
                iterator.remove();
            }
        } catch (final IOException  | PersistenceException | InterruptedException e) {
            LOGGER.error("processRecord error", e);
        } finally {
            allocator.free("publish");
        }
    }

    /**
     * Sets the espece dao.
     *
     * @param especeDAO the new espece dao
     */
    public void setEspeceDAO(IEspeceDAO especeDAO) {
        this.especeDAO = especeDAO;
    }

    /**
     * Sets the sequence pem dao.
     *
     * @param sequencePemDAO the new sequence pem dao
     */
    public void setSequencePemDAO(ISequencePemDAO sequencePemDAO) {
        this.sequencePemDAO = sequencePemDAO;
    }

    /**
     * Sets the valeur qualitative dao.
     *
     * @param valeurQualitativeDAO the new valeur qualitative dao
     */
    public void setValeurQualitativeDAO(IValeurQualitativeDAO valeurQualitativeDAO) {
        this.valeurQualitativeDAO = valeurQualitativeDAO;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.inra.ecoinfo.monsoere.dataset.impl.AbstractProcessRecord#setVersionFileDAO(org.inra.ecoinfo
     * .dataset.versioning.IVersionFileDAO)
     */
    @Override
    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }
}

package org.inra.ecoinfo.monsoere.extraction.pem.impl;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IOutputsBuildersResolver;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Class PemOutputBuilderResolver.
 */
public class PemOutputBuilderResolver extends AbstractOutputBuilder implements IOutputsBuildersResolver, IOutputBuilder {
    /**
     * The Constant SEPARATOR_TEXT.
     */
    protected static final String SEPARATOR_TEXT = "_";
    /**
     * The Constant EXTENSION_ZIP.
     */
    protected static final String EXTENSION_ZIP = ".zip";
    /**
     * The request reminder output builder.
     */
    protected IOutputBuilder requestReminderOutputBuilder;
    /**
     * The pem output builder.
     */
    protected IOutputBuilder pemOutputBuilder;
    /**
     * The configuration.
     */
    protected IFileCompConfiguration fileCompConfiguration;

    /**
     * Adds the output builder by condition.
     * <p>
     *
     * @param condition the condition
     * @param outputBuilder the output builder
     * @param outputsBuilders the outputs builders
     */
    private void addOutputBuilderByCondition(Boolean condition, IOutputBuilder outputBuilder,
            List<IOutputBuilder> outputsBuilders) {
        if (condition) {
            outputsBuilders.add(outputBuilder);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo
     * .extraction.IParameter)
     */
    /**
     *
     * @param parameters
     * @return
     * @throws BusinessException
     */
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        RObuildZipOutputStream rObuildZipOutputStream ;
        try {
            rObuildZipOutputStream = super.buildOutput(parameters);
            final List<IOutputBuilder> listOutputs = resolveOutputsBuilders(parameters
                    .getParameters());
            final ZipOutputStream zipOutputStream = rObuildZipOutputStream.getZipOutputStream();
            NoExtractionResultException noDataToExtract = null;
            for (final IOutputBuilder iOutputBuilder : listOutputs) {
                iOutputBuilder.buildOutput(parameters);
            }
            for (final Map<String, File> filesMap : ((DefaultParameter) parameters)
                    .getFilesMaps()) {
                AbstractIntegrator.embedInZip(zipOutputStream, filesMap);
            }
            try {
                zipOutputStream.flush();
                zipOutputStream.close();
            } catch (final IOException e) {
                throw new BusinessException("io exception", e);
            }
            if (noDataToExtract != null) {
                throw noDataToExtract;
            }
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
        return rObuildZipOutputStream;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.extraction.IOutputsBuildersResolver#resolveOutputsBuilders
     * (java.util.Map)
     */
    @Override
    public List<IOutputBuilder> resolveOutputsBuilders(Map<String, Object> metadatasMap) {
        final List<IOutputBuilder> outputsBuilders = new LinkedList<>();
        addOutputBuilderByCondition(true, pemOutputBuilder, outputsBuilders);
        outputsBuilders.add(requestReminderOutputBuilder);
        return outputsBuilders;
    }

    /**
     *
     * @param fileCompConfiguration
     */
    public void setFileCompConfiguration(IFileCompConfiguration fileCompConfiguration) {
        this.fileCompConfiguration = fileCompConfiguration;
    }

    /**
     * Sets the pem output builder.
     * <p>
     *
     * @param pemDatasOutputBuilder the new pem output builder
     */
    public void setPemOutputBuilder(IOutputBuilder pemDatasOutputBuilder) {
        this.pemOutputBuilder = pemDatasOutputBuilder;
    }

    /**
     * Sets the request reminder output builder.
     * <p>
     *
     * @param requestReminderOutputBuilder the new request reminder output
     * builder
     */
    public void setRequestReminderOutputBuilder(IOutputBuilder requestReminderOutputBuilder) {
        this.requestReminderOutputBuilder = requestReminderOutputBuilder;
    }

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

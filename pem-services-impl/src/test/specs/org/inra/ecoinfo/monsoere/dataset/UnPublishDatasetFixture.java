package org.inra.ecoinfo.monsoere.dataset;


import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.monsoere.MonSoereTransactionalTestFixtureExecutionListener;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tcherniatinsky
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {MonSoereTransactionalTestFixtureExecutionListener.class})

public class UnPublishDatasetFixture extends org.inra.ecoinfo.dataset.UnPublishDatasetFixture {

    /**
     *
     */
    public UnPublishDatasetFixture() {
        super();
        MockitoAnnotations.initMocks(this);
    }
    
}

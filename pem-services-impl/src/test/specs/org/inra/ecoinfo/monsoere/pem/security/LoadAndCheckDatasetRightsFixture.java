package org.inra.ecoinfo.monsoere.pem.security;

import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.security.CheckRightsFixture;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {TransactionalTestFixtureExecutionListener.class})
public class LoadAndCheckDatasetRightsFixture extends CheckRightsFixture {

    /**
     *
     */
    public LoadAndCheckDatasetRightsFixture() {
        super();
        MockitoAnnotations.initMocks(this);
    }

    /**
     *
     * @return
     */
    @Override
    protected <T extends INodeable> Class<T> getTypeResourceVariable() {
        return (Class<T>) DatatypeVariableUnite.class;
    }
}

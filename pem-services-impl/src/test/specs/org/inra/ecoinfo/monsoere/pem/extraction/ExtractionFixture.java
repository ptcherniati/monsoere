package org.inra.ecoinfo.monsoere.pem.extraction;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.mga.business.composite.FlatNode;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.monsoere.MonSoereTransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.monsoere.extraction.AbstractDatesFormParam;
import org.inra.ecoinfo.monsoere.extraction.DatesYearsContinuousFormParamVO;
import org.inra.ecoinfo.monsoere.extraction.pem.impl.PemParameters;
import org.inra.ecoinfo.monsoere.refdata.espece.Espece;
import org.inra.ecoinfo.monsoere.refdata.projet.Projet;
import org.inra.ecoinfo.monsoere.refdata.variable.VariableMonSoere;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tcherniatinsky
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {MonSoereTransactionalTestFixtureExecutionListener.class})
public class ExtractionFixture extends org.inra.ecoinfo.extraction.AbstractExtractionFixture {

    /**
     *
     */
    public ExtractionFixture() {
        super();
        MockitoAnnotations.initMocks(this);
    }

    /**
     *
     * @param projetName
     * @param sitesListNames
     * @param especeListNames
     * @param variableListNames
     * @param dateDedebut
     * @param datedeFin
     * @param filecomps
     * @param commentaire
     * @param affichage
     * @return
     * @throws BusinessException
     */
    public String extract(String projetName, String sitesListNames, String especeListNames, String variableListNames, String dateDedebut, String datedeFin, String filecomps, String commentaire, String affichage) throws BusinessException {

        final String projetCode = Utils.createCodeFromString(projetName);
        final Map<String, Object> metadatasMap = new HashMap<>();
        final String[] sitesNames = sitesListNames.split(" *, *");
        final List<IFlatNode> siteNodes = new LinkedList<>();
        IFlatNode projetNode = null;
        for (final String siteName : sitesNames) {
            final Stream<INode> sitesStream = MonSoereTransactionalTestFixtureExecutionListener.policyManager.getNodesByNodeableCode(Utils.createCodeFromString("sitemonsoere/" + siteName), WhichTree.TREEDATASET);
            Set<INode> sites = sitesStream
                    .filter(t -> t.getParent() != null && t.getParent().getNodeable().getCode().equals(projetCode)).collect(Collectors.toSet());
            projetNode = sites.stream()
                    .findFirst()
                    .map(n -> new FlatNode(n.getParent(), MonSoereTransactionalTestFixtureExecutionListener.policyManager.getCurrentUser().getOwnGroup(WhichTree.TREEDATASET)))
                    .orElseThrow(BusinessException::new);
            sites.stream()
                    .map(n -> new FlatNode(n, MonSoereTransactionalTestFixtureExecutionListener.policyManager.getCurrentUser().getOwnGroup(WhichTree.TREEDATASET)))
                    .collect(Collectors.toCollection(() -> siteNodes));
            
        }
        final String[] especeNames = especeListNames.split(" *, *");
        final List<Espece> especes = new LinkedList<>();
        for (final String especeName : especeNames) {
            final Espece espece = MonSoereTransactionalTestFixtureExecutionListener.especeDAO.getEspeceByCode(Utils.createCodeFromString(especeName))
                    .orElseThrow(()-> new BusinessException("espèce non valide" + especeName));
            especes.add(espece);
        }
        final String[] variableNames = variableListNames.split(" *, *");
        final Set<VariableMonSoere> variables = new TreeSet<>();
        for (final String variableName : variableNames) {
            final Variable variable = MonSoereTransactionalTestFixtureExecutionListener.variableDAO.getByCode(Utils.createCodeFromString(variableName))
                    .orElseThrow(()-> new BusinessException("variable non valide" + variableName));
            variables.add((VariableMonSoere) variable);
        }
        Map<String, FileComp> files = MonSoereTransactionalTestFixtureExecutionListener.fileCompManager.getAllFilesComp()
                .stream()
                .collect(Collectors.toMap(f->f.getFileName(), f->f));
        final Set<FileComp> fileCompsSet = Stream.of(filecomps.split(" *, *"))
                .filter(file->files.containsKey(file))
                .map(file->files.get(file))
                .collect(Collectors.toSet());
        final DatesYearsContinuousFormParamVO datesYearsContinuousFormParamVO = new DatesYearsContinuousFormParamVO(MonSoereTransactionalTestFixtureExecutionListener.localizationManager);
        datesYearsContinuousFormParamVO.getPeriods().get(0).put(AbstractDatesFormParam.START_INDEX, dateDedebut);
        datesYearsContinuousFormParamVO.getPeriods().get(0).put(AbstractDatesFormParam.END_INDEX, datedeFin);

        metadatasMap.put(Site.class.getSimpleName(), siteNodes);
        metadatasMap.put(Site.class.getSimpleName() + "_Id", siteNodes.stream()
                .map(t -> t.getNodeableId())
                .distinct()
                .collect(Collectors.toList()));

        metadatasMap.put(Projet.class.getSimpleName(), projetNode);
        metadatasMap.put(Projet.class.getSimpleName() + "_Id", projetNode.getId());

        metadatasMap.put(Espece.class.getSimpleName(), especes);
        metadatasMap.put(FileComp.class.getSimpleName(), new LinkedList<>(fileCompsSet));
        metadatasMap.put(Variable.class.getSimpleName(), variables);
        metadatasMap.put(DatesYearsContinuousFormParamVO.class.getSimpleName(), datesYearsContinuousFormParamVO);
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, commentaire);

        final PemParameters parameters = new PemParameters(metadatasMap);
        try {
            MonSoereTransactionalTestFixtureExecutionListener.extractionManager.extract(parameters, Integer.parseInt(affichage));
        } catch (final NumberFormatException | BusinessException e) {
            return "false : " + e.getMessage();
        }
        return "true";
    }
}

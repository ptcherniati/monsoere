package org.inra.ecoinfo.monsoere.mga.configurator;

import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.configurator.AbstractMgaDisplayerConfiguration;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import static org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator.*;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.monsoere.refdata.projet.Projet;
import org.inra.ecoinfo.monsoere.refdata.site.SiteMonSoere;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.inra.ecoinfo.refdata.theme.Theme;

/**
 *
 * @author yahiaoui
 */
public class MgaIOConfigurator extends AbstractMgaIOConfigurator {

    private static final Integer[] NORMAL_ORDER_0_1_2_3 = new Integer[]{0, 1, 2, 3};
    private static final Integer[] INVERSE_ORDER_3_0_2_1 = new Integer[]{3, 0, 2, 1};
    private static final Integer[] DATASET_ORDER_0 = new Integer[]{3, 0, 2, 1};
    private static final Class<INodeable>[] ENTRY_INSTANCE_PSTD = new Class[]{
        Projet.class,
        SiteMonSoere.class,
        Theme.class,
        DataType.class
    };
    private static final Class<INodeable>[] ENTRY_INSTANCE_PSTDV = new Class[]{
        Projet.class,
        SiteMonSoere.class,
        Theme.class,
        DataType.class,
        DatatypeVariableUnite.class
    };
    private static final Class<INodeable>[] ENTRY_INSTANCE_REF = new Class[]{
        Refdata.class
    };
    private static final Activities[] ACTIVITIES_SAPDSE
            = new Activities[]{
                Activities.synthese,
                Activities.administration,
                Activities.publication,
                Activities.depot,
                Activities.suppression,
                Activities.extraction};
    private static final Activities[] ACTIVITIES_ESTA
            = new Activities[]{
                Activities.edition,
                Activities.suppression,
                Activities.telechargement,
                Activities.administration};
    private static final Activities[] ACTIVITIES_A
            = new Activities[]{
                Activities.associate
            };

    /**
     *
     */
    public MgaIOConfigurator() {
        super(new AbstractMgaDisplayerConfiguration() {
        });
    }

    /**
     *
     */
    @Override
    protected void initConfigurations() {

        /**
         * Configuration Zero
         */

        boolean includeAncestorDataset = true;

        Activities[] activitiesDataset = ACTIVITIES_SAPDSE;

        WhichTree whichTreeDataSet = WhichTree.TREEDATASET;

        Class stickyLeafDatasetRights = DatatypeVariableUnite.class;

        boolean displayColumnNamesDataset = false;

        Configuration configDatasetRights
                = new Configuration(
                        DATASET_CONFIGURATION_RIGHTS,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_PSTDV,
                        activitiesDataset,
                        INVERSE_ORDER_3_0_2_1,
                        includeAncestorDataset,
                        whichTreeDataSet,
                        stickyLeafDatasetRights,
                        displayColumnNamesDataset);
        Configuration configDataset
                = new Configuration(
                        DATASET_CONFIGURATION,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_PSTD,
                        activitiesDataset,
                        INVERSE_ORDER_3_0_2_1,
                        includeAncestorDataset,
                        whichTreeDataSet,
                        null,
                        displayColumnNamesDataset);

        /**
         * Configuration Zero
         */

        boolean includeAncestorRefdata = true;

        WhichTree whichTreeRefdata = WhichTree.TREEREFDATA;

        Class<DatatypeVariableUnite> stickyLeafRefdata = null;

        boolean displayColumnNamesRefdata = false;

        AbstractMgaIOConfiguration configRefdataRights
                = new Configuration(REFDATA_CONFIGURATION_RIGHTS,
                        Refdata.class,
                        DATASET_ORDER_0,
                        ENTRY_INSTANCE_REF,
                        ACTIVITIES_ESTA,
                        DATASET_ORDER_0,
                        includeAncestorRefdata,
                        whichTreeRefdata,
                        stickyLeafRefdata,
                        displayColumnNamesRefdata);

        AbstractMgaIOConfiguration configRefdata
                = new Configuration(REFDATA_CONFIGURATION,
                        Refdata.class,
                        DATASET_ORDER_0,
                        ENTRY_INSTANCE_REF,
                        ACTIVITIES_ESTA,
                        DATASET_ORDER_0,
                        includeAncestorRefdata,
                        whichTreeRefdata,
                        stickyLeafRefdata,
                        displayColumnNamesRefdata);

        AbstractMgaIOConfiguration configExtraction
                = new Configuration(SYNTHESIS_CONFIGURATION,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_PSTD,
                        activitiesDataset,
                        INVERSE_ORDER_3_0_2_1,
                        includeAncestorDataset,
                        whichTreeDataSet,
                        null,
                        displayColumnNamesDataset);

        /**
         * Configuration Associate // For RefData Insertion
         */

        AbstractMgaIOConfiguration configAssociate
                = new Configuration(ASSOCIATE_CONFIGURATION,
                        DataType.class,
                        NORMAL_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_PSTD,
                        ACTIVITIES_A,
                        INVERSE_ORDER_3_0_2_1,
                        includeAncestorDataset,
                        whichTreeDataSet,
                        null,
                        displayColumnNamesDataset);
        /**
         * ------------------------------------------------------------------------------------
         * *
         */
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION_RIGHTS, k -> configDatasetRights);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION_RIGHTS, k -> configRefdataRights);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION, k -> configRefdata);
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION, k -> configDataset);
        getConfigurations().computeIfAbsent(ASSOCIATE_CONFIGURATION, k -> configAssociate);
        getConfigurations().computeIfAbsent(SYNTHESIS_CONFIGURATION, k -> configExtraction);

        /**
         * ------------------------------------------------------------------------------------
         * *
         */
    }

    static class MgaIOConfiguration extends AbstractMgaIOConfiguration {

        public MgaIOConfiguration(Integer codeConfiguration, Class<? extends INodeable> leafType, Integer[] entryOrder, Class<INodeable>[] entryTypes, Activities[] activities, Integer[] sortOrder, boolean includeAncestor, WhichTree whichTree, Class<? extends INodeable> stickyLeafType, boolean displayColumnNames) {
            super(codeConfiguration, leafType, entryOrder, entryTypes, activities, sortOrder, includeAncestor, whichTree, stickyLeafType, displayColumnNames);
        }

    }

    static class Configuration extends AbstractMgaIOConfiguration {

        public Configuration(Integer codeConfiguration, Class<? extends INodeable> leafType, Integer[] entryOrder, Class<INodeable>[] entryTypes, Activities[] activities, Integer[] sortOrder, boolean includeAncestor, WhichTree whichTree, Class<? extends INodeable> stickyLeafType, boolean displayColumnNames) {
            super(codeConfiguration, leafType, entryOrder, entryTypes, activities, sortOrder, includeAncestor, whichTree, stickyLeafType, displayColumnNames);
        }

    }

}

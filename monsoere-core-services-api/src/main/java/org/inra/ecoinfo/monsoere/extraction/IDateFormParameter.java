package org.inra.ecoinfo.monsoere.extraction;

import java.util.List;

/**
 * The Interface IDateFormParameter.
 */
public interface IDateFormParameter {

    /**
     * Gets the periods from date form parameter.
     *
     * @return the periods from date form parameter
     */
    List<Periode> getPeriodsFromDateFormParameter();
}

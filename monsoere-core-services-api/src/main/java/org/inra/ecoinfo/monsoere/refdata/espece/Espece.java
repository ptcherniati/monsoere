package org.inra.ecoinfo.monsoere.refdata.espece;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.utils.Utils;

/**
 * The Class Espece.
 *
 * @author philippe
 * @author "Guillaume Enrico"
 */
@Entity
@Table(name = Espece.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = { "esp_nom" }))
public class Espece implements Comparable<Espece>, Serializable {

    /**
     * The Constant ID_JPA.
     */
    static public final String  ID_JPA          = "esp_id";
    /**
     * The Constant NAME_ENTITY_JPA.
     */
    static public final String  NAME_ENTITY_JPA = "espece_esp";
    /**
     * The Constant STRING_EMPTY.
     */
    static private final String STRING_EMPTY    = "";
    /**
     * The id.
     */
    @Id
    @Column(name = Espece.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long                id;
    /**
     * The code.
     */
    @Column(nullable = false, unique = true, name = "esp_code", length = 150)
    private String              code;
    /**
     * The definition.
     */
    @Column(nullable = true, name = "esp_definition", length = 150)
    private String              definition      = Espece.STRING_EMPTY;
    /**
     * The nom.
     */
    @Column(nullable = false, unique = true, name = "esp_nom", length = 150)
    @NaturalId
    private String              nom             = Espece.STRING_EMPTY;

    /**
     * Instantiates a new espece.
     */
    public Espece() {
        super();
    }

    /**
     * Instantiates a new espece.
     *
     * @param nom
     *            the nom
     * @param definition
     *            the definition
     */
    public Espece(final String nom, final String definition) {
        super();
        this.code = Utils.createCodeFromString(nom);
        this.nom = nom;
        this.definition = definition;
    }

    @Override
    public int compareTo(Espece o) {
        return getCode().compareTo(o.getCode());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else {
            return EqualsBuilder.reflectionEquals(this, obj, new String[] { "id" });
        }
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the definition.
     *
     * @return the definition
     */
    public String getDefinition() {
        return definition == null ? Espece.STRING_EMPTY : definition;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return nom == null ? Espece.STRING_EMPTY : nom;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, new String[] { "id" });
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Sets the definition.
     *
     * @param definition
     *            the new definition
     */
    public void setDefinition(final String definition) {
        this.definition = definition;
    }

    /**
     * Sets the description.
     *
     * @param definition
     *            the new description
     */
    public void setDescription(final String definition) {
        this.definition = definition == null ? Espece.STRING_EMPTY : definition;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Sets the nom.
     *
     * @param nom
     *            the new nom
     */
    public void setNom(final String nom) {
        this.nom = nom == null ? Espece.STRING_EMPTY : nom;
        this.code = Utils.createCodeFromString(this.nom);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return nom;
    }
}

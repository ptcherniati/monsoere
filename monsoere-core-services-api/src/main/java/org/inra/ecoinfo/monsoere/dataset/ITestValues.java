package org.inra.ecoinfo.monsoere.dataset;

import com.Ostermiller.util.CSVParser;
import java.io.Serializable;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface ITestValues.
 */
public interface ITestValues extends Serializable {

    /**
     *
     * @param startline
     * @param parser
     * @param versionFile
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @param datatypeName
     * @throws BusinessException
     */
    void testValues(long startline, CSVParser parser, VersionFile versionFile, String encoding,
            BadsFormatsReport badsFormatsReport, DatasetDescriptor datasetDescriptor,
            String datatypeName) throws BusinessException;
}

package org.inra.ecoinfo.monsoere.dataset;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.io.Serializable;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IProcessRecord.
 */
public interface IProcessRecord extends Serializable {

    /**
     * Lookup dataset descriptor.
     *
     * @param datasetDescriptorPath
     *            the dataset descriptor path
     * @return the dataset descriptor
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    DatasetDescriptor lookupDatasetDescriptor(String datasetDescriptorPath) throws IOException;

    /**
     *
     * @param parser
     * @param versionFile
     * @param fileEncoding
     * @param datasetDescriptor
     * @throws BusinessException
     */
    void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding,
            DatasetDescriptor datasetDescriptor) throws BusinessException;
}

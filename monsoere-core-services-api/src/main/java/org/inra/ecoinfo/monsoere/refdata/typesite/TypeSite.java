package org.inra.ecoinfo.monsoere.refdata.typesite;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.monsoere.refdata.site.SiteMonSoere;

/**
 * The Class TypeSite.
 *
 * @author philippe
 * @author "Guillaume Enrico"
 */
@Entity(name = "TypeSitePEM")
@Table(name = TypeSite.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = { "tze_code" }))
public class TypeSite implements Serializable{

    /**
     * The Constant ID_JPA.
     */
    static public final String  ID_JPA          = "tze_id";
    /**
     * The Constant NAME_ENTITY_JPA.
     */
    static public final String  NAME_ENTITY_JPA = "types_zone_etude_tze";
    /**
     * The Constant STRING_EMPTY.
     */
    static private final String STRING_EMPTY    = "";
    /**
     * The id.
     */
    @Id
    @Column(name = TypeSite.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long                id;
    /**
     * The code.
     */
    @Column(nullable = false, unique = true, name = "tze_code", length = 150)
    private String              code;
    /**
     * The definition.
     */
    @Column(nullable = true, name = "tze_definition", length = 150)
    private String              definition      = TypeSite.STRING_EMPTY;
    /**
     * The sites.
     */
    @OneToMany(mappedBy = "typeSite", cascade = ALL)
    @LazyToOne(LazyToOneOption.PROXY)
    private List<SiteMonSoere>  sites           = new LinkedList<>();

    /**
     * Instantiates a new type site.
     */
    public TypeSite() {
        super();
    }

    /**
     * Instantiates a new type site.
     *
     * @param code
     *            the code
     * @param definition
     *            the definition
     */
    public TypeSite(final String code, final String definition) {
        super();
        this.code = code;
        this.definition = definition;
    }

    /**
     * Adds the site.
     *
     * @param site
     *            the site
     */
    public void addSite(final SiteMonSoere site) {
        if (site == null) {
            return;
        }
        site.setTypeSite(this);
        if (!getSites().contains(site)) {
            getSites().add(site);
        }
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the definition.
     *
     * @return the definition
     */
    public String getDefinition() {
        return definition == null ? TypeSite.STRING_EMPTY : definition;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return code;
    }

    /**
     * Gets the sites.
     *
     * @return the sites
     */
    public List<SiteMonSoere> getSites() {
        return sites;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Sets the definition.
     *
     * @param definition
     *            the new definition
     */
    public void setDefinition(final String definition) {
        this.definition = definition == null ? TypeSite.STRING_EMPTY : definition;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Sets the sites.
     *
     * @param sites
     *            the new sites
     */
    public void setSites(final List<SiteMonSoere> sites) {
        this.sites = sites;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return code;
    }
}

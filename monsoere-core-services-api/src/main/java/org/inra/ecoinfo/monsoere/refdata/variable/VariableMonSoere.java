package org.inra.ecoinfo.monsoere.refdata.variable;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.refdata.variable.Variable;

/**
 *
 * @author tcherniatinsky
 */

@Entity(name= "VariableMonSoere")
@Table(name = VariableMonSoere.NAME_ENTITY_JPA)
@PrimaryKeyJoinColumn(name = "VariableMonSoere_ID")
public class VariableMonSoere extends Variable {

    

    /**
     * The Constant serialVersionUID.
     */
    private static final long  serialVersionUID = 1L;
    /**
     * The Constant NAME_ENTITY_JPA.
     */
    static public final String NAME_ENTITY_JPA  = "variablemonsoere";

    /**
     * Instantiates a new variable mon soere.
     */
    public VariableMonSoere() {
        super();       
    }

    /**
     *
     * @param id
     */
    public VariableMonSoere(final Long id) {
        super(id);        
    }

    /**
     *
     * @param nom
     * @param isQualitative
     */
    public VariableMonSoere(final String nom, final Boolean isQualitative) {
        super();
        setCode(nom);
        setIsQualitative(isQualitative);
    }

    /**
     *
     * @param nom
     * @param definition
     * @param affichage
     */
    public VariableMonSoere(final String nom, final String definition, final String affichage) {
        super(nom, definition, affichage, false);
        // TODO Auto-generated constructor stub
    }

    /**
     *
     * @param nom
     * @param definition
     * @param affichage
     * @param isQualitative
     */
    public VariableMonSoere(final String nom, final String definition, final String affichage,
            final Boolean isQualitative) {
        super(nom, definition, affichage, isQualitative);        
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else {
            return EqualsBuilder.reflectionEquals(this, obj, new String[] { "id" });
        }
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
    
    /**
     *
     * @param <T>
     * @return
     */

    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) VariableMonSoere.class;
    }
    
}

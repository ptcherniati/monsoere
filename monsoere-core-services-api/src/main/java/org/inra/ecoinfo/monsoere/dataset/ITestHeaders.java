package org.inra.ecoinfo.monsoere.dataset;

import com.Ostermiller.util.CSVParser;
import java.io.Serializable;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface ITestHeaders.
 */
public interface ITestHeaders extends Serializable {

    /**
     *
     * @param parser
     * @param versionFile
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @return
     * @throws BusinessException
     */
    long testHeaders(CSVParser parser, VersionFile versionFile, String encoding,
            BadsFormatsReport badsFormatsReport, DatasetDescriptor datasetDescriptor)
            throws BusinessException;
}

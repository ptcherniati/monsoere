/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.monsoere.refdata.site;


import org.inra.ecoinfo.monsoere.refdata.typesite.TypeSite;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class SiteMonSoereTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    SiteMonSoere s1;
    SiteMonSoere s2;
    SiteMonSoere s3;
    SiteMonSoere s4;
    SiteMonSoere s5;
    SiteMonSoere as1;

    TypeSite     ts1 = new TypeSite();

    TypeSite     ts2 = new TypeSite();

    /**
     *
     */
    public SiteMonSoereTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        s1 = new SiteMonSoere(ts1, as1, "nom1", "desciption1");
        as1 = new SiteMonSoere(ts1, as1, "nom1", "desciption1");
        s2 = new SiteMonSoere(ts1, as1, "nom2", "desciption1");
        s3 = new SiteMonSoere(ts1, s1, "nom1", "desciption1");
        s4 = new SiteMonSoere(ts1, null, "Nom4", "desciption1");
        s5 = new SiteMonSoere(ts1, s4, "Nom5", "desciption1");
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of equals method, of class SiteMonSoere.
     */
    @Test
    public void testEquals() {
        Assert.assertEquals(s1, s1);
        Assert.assertEquals(s1, as1);
        Assert.assertNotSame(s1, s2);
        Assert.assertNotSame(s1, s3);
    }

    /**
     * Test of getId method, of class SiteMonSoere.
     */
    @Test
    public void testGetId() {
        SiteMonSoere instance = new SiteMonSoere();
        Long expResult = 1L;
        instance.setId(expResult);
        Long result = instance.getId();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getRoot method, of class SiteMonSoere.
     */
    @Test
    public void testGetRoot() {
        SiteMonSoere result = s5.getRoot();
        Assert.assertEquals(s4, result);
    }

    /**
     * Test of getRootSite method, of class SiteMonSoere.
     */
    @Test
    public void testGetRootSite() {
        SiteMonSoere result = s5.getRoot();
        Assert.assertEquals(s4, result);
    }

    /**
     * Test of getTypeSite method, of class SiteMonSoere.
     */
    @Test
    public void testGetTypeSite() {
        new SiteMonSoere();
        Assert.assertEquals(ts1, s1.getTypeSite());
    }

    /**
     * Test of toString method, of class SiteMonSoere.
     */
    @Test
    public void testToString() {
        Assert.assertEquals("Site : Nom4/Nom5", s5.toString());
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.monsoere.extraction;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class PeriodeTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    Periode instance;

    String  dateDebut = "datedébut";

    String  dateFin   = "datefin";

    /**
     *
     */
    public PeriodeTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        instance = new Periode(dateDebut, dateFin);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getDateEnd method, of class Periode.
     */
    @Test
    public void testGetDateEnd() {
        String result = instance.getDateEnd();
        Assert.assertEquals(dateFin, result);
    }

    /**
     * Test of getDateStart method, of class Periode.
     */
    @Test
    public void testGetDateStart() {
        String result = instance.getDateStart();
        Assert.assertEquals(dateDebut, result);
    }

    /**
     * Test of setDateEnd method, of class Periode.
     */
    @Test
    public void testSetDateEnd() {
        instance.setDateEnd(dateDebut);
        Assert.assertEquals(dateDebut, instance.getDateEnd());
    }

    /**
     * Test of setDateStart method, of class Periode.
     */
    @Test
    public void testSetDateStart() {
        instance.setDateStart(dateFin);
        Assert.assertEquals(dateFin, instance.getDateStart());
    }

}

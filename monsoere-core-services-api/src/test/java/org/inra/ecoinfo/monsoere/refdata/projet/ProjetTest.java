/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.monsoere.refdata.projet;


import org.inra.ecoinfo.monsoere.refdata.site.SiteMonSoere;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author ptcherniati
 */
public class ProjetTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    @Mock
    SiteMonSoere s1;

    @Mock
    SiteMonSoere s2;

    @Mock
    SiteMonSoere s3;

    /**
     *
     */
    public ProjetTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getCode method, of class Projet.
     */
    @Test
    public void testGetCode() {
        Projet instance = new Projet();
        String expResult = "projet";
        instance.setCode(expResult);
        instance.getCode();
    }

    /**
     * Test of getDescriptionProjet method, of class Projet.
     */
    @Test
    public void testGetDescriptionProjet() {
        Projet instance = new Projet();
        String expResult = "description";
        instance.setDescriptionProjet(expResult);
        String result = instance.getDescriptionProjet();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getId method, of class Projet.
     */
    @Test
    public void testGetId() {
        Projet instance = new Projet();
        Long expResult = 1L;
        instance.setId(expResult);
        Long result = instance.getId();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getNom method, of class Projet.
     */
    @Test
    public void testGetName() {
        Projet instance = new Projet();
        String expResult = "Nom accentué";
        instance.setCode(expResult);
        String result = instance.getName();
        Assert.assertEquals(expResult, result);
        Assert.assertEquals("Nom accentué", instance.getCode());

    }

}

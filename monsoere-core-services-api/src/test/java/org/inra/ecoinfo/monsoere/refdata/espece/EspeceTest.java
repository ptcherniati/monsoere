/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.monsoere.refdata.espece;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class EspeceTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    private Espece espece1;
    private Espece autreEspece1;

    private Espece espece2;

    private Espece espece3;

    /**
     *
     */
    public EspeceTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        espece1 = new Espece("Nom1", "Définition1");
        espece1.setId(1L);
        autreEspece1 = new Espece("Nom1", "Définition1");
        espece2 = new Espece("Nom2", "Définition2");
        espece2.setId(2L);
        espece3 = new Espece("Nom3", "Définition3");
        espece3.setId(3L);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of compareTo method, of class Espece.
     */
    @Test
    public void testCompareTo() {
        // equals
        Espece o = autreEspece1;
        Espece instance = espece1;
        int expResult = 0;
        int result = instance.compareTo(o);
        Assert.assertEquals(expResult, result);
        // less
        o = espece2;
        instance = espece1;
        expResult = -1;
        result = instance.compareTo(o);
        Assert.assertEquals(expResult, result);
        // less
        o = espece1;
        instance = espece2;
        expResult = 1;
        result = instance.compareTo(o);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Espece.
     */
    @Test
    public void testEquals() {
// equals
                Object obj = espece1;
        Espece instance = espece1;
        boolean result = instance.equals(obj);
        Assert.assertTrue(result);
        // equals
        // equals
        // equals
        // equals
        // equals
        // equals
        // equals
        // equals
        obj = autreEspece1;
        instance = espece1;
        result = instance.equals(obj);
        Assert.assertTrue(result);
        // notequals
        // notequals
        // notequals
        // notequals
        obj = espece2;
        instance = espece1;
        result = instance.equals(obj);
        Assert.assertFalse(result);
        // not espece
        // not espece
                obj = "";
        instance = espece1;
        result = instance.equals(obj);
        Assert.assertFalse(result);
        // null
                obj = null;
        instance = espece1;
        result = instance.equals(obj);
        Assert.assertFalse(result);
    }

    /**
     * Test of getCode method, of class Espece.
     */
    @Test
    public void testGetCode() {
        String result = espece1.getCode();
        Assert.assertEquals("nom1", result);
    }

    /**
     * Test of getDefinition method, of class Espece.
     */
    @Test
    public void testGetDefinition() {
        String result = espece1.getDefinition();
        Assert.assertEquals("Définition1", result);
    }

    /**
     * Test of getId method, of class Espece.
     */
    @Test
    public void testGetId() {
        Long result = espece1.getId();
        Assert.assertTrue(1L == result);
    }

    /**
     * Test of getNom method, of class Espece.
     */
    @Test
    public void testGetNom() {
        String result = espece1.getNom();
        Assert.assertEquals("Nom1", result);
    }

    /**
     * Test of hashCode method, of class Espece.
     */
    @Test
    public void testHashCode() {
// equals
                Assert.assertEquals(espece1.hashCode(), autreEspece1.hashCode());
        // non equals
        Assert.assertNotSame(espece1.hashCode(), espece2.hashCode());
    }

    /**
     * Test of setNom method, of class Espece.
     */
    @Test
    public void testSetNom() {
        String nom = "Un nom avec éè";
        Espece instance = new Espece();
        instance.setNom(nom);
        Assert.assertEquals("Un nom avec éè", instance.getNom());
        Assert.assertEquals("un_nom_avec_ee", instance.getCode());
    }

    /**
     * Test of toString method, of class Espece.
     */
    @Test
    public void testToString() {
        Assert.assertEquals("Nom1", espece1.toString());
    }

}

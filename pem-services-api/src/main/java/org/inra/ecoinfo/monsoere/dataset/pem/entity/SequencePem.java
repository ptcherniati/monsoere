package org.inra.ecoinfo.monsoere.dataset.pem.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

/**
 * The Class SequencePem.
 */
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = {
        VersionFile.ID_JPA, "date"})},
        indexes = {
            @Index(name = "sequence_pem_version_idx", columnList = VersionFile.ID_JPA)
        }
      //  name = SequencePem.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {version, "date" })
)
public class SequencePem implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long  serialVersionUID = 1L;
    /**
     * The Constant TABLE_NAME.
     */
    static public final String TABLE_NAME       = "sequence_pem";
    /**
     * The Constant ID_JPA.
     */
    static public final String ID_JPA           = "spem_id";
    /**
     * The id.
     */
    @Id
    @Column(name = SequencePem.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long             id;
    /**
     * The date.
     */
    @Column(name = "date")
    private LocalDate               date;
    /**
     * The version.
     */
    @ManyToOne(cascade = { PERSIST, MERGE, REFRESH }, optional = false, targetEntity = VersionFile.class)
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private VersionFile        version;
    /**
     * The mesures pem.
     */
    @OneToMany(mappedBy = "sequencePem", cascade = ALL)
    private List<MesurePem>    mesuresPem       = new LinkedList<>();

    /**
     * Instantiates a new sequence pem.
     */
    public SequencePem() {
        super();
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the mesures pem.
     *
     * @return the mesures pem
     */
    public List<MesurePem> getMesuresPem() {
        return mesuresPem;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public VersionFile getVersion() {
        return version;
    }

    /**
     * Sets the date.
     *
     * @param date
     *            the new date
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Sets the mesures pem.
     *
     * @param mesuresPem
     *            the new mesures pem
     */
    public void setMesuresPem(List<MesurePem> mesuresPem) {
        this.mesuresPem = mesuresPem;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the new version
     */
    public void setVersion(VersionFile version) {
        this.version = version;
    }
}

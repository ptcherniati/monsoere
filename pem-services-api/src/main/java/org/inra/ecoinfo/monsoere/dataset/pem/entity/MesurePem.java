package org.inra.ecoinfo.monsoere.dataset.pem.entity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.monsoere.refdata.espece.Espece;

/**
 * The Class MesurePem.
 */
@Entity
@Table(name = MesurePem.TABLE_NAME, 
        uniqueConstraints = { @UniqueConstraint(columnNames = {
        SequencePem.ID_JPA, Espece.ID_JPA  })},
        indexes = {
            @Index(name = "mesure_pem_espece_idx", columnList = Espece.ID_JPA),
            @Index(name = "mesure_pem_sequence_idx", columnList = SequencePem.ID_JPA)
        })
public class MesurePem implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long   serialVersionUID       = 1L;
    /**
     * The Constant TABLE_NAME.
     */
    public static final String  TABLE_NAME             = "mesure_pem";
    /**
     * The Constant ID_JPA.
     */
    public static final String  ID_JPA                 = "mpem_id";
    /**
     * The id.
     */
    @Id
    @Column(name = MesurePem.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long              id;
    /**
     * The espece.
     */
    @ManyToOne(cascade = { PERSIST, MERGE, REFRESH }, optional = false, targetEntity = Espece.class)
    @JoinColumn(name = Espece.ID_JPA, referencedColumnName = Espece.ID_JPA, nullable = false)
    @NaturalId
    private Espece              espece;
    /**
     * The sequence pem.
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
    @JoinColumn(name = SequencePem.ID_JPA, referencedColumnName = SequencePem.ID_JPA, nullable = false)
    @NaturalId
    private SequencePem         sequencePem;
    /**
     * The valeurs pem.
     */
    @OneToMany(mappedBy = "mesurePem", cascade = ALL)
    private List<ValeurPem>     valeursPem             = new LinkedList<>();

    /**
     * Adds the valeur pem.
     *
     * @param valeurPem
     *            the valeur pem
     */
    public void addValeurPem(ValeurPem valeurPem) {
        if (!valeursPem.contains(valeurPem)) {
            this.valeursPem.add(valeurPem);
            valeurPem.setMesurePem(this);
        }
    }

    /**
     * Gets the espece.
     *
     * @return the espece
     */
    public Espece getEspece() {
        return espece;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the sequence pem.
     *
     * @return the sequence pem
     */
    public SequencePem getSequencePem() {
        return sequencePem;
    }

    /**
     * Gets the valeurs pem.
     *
     * @return the valeurs pem
     */
    public List<ValeurPem> getValeursPem() {
        return valeursPem;
    }

    /**
     * Sets the espece.
     *
     * @param espece
     *            the new espece
     */
    public void setEspece(Espece espece) {
        this.espece = espece;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Sets the sequence pem.
     *
     * @param sequencePem
     *            the new sequence pem
     */
    public void setSequencePem(SequencePem sequencePem) {
        this.sequencePem = sequencePem;
    }

    /**
     * Sets the valeurs pem.
     *
     * @param valeurs
     *            the new valeurs pem
     */
    public void setValeursPem(List<ValeurPem> valeurs) {
        this.valeursPem = valeurs;
    }
}

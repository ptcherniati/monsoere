package org.inra.ecoinfo.monsoere.dataset.pem;

import java.time.LocalDate;
import java.util.List;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.MesurePem;
import org.inra.ecoinfo.monsoere.refdata.espece.Espece;

/**
 * The Interface IMesurePemDAO.
 * @param <D>
 */
public interface IMesurePemDAO<D> extends IDAO<MesurePem> {

    /**
     * Extract datas.
     *
     * @param projetSiteIds
     *            the projet site ids
     * @param especeIds
     *            the espece ids
     * @param dateRequestParamVO
     *            the date request param vo
     * @param user
     * @return the list
     */
    List<MesurePem> extractDatas(List<Long> projetSiteIds, List<Long> especeIds, D dateRequestParamVO, IUser user);

    /**
     *
     * @param projetSiteIds
     * @param especeIds
     * @param dateRequestParamVO
     * @param user
     * @return
     */
    Long getExtractionSize(List<Long> projetSiteIds, List<Long> especeIds, D dateRequestParamVO, IUser user);

//    List<FileComp> retrievePemAvailablesAssociates(List<Long> pathes, Date dateStart, Date dateEnd)
//            throws PersistenceException;

    /**
     * Retrieve pem availables especes.
     *
     * @param plateformeId
     *            the plateforme id
     * @param dateStart
     *            the date start
     * @param dateEnd
     *            the date end
     * @return the list
     */
    List<Espece> retrievePemAvailablesEspeces( List<Long> plateformeId, LocalDate dateStart, LocalDate dateEnd);
}

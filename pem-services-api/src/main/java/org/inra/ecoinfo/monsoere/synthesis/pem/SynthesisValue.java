package org.inra.ecoinfo.monsoere.synthesis.pem;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 * The Class SynthesisValue.
 */
@Entity(name = "PemSynthesisValue")
@Table(indexes = { 
    @Index(name = "PemSynthesisValue_site_variable_idx", columnList = "site,variable") })
public class SynthesisValue extends GenericSynthesisValue {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new synthesis value.
     */
    public SynthesisValue() {
        super();
    }

    /**
     * Instantiates a new synthesis value.
     *
     * @param date
     *            the date
     * @param site
     *            the site
     * @param variable
     *            the variable
     * @param valueFloat
     *            the value float
     * @param valueString
     *            the value string
     */
    public SynthesisValue(final LocalDate date, final String site, final String variable,
            final Double valueFloat, final String valueString, long idNode) {
        super(date.atStartOfDay(), site, variable, valueFloat==null?null:valueFloat.floatValue(), valueString, idNode, valueString!=null);
    }
}

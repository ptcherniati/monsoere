package org.inra.ecoinfo.monsoere.dataset.pem;

import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.SequencePem;

/**
 * The Interface ISequencePemDAO.
 */
public interface ISequencePemDAO extends IDAO<SequencePem> {

    /**
     *
     * @param date
     * @param version
     * @return
     */
    Optional<SequencePem> getByDateAndVersion(LocalDate date, VersionFile version);
}

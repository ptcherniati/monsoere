package org.inra.ecoinfo.monsoere.dataset.pem.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;

/**
 * The Class ValeurPem.
 */
@Entity
@Table(name = ValeurPem.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {
        MesurePem.ID_JPA, RealNode.ID_JPA }),
        indexes = {
            @Index(name = "valeur_pem_mesure_idx", columnList = MesurePem.ID_JPA),
            @Index(name = "valeur_pem_realnode_idx", columnList = RealNode.ID_JPA),
            @Index(name = "valeur_pem_vq_idx", columnList = ValeurQualitative.ID_JPA)
        })
public class ValeurPem implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long     serialVersionUID                     = 1L;
    /**
     * The Constant TABLE_NAME.
     */
    static public final String    TABLE_NAME                           = "valeur_pem";
    /**
     * The Constant ID_JPA.
     */
    static public final String    ID_JPA                               = "vpem_id";
    /**
     * The Constant RESOURCE_PATH_WITH_VARIABLE_SPECIFIC.
     */
    static final String           RESOURCE_PATH_WITH_VARIABLE_SPECIFIC = "%s/%s/%s/%s/%s";
    /**
     * The id.
     */
    @Id
    @Column(name = ValeurPem.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long                id;
    /**
     * The mesure pem.
     */
    @ManyToOne(cascade = { PERSIST, MERGE, REFRESH })
    @JoinColumn(name = MesurePem.ID_JPA, referencedColumnName = MesurePem.ID_JPA, nullable = false)
    @NaturalId
    protected MesurePem           mesurePem;

    /**
     * The datatype variable unite @link(RealNode).
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    @NaturalId
    RealNode         realNode;
    
    /**
     * The value.
     */
    @Column(name = "vpm_value", nullable = true)
    private Float                 value;
    /**
     * The valeur qualitative.
     */
    @ManyToOne(cascade = { PERSIST, MERGE, REFRESH }, targetEntity = ValeurQualitative.class, optional = true)
    @JoinColumn(name = ValeurQualitative.ID_JPA, referencedColumnName = ValeurQualitative.ID_JPA, nullable = true)
    private ValeurQualitative     valeurQualitative;

    /**
     * Instantiates a new valeur pem.
     */
    public ValeurPem() {
        super();
    }

    /**
     * Instantiates a new valeur pem.
     *
     * @param mesurePem
     *            the mesure pem
     */
    public ValeurPem(MesurePem mesurePem) {
        this.mesurePem = mesurePem;
        if (mesurePem != null) {
            mesurePem.getValeursPem().add(this);
        }
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     * Gets the mesure pem.
     *
     * @return the mesure pem
     */
    public MesurePem getMesurePem() {
        return mesurePem;
    }

    
    /**
     * Gets the valeur qualitative.
     *
     * @return the valeur qualitative
     */
    public ValeurQualitative getValeurQualitative() {
        return valeurQualitative;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public Float getValue() {
        return value;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Sets the mesure pem.
     *
     * @param mesurePem
     *            the new mesure pem
     */
    public void setMesurePem(MesurePem mesurePem) {
        this.mesurePem = mesurePem;
    }

    /**
     * Sets the valeur qualitative.
     *
     * @param valeurQualitative
     *            the new valeur qualitative
     */
    public void setValeurQualitative(ValeurQualitative valeurQualitative) {
        this.valeurQualitative = valeurQualitative;
        this.value = null;
    }

    /**
     * Sets the value.
     *
     * @param value
     *            the new value
     */
    public void setValue(Float value) {
        this.value = value;
        this.valeurQualitative = null;
    }

    /**
     * Sets the value.
     *
     * @param value
     *            the new value
     */
    public void setValue(ValeurQualitative value) {
        setValeurQualitative(value);
    }
}

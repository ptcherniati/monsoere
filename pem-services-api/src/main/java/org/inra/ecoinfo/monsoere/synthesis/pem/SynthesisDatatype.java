package org.inra.ecoinfo.monsoere.synthesis.pem;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;

/**
 * The Class SynthesisDatatype.
 */
@Entity(name = "PemSynthesisDatatype")
public class SynthesisDatatype extends GenericSynthesisDatatype {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new synthesis datatype.
     */
    public SynthesisDatatype() {
        super();
    }

    /**
     * Instantiates a new synthesis datatype.
     *
     * @param site
     *            the site
     * @param minDate
     *            the min date
     * @param maxDate
     *            the max date
     */
    public SynthesisDatatype(final String site, final LocalDateTime minDate, final LocalDateTime maxDate, String idNodes) {
        super(minDate, maxDate, site, idNodes) ;
    }
}

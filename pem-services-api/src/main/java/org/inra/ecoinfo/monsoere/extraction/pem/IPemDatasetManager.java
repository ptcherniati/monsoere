package org.inra.ecoinfo.monsoere.extraction.pem;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.monsoere.refdata.espece.Espece;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * The Interface IPemDatasetManager.
 */
public interface IPemDatasetManager extends Serializable{

//    List<FileComp> retrieveListsAssociates(List<ProjetSite> projetSites,
//            List<VariableMonSoere> variables, Date beginDate, Date endDate)
//            throws BusinessException;

    /**
     * Retrieve lists especes.
     *
     * @param plateformeCode
     *            the plateforme id
     * @param dateStart
     *            the date start
     * @param dateEnd
     *            the date end
     * @return the list
     * @throws BusinessException
     *             the business exception
     */
    List<Espece> retrieveListEspeces( List<Long> plateformeCode, LocalDate dateStart, LocalDate dateEnd)
            throws BusinessException;

    /**
     * Retrieve lists variables.
     *
     * @param sites
     * @param dateStart
     * @param dateEnd
     * @return the list
     */

    List<INode> getAvailablesVariablesFilteredByrights(Set<String> sites, LocalDate dateStart, LocalDate dateEnd);

    /**
     *
     * @param currentUser
     * @return
     */
    List<INode> getSitesAvailables(IUser currentUser);
}

package org.inra.ecoinfo.monsoere.dataset.pem;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.monsoere.dataset.pem.entity.ValeurPem;

/**
 * The Interface IValeurPemDAO.
 */
public interface IValeurPemDAO extends IDAO<ValeurPem> {

    /**
     *
     * @param sites
     * @param dateStart
     * @param dateEnd
     * @param user
     * @return
     */
    List<INode> getAvailablesVariablesFilteredByRights(Set<String> sites, LocalDate dateStart, LocalDate dateEnd, IUser user);

    /**
     *
     * @param currentUser
     * @return
     */
    List<INode> getSitesAvailables(IUser currentUser);
}

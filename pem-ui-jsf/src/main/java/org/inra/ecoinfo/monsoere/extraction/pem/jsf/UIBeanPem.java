package org.inra.ecoinfo.monsoere.extraction.pem.jsf;

import com.google.common.base.Strings;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;


import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.filecomp.entity.FileType;
import org.inra.ecoinfo.filecomp.jsf.ItemFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.mga.business.composite.IFlatNode;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.ITreeNode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.mga.managedbean.UIBeanActionTree;
import org.inra.ecoinfo.monsoere.extraction.DatesYearsContinuousFormParamVO;
import org.inra.ecoinfo.monsoere.extraction.pem.IPemDatasetManager;
import org.inra.ecoinfo.monsoere.extraction.pem.impl.PemDatasetManager;
import org.inra.ecoinfo.monsoere.extraction.pem.impl.PemParameters;
import org.inra.ecoinfo.monsoere.refdata.espece.Espece;
import org.inra.ecoinfo.monsoere.refdata.projet.Projet;
import org.inra.ecoinfo.monsoere.refdata.variable.VariableMonSoere;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.UncatchedExceptionLogger;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.primefaces.model.TreeNode;
import org.slf4j.LoggerFactory;
import org.springframework.orm.jpa.JpaTransactionManager;

/**
 *
 * @author tcherniatinsky
 */
@ManagedBean(name = "uiPem")
@ViewScoped
public class UIBeanPem extends AbstractUIBeanForSteps implements Serializable {

    private static final String DATATYPE = "piegeage_en_montee";

    private static final String RULE_NAVIGATION_JSF = "pem";
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ManagedProperty(value = "#{policyManager}")
    protected IPolicyManager policyManager;

    /**
     *
     */
    @ManagedProperty(value = "#{localizationManager}")
    protected ILocalizationManager localizationManager;

    /**
     *
     */
    @ManagedProperty(value = "#{pemDatasetManager}")
    protected IPemDatasetManager pemDatasetManager;

    /**
     *
     */
    @ManagedProperty(value = "#{notificationsManager}")
    protected INotificationsManager notificationsManager;
    @ManagedProperty(value = "#{extractionManager}")
    private IExtractionManager extractionManager;

    /**
     *
     */
    @ManagedProperty(value = "#{transactionManager}")
    protected JpaTransactionManager transactionManager;
    @ManagedProperty(value = "#{uiActionTree}")
    private UIBeanActionTree uiBeanActionTree;

    /**
     *
     */
    @ManagedProperty(value = "#{fileCompManager}")
    protected IFileCompManager fileCompManager;
    private Properties propertiesProjectsNames;
    private Properties propertiesSiteNames;
    private Properties propertiesVariableNames;
    private long idProjectSelected;
    private long idPlateformeSelected;
    private long idEspeceSelected;
    private long idAssociateSelected;
    private long idVariableSelected;
    private Map<Long, ProjectJSF> projectsAvailables = new TreeMap();
    private final Map<Long, EspeceJSF> especesAvailables = new HashMap<>();
    private final Map<Long, AssociateJSF> associatesAvailables = new HashMap<>();
    private final Map<Long, VariableJSF> variablesAvailables = new HashMap<>();
    private String affichage = "1";
    private ParametersRequest parametersRequest = new ParametersRequest();

    /**
     *
     * @throws BusinessException
     */
    public UIBeanPem() throws BusinessException {
        super();
        setEnabledMotivation(true);        
    }

    /**
     *
     * @return
     */
    public String addAllAssociates() {
        final Map<Long, AssociateJSF> associatesSelected = getParametersRequest().getAssociatesSelected();
        associatesSelected.clear();
        associatesAvailables.values().stream().map((associate) -> {
            associatesSelected.put(associate.getFile().getFileComp().getId(), associate);
            return associate;
        }).forEach((associate) -> {
            associate.setSelected(true);
        });
        return null;
    }

    /**
     *
     * @return
     */
    public String addAllEspeces() {
        final Map<Long, EspeceJSF> especesSelected = getParametersRequest().getEspecesSelected();
        especesSelected.clear();
        especesAvailables.values().stream().map((espece) -> {
            especesSelected.put(espece.getEspece().getId(), espece);
            return espece;
        }).forEach((espece) -> {
            espece.setSelected(true);
        });
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String addAllVariables() throws BusinessException {
        final Map<Long, VariableJSF> variablesSelected = parametersRequest.getVariablesSelected();
        variablesSelected.clear();
        variablesAvailables.values().stream().map((variable) -> {
            variablesSelected.put(variable.getVariable().getId(), variable);
            return variable;
        }).forEach((variable) -> {
            variable.setSelected(true);
        });
        return null;
    }

    private void case3() {
        try {
            createOrUpdateListEspecesAvailables();
        } catch (final BusinessException e) {
            LoggerFactory.getLogger(getClass()).error( "update espece error", e);
            especesAvailables.clear();
        } catch (final DateTimeParseException e) {
            LoggerFactory.getLogger(getClass()).error( "update espece error", e);
        }
    }

    private void case4() {
        try {
            createOrUpdateListVariablesAvailables();
        } catch (final BusinessException e) {
            variablesAvailables.clear();
            LoggerFactory.getLogger(getClass()).error( "update variable erro", e);
        } catch (final DateTimeParseException e) {
            LoggerFactory.getLogger(getClass()).error( "update variable erro", e);
        }
    }

    private void case5() throws BadExpectedValueException {
        try {
            createOrUpdateListAssociatesAvailables();
        } catch (final BusinessException e) {
            LoggerFactory.getLogger(getClass()).error( "update associates error", e);
            especesAvailables.clear();
        } catch (final DateTimeParseException e) {
            LoggerFactory.getLogger(getClass()).error( "update associates error", e);
        }
    }

    /**
     *
     * @param id
     * @return
     * @throws BusinessException
     */
    public String changeProject(long id) throws BusinessException {
        idProjectSelected = idProjectSelected == id ? -1L : id;
        parametersRequest.getPlateformesSelected().clear();
        projectsAvailables.entrySet().stream()
                .map((projectSelectedEntry) -> {
                    projectSelectedEntry.getValue().checked = projectSelectedEntry.getKey() == idProjectSelected;
                    return projectSelectedEntry;
                }).forEach((projectSelectedEntry) -> {
            projectSelectedEntry.getValue().getPlateformes().values().stream().forEach((plateformeSelected) -> {
                plateformeSelected.setSelected(false);
            });
        });
        return null;
    }

    private void createOrUpdateListAssociatesAvailables() throws BusinessException, DateTimeParseException,
            BadExpectedValueException {
        associatesAvailables.clear();
        parametersRequest.getAssociatesSelected().clear();
        List<Long> nodeIds = new LinkedList();
        parametersRequest.variablesSelected
                .entrySet().stream().forEach((variableEntry) -> {
                    variableEntry.getValue().datatypeVariableUnites.stream().map(t -> t.getId()).forEach(nodeIds::add);
                });
        if (!parametersRequest.plateformesSelected.isEmpty()
                && parametersRequest.getVariableStepIsValid()) {
            IntervalDate intervalDate = new IntervalDate(parametersRequest.getFirstStartDate(),
                    parametersRequest.getLastEndDate(), DateUtil.DD_MM_YYYY);
            List<IntervalDate> intervalsDate = new LinkedList<>();
            intervalsDate.add(intervalDate);
            Map<String, Properties> propertiesForDescriptions = getPropertiesForDescriptions();
            Map<String, FileType> mapFileTypes = getMapFileTypes();
            final List<FileComp> files = fileCompManager.getFileCompFromPathesAndIntervalsDate(nodeIds, intervalsDate);
            files.stream()
                    .filter((filecomp) -> (!associatesAvailables.keySet().contains(filecomp.getId())))
                    .forEach((file) -> {
                        final AssociateJSF associateVO = new AssociateJSF(
                                new ItemFile(file, propertiesForDescriptions, mapFileTypes, policyManager.getCurrentUser().getLanguage())
                        );
                        associatesAvailables.put(file.getId(), associateVO);
                        if (associateVO.getMandatory()) {
                            parametersRequest.associatesSelected.put(file.getId(),
                                    associateVO);
                        }
                    });
        }
    }

    private Map<String, FileType> getMapFileTypes() throws BusinessException {
        Map<String, FileType> fileTypes = fileCompManager.retrieveListTypesPossible().stream()
                .collect(Collectors.toMap(f -> f.getCode(), f -> f));
        return fileTypes;
    }

    /**
     *
     * @return
     */
    public Map<String, Properties> getPropertiesForDescriptions() {
        Map<String, Properties> descriptionProperties = new HashMap<>();
        Localization.getLocalisations().stream().filter((locale) -> !(Localization.getDefaultLocalisation().equals(locale))).forEach((locale) -> {
            descriptionProperties.put(locale, localizationManager.newProperties(FileComp.NAME_ENTITY_JPA, FileComp.ENTITY_DESCRIPTION_COLUM_NAME, new Locale(locale)));
        });
        return descriptionProperties;
    }

    private void createOrUpdateListEspecesAvailables() throws BusinessException, DateTimeParseException {

        especesAvailables.clear();
        parametersRequest.getEspecesSelected().clear();
        if (!parametersRequest.plateformesSelected.isEmpty()
                && parametersRequest.getDateStepIsValid()) {

            final List<Espece> especes = pemDatasetManager.retrieveListEspeces(
                    new LinkedList<>(parametersRequest.plateformesSelected.keySet()),
                    parametersRequest.getFirstStartDate().toLocalDate(), parametersRequest.getLastEndDate().toLocalDate());

            especes.stream().forEach((espece) -> {
                //                if (!especesAvailables.keySet().contains(espece.getId())) {
                final EspeceJSF especeVO = new EspeceJSF(espece);
                especesAvailables.put(espece.getId(), especeVO);
//                }
            });
        }
    }

    private void createOrUpdateListVariablesAvailables() throws BusinessException, DateTimeParseException {

        variablesAvailables.clear();
        parametersRequest.getVariablesSelected().clear();
        if (!parametersRequest.plateformesSelected.isEmpty()
                && parametersRequest.getDateStepIsValid()) {
            Set<String> sites = parametersRequest.plateformesSelected.entrySet().stream()
                    .map(p -> p.getValue().getPath())
                    .distinct().collect(Collectors.toSet());
            List<INode> variablesNodes = pemDatasetManager.getAvailablesVariablesFilteredByrights(sites, parametersRequest.datesYearsContinuousFormParamVO.getDateStart(), parametersRequest.datesYearsContinuousFormParamVO.getDateEnd());
            variablesNodes.forEach((node) -> {
                DatatypeVariableUnite dvu = (DatatypeVariableUnite) node.getRealNode().getNodeable();
                if (variablesAvailables.containsKey(dvu.getVariable().getId())) {
                    variablesAvailables.get(dvu.getVariable().getId()).datatypeVariableUnites.add((NodeDataSet) node);
                } else {
                    variablesAvailables.put(dvu.getVariable().getId(), new VariableJSF(node));
                }
            });
        }
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String extract() throws BusinessException {
        final Map<String, Object> metadatasMap = new HashMap<>();
        List<IFlatNode> plateformes = parametersRequest.getPlateformesSelected().values().stream()
                .map(plateformeJSF -> plateformeJSF.getSite())
                .collect(Collectors.toList());
        metadatasMap.put(Site.class.getSimpleName(), plateformes);
        metadatasMap.put(Site.class.getSimpleName() + "_Id", parametersRequest.getPlateformesSelected().keySet().stream().collect(Collectors.toList()));
        IFlatNode projet = parametersRequest.getPlateformesSelected().values().stream()
                .map(p -> p.projet.projet)
                .findFirst().orElse(null);
        metadatasMap.put(Projet.class.getSimpleName(), projet);
        metadatasMap.put(Projet.class.getSimpleName() + "_Id", projet.getId());

        final List<Espece> especes = parametersRequest.getListEspecesSelected().stream().map(x -> x.getEspece()).collect(Collectors.toList());
        metadatasMap.put(Espece.class.getSimpleName(), especes);
        final List<FileComp> files = parametersRequest.getListAssociatesSelected().stream()
                .map(x -> x.getFile().getFileComp())
                .collect(Collectors.toList());
        metadatasMap.put(FileComp.class.getSimpleName(), files);
        final List<Variable> variables = parametersRequest.getListVariablesSelected().stream().map(x -> x.getVariable()).collect(Collectors.toList());
        metadatasMap.put(Variable.class.getSimpleName(), variables);
        metadatasMap.put(DatesYearsContinuousFormParamVO.class.getSimpleName(),
                parametersRequest.getDatesYearsContinuousFormParamVO());
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS,
                parametersRequest.getCommentExtraction());

        final PemParameters parameters = new PemParameters(metadatasMap);
        setStaticMotivation(getMotivation());
        extractionManager.extract(parameters, Integer.parseInt(affichage));
        return null;
    }

    /**
     *
     * @return
     */
    public String getAffichage() {
        return affichage;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public List<AssociateJSF> getAssociatesAvailables() throws BusinessException {
        if (associatesAvailables == null || associatesAvailables.isEmpty()) {
            try {
                createOrUpdateListAssociatesAvailables();
            } catch (DateTimeParseException | BadExpectedValueException ex) {
                LoggerFactory.getLogger(UIBeanPem.class.getName()).error(ex.getMessage());
            }
        }
        assert associatesAvailables != null : "null associates";
        return new LinkedList<>(associatesAvailables.values());
    }

    /**
     *
     * @return @throws BusinessException
     */
    public List<EspeceJSF> getEspecesAvailables() throws BusinessException {
        if (especesAvailables == null || especesAvailables.isEmpty()) {
            try {
                createOrUpdateListEspecesAvailables();
            } catch (DateTimeParseException ex) {
                LoggerFactory.getLogger(UIBeanPem.class.getName()).error(ex.getMessage());
            }
        }
        assert especesAvailables != null : "null spiecies";
        return new LinkedList<>(especesAvailables.values());
    }

    /**
     *
     * @return
     */
    public Long getIdAssociateSelected() {
        return idAssociateSelected;
    }

    /**
     *
     * @return
     */
    public Long getIdEspeceSelected() {
        return idEspeceSelected;
    }

    /**
     *
     * @return
     */
    public Long getIdPlateformeSelected() {
        return idPlateformeSelected;
    }

    /**
     *
     * @return
     */
    public Long getIdProjectSelected() {
        return idProjectSelected;
    }

    /**
     *
     * @return
     */
    public Long getIdVariableSelected() {
        return idVariableSelected;
    }

    @Override
    public boolean getIsStepValid() {
        if (getStep() == 1) {
            return getParametersRequest().getPlateformeStepIsValid();
        }
        if (getStep() == 2) {
            return getParametersRequest().getDateStepIsValid();
        }
        if (getStep() == 3) {
            return getParametersRequest().getEspeceStepIsValid();
        }
        if (getStep() == 4) {
            return getParametersRequest().getVariableStepIsValid();
        }
        if (getStep() == 5) {
            return getParametersRequest().getVariableStepIsValid();
        }
        return false;
    }

    /**
     *
     * @return
     */
    public ParametersRequest getParametersRequest() {
        return parametersRequest;
    }

    /**
     *
     */
    @PostConstruct
    public void loadProjectsAvailables() {
        uiBeanActionTree.loadTreeForUserAccordingLeafsRestrictions(
                PemDatasetManager.EXTRACTION_CONFIGURATION, "currentuser", "ALL", "extraction");

        List<TreeNode> projets = uiBeanActionTree.getCurrentTreeNode().getChildren();
        List<Long> sitesAvailables = pemDatasetManager.getSitesAvailables(policyManager.getCurrentUser()).stream()
                .map(n -> n.getId())
                .collect(Collectors.toList());
        projectsAvailables = projets.stream()
                .map(node
                        -> getProjectFromNode(node, sitesAvailables))
                .filter(
                        p -> !p.getPlateformes().isEmpty())
                .collect(Collectors.toMap(
                        k -> k.getProjet().getNodeableId(),
                        k -> k));
        if (projectsAvailables.size() == 1) {
            projectsAvailables.values().iterator().next().setChecked(true);
        }
    }

    /**
     *
     * @param node
     * @param sitesNodeAvailables
     * @return
     */
    protected ProjectJSF getProjectFromNode(TreeNode node, List<Long> sitesNodeAvailables) {
        IFlatNode selectedLocalNode = ((ITreeNode<IFlatNode>) node.getData()).getData();
        ProjectJSF projetJSF = new ProjectJSF(selectedLocalNode, selectedLocalNode.getNodeableId());
        List<TreeNode> treeNodeSite = node.getChildren();
        treeNodeSite.stream()
                .flatMap(this::getLeaves)
                .distinct()
                .map((treeSite) -> ((ITreeNode<IFlatNode>) treeSite.getData()).getData())
                .filter(
                        f -> sitesNodeAvailables.contains(f.getId()))
                .forEach((nodeDataSetSite) -> {
                    PlateformeJSF plateformeJSF = new PlateformeJSF(nodeDataSetSite, projetJSF);
                    projetJSF.getPlateformes().put(nodeDataSetSite.getNodeableId(), plateformeJSF);
                });
        return projetJSF;
    }

    private Stream<TreeNode> getLeaves(TreeNode node) {
        return node.isLeaf()
                ? Stream.of(node.getParent())
                : node.getChildren().stream().flatMap(n -> getLeaves(n));
    }

    /**
     *
     * @return @throws BusinessException
     */
    public Map<Long, ProjectJSF> getProjectsAvailables() throws BusinessException {
        return projectsAvailables;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String getProjetSelected() throws BusinessException {
        String localizedProjetSelected = null;
        final String nom = projectsAvailables.get(idProjectSelected).getProjet().getName();
        localizedProjetSelected = getPropertiesProjectsNames().getProperty(nom);
        if (Strings.isNullOrEmpty(localizedProjetSelected)) {
            localizedProjetSelected = nom;
        }
        return localizedProjetSelected;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesProjectsNames() {
        if (this.propertiesProjectsNames == null) {
            this.setPropertiesProjectsNames(localizationManager.newProperties(Site.NAME_ENTITY_JPA,
                    "nom"));
        }
        return propertiesProjectsNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesSiteNames() {
        if (this.propertiesSiteNames == null) {
            this.setPropertiesSiteNames(localizationManager.newProperties(Site.NAME_ENTITY_JPA,
                    "nom"));
        }
        return propertiesSiteNames;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesVariableNames() {
        if (this.propertiesVariableNames == null) {
            this.setPropertiesVariableNames(localizationManager.newProperties(
                    VariableMonSoere.NAME_ENTITY_JPA, "nom"));
        }
        return propertiesVariableNames;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public List<VariableJSF> getVariablesAvailables() throws BusinessException {
        if (variablesAvailables == null) {
            try {
                createOrUpdateListVariablesAvailables();
            } catch (DateTimeParseException ex) {
                LoggerFactory.getLogger(UIBeanPem.class.getName()).error(ex.getMessage());
            }
        }
        assert variablesAvailables != null : "null variables";
        return new LinkedList<>(variablesAvailables.values());
    }

    /**
     *
     * @return
     */
    public String navigate() {
        return UIBeanPem.RULE_NAVIGATION_JSF;
    }

    @Override
    public String nextStep() {
        super.nextStep();
        switch (getStep()) {
            case 3:
                case3();
                break;
            case 4:
                case4();
                break;
            case 5:
                try {
                    case5();
                } catch (BadExpectedValueException e) {
                    UncatchedExceptionLogger.log("can't get next step", e);
                }
                break;
            default:
                break;
        }
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String removeAllAssociates() throws BusinessException {
        try {
            createOrUpdateListAssociatesAvailables();
        } catch (DateTimeParseException | BadExpectedValueException ex) {
            LoggerFactory.getLogger(UIBeanPem.class.getName()).error(ex.getMessage());
        }
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String removeAllEspeces() throws BusinessException {
        try {
            createOrUpdateListEspecesAvailables();
            return null;
        } catch (DateTimeParseException ex) {
            LoggerFactory.getLogger(UIBeanPem.class.getName()).error(ex.getMessage());
        }
        return null;
    }

    /**
     *
     * @return @throws BusinessException
     */
    public String removeAllVariables() throws BusinessException {
        try {
            createOrUpdateListVariablesAvailables();
        } catch (DateTimeParseException ex) {
            LoggerFactory.getLogger(UIBeanPem.class.getName()).error(ex.getMessage());
        }
        return null;
    }

    /**
     *
     * @param id
     * @return
     * @throws BusinessException
     */
    public String removeAssociate(Long id) throws BusinessException {
        idAssociateSelected = id;
        final AssociateJSF associateSelected = associatesAvailables.get(idAssociateSelected);
        parametersRequest.getAssociatesSelected().remove(idAssociateSelected);
        associateSelected.setSelected(false);
        return null;
    }

    /**
     *
     * @param id
     * @return
     * @throws BusinessException
     */
    public String selectAssociate(Long id) throws BusinessException {
        idAssociateSelected = id;
        final AssociateJSF associateSelected = associatesAvailables.get(idAssociateSelected);
        if (!associateSelected.getSelected()) {
            parametersRequest.getAssociatesSelected().remove(idAssociateSelected);
            associateSelected.setSelected(false);
        } else {
            parametersRequest.getAssociatesSelected().put(idAssociateSelected, associateSelected);
            associateSelected.setSelected(true);
        }
        return null;
    }

    /**
     *
     * @param id
     * @return
     * @throws BusinessException
     */
    public String selectEspece(Long id) throws BusinessException {
        idEspeceSelected = id;
        final EspeceJSF especeSelected = especesAvailables.get(idEspeceSelected);
        if (especeSelected.getSelected()) {
            parametersRequest.getEspecesSelected().remove(idEspeceSelected);
            especeSelected.setSelected(false);
        } else {
            parametersRequest.getEspecesSelected().put(idEspeceSelected, especeSelected);
            especeSelected.setSelected(true);
        }
        return null;
    }

    /**
     *
     * @param id
     * @throws BusinessException
     */
    public void selectPlateforme(Long id) throws BusinessException {
        idPlateformeSelected = id;
        PlateformeJSF plateformeSelected = null;
        for (Entry<Long, ProjectJSF> entry : projectsAvailables.entrySet()) {
            if (entry.getValue().getPlateformes().containsKey(id)) {
                plateformeSelected = entry.getValue().getPlateformes().get(id);
                idProjectSelected = entry.getValue().getProjet().getNodeableId();
                break;
            }
        }
        if (plateformeSelected == null) {
            return;
        }
        if (plateformeSelected.getSelected()) {
            parametersRequest.getPlateformesSelected().remove(idPlateformeSelected);
            plateformeSelected.setSelected(false);
        } else {
            parametersRequest.getPlateformesSelected()
                    .put(idPlateformeSelected, plateformeSelected);
            plateformeSelected.setSelected(true);
        }
    }

    /**
     *
     * @param id
     * @return
     * @throws BusinessException
     */
    public String selectVariable(Long id) throws BusinessException {
        idVariableSelected = id;
        final VariableJSF variableSelected = variablesAvailables.get(idVariableSelected);
        if (variableSelected.getSelected()) {
            parametersRequest.getVariablesSelected().remove(idVariableSelected);
            variableSelected.setSelected(false);
        } else {
            parametersRequest.getVariablesSelected().put(idVariableSelected, variableSelected);
            variableSelected.setSelected(true);
        }
        return null;
    }

    /**
     *
     * @param affichage
     */
    public void setAffichage(String affichage) {
        this.affichage = affichage;
    }

    /**
     *
     * @param extractionManager
     */
    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     *
     * @param fileCompManager
     */
    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

    /**
     *
     * @param idAssociateSelected
     */
    public void setIdAssociateSelected(Long idAssociateSelected) {
        this.idAssociateSelected = idAssociateSelected;
    }

    /**
     *
     * @param idEspeceSelected
     */
    public void setIdEspeceSelected(Long idEspeceSelected) {
        this.idEspeceSelected = idEspeceSelected;
    }

    /**
     *
     * @param idPlateformeSelected
     */
    public void setIdPlateformeSelected(Long idPlateformeSelected) {
        this.idPlateformeSelected = idPlateformeSelected;
    }

    /**
     *
     * @param idProjectSelected
     */
    public void setIdProjectSelected(Long idProjectSelected) {
        this.idProjectSelected = idProjectSelected;
    }

    /**
     *
     * @param idVariableSelected
     */
    public void setIdVariableSelected(Long idVariableSelected) {
        this.idVariableSelected = idVariableSelected;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @param notificationsManager
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     *
     * @param parametersRequest
     */
    public void setParametersRequest(ParametersRequest parametersRequest) {
        this.parametersRequest = parametersRequest;
    }

    /**
     *
     * @param pemDatasetManager
     */
    public void setPemDatasetManager(IPemDatasetManager pemDatasetManager) {
        this.pemDatasetManager = pemDatasetManager;
    }

    /**
     *
     * @param propertiesProjectsNames
     */
    public void setPropertiesProjectsNames(Properties propertiesProjectsNames) {
        this.propertiesProjectsNames = propertiesProjectsNames;
    }

    /**
     *
     * @param propertiesSiteNames
     */
    public void setPropertiesSiteNames(Properties propertiesSiteNames) {
        this.propertiesSiteNames = propertiesSiteNames;
    }

    /**
     *
     * @param propertiesVariableNames
     */
    public void setPropertiesVariableNames(Properties propertiesVariableNames) {
        this.propertiesVariableNames = propertiesVariableNames;
    }

    /**
     *
     * @param policyManager
     */
    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    /**
     *
     * @param transactionManager
     */
    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     *
     * @return
     */
    public UIBeanActionTree getUiBeanActionTree() {
        return uiBeanActionTree;
    }

    /**
     *
     * @param uiBeanActionTree
     */
    public void setUiBeanActionTree(UIBeanActionTree uiBeanActionTree) {
        this.uiBeanActionTree = uiBeanActionTree;
    }

    /**
     *
     */
    public static class AssociateJSF implements Serializable {

        private ItemFile file;
        private String localizedFileName;
        private Boolean selected = false;
        private boolean mandatory;

        /**
         *
         * @param file
         */
        public AssociateJSF(ItemFile file) {
            super();
            this.file = file;
            if (file != null) {
                this.localizedFileName = file.getOriginalFileName();
                this.mandatory = file.getMandatory();
                this.selected = file.getMandatory();
            }
        }

        /**
         *
         * @return
         */
        public ItemFile getFile() {
            return file;
        }

        /**
         *
         * @return
         */
        public String getLocalizedFileName() {
            return localizedFileName;
        }

        /**
         *
         * @return
         */
        public boolean getMandatory() {
            return mandatory;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param file
         */
        public void setFile(ItemFile file) {
            this.file = file;
        }

        /**
         *
         * @param localizedFileName
         */
        public void setLocalizedFileName(String localizedFileName) {
            this.localizedFileName = localizedFileName;
        }

        /**
         *
         * @param mandatory
         */
        public void setMandatory(boolean mandatory) {
            this.mandatory = mandatory;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

    }

    /**
     *
     */
    public static class EspeceJSF implements Serializable {

        private Espece espece;
        private String localizedEspeceName;
        private Boolean selected = false;

        /**
         *
         * @param espece
         */
        public EspeceJSF(Espece espece) {
            super();
            this.espece = espece;
            if (espece != null) {
                this.localizedEspeceName = espece.getNom();
            }
        }

        /**
         *
         * @return
         */
        public Espece getEspece() {
            return espece;
        }

        /**
         *
         * @return
         */
        public String getLocalizedEspeceName() {
            return localizedEspeceName;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param espece
         */
        public void setEspece(Espece espece) {
            this.espece = espece;
        }

        /**
         *
         * @param localizedEspeceName
         */
        public void setLocalizedEspeceName(String localizedEspeceName) {
            this.localizedEspeceName = localizedEspeceName;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }
    }


    /**
     *
     */
    public static class PlateformeJSF implements Serializable {

        private final IFlatNode site;
        private String localizedPlateformeName;
        private String localizedSiteName;
        private Boolean selected = false;
        private ProjectJSF projet;

        /**
         *
         * @param site
         * @param projet
         */
        public PlateformeJSF(IFlatNode site, ProjectJSF projet) {
            super();
            this.projet = projet;
            this.site = site;
            if (site != null) {
                this.localizedPlateformeName = site.getName();
            }
        }

        /**
         *
         * @return
         */
        public String getLocalizedPlateformeName() {
            return localizedPlateformeName;
        }

        /**
         *
         * @return
         */
        public String getLocalizedSiteName() {
            return localizedSiteName;
        }

        /**
         *
         * @return
         */
        public IFlatNode getSite() {
            return site;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param localizedPlateformeName
         */
        public void setLocalizedPlateformeName(String localizedPlateformeName) {
            this.localizedPlateformeName = localizedPlateformeName;
        }

        /**
         *
         * @param localizedSiteName
         */
        public void setLocalizedSiteName(String localizedSiteName) {
            this.localizedSiteName = localizedSiteName;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public String getPath() {
            return String.format("%s,%s", projet.projet.getCode(), site.getCode());
        }
    }

    /**
     *
     */
    public static class ProjectJSF{

        private final IFlatNode projet;
        private final Long idNodeProject;
        private String localizedProjetName;
        private boolean checked;
        private final Map<Long, PlateformeJSF> plateformes = new HashMap<>();

        /**
         *
         * @param flatNode
         * @param idNodeProject
         */
        public ProjectJSF(IFlatNode flatNode, Long idNodeProject) {

            super();
            this.projet = flatNode;
            this.idNodeProject = idNodeProject;

            if (projet != null) {

                this.localizedProjetName = flatNode.getName();
            }
        }

        /**
         *
         * @return
         */
        public boolean isChecked() {
            return checked;
        }

        /**
         *
         * @param checked
         */
        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        /**
         *
         * @return
         */
        public Long getIdNodeProject() {
            return idNodeProject;
        }

        /**
         *
         * @return
         */
        public String getLocalizedProjetName() {
            return localizedProjetName;
        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeJSF> getPlateformes() {
            return plateformes;
        }

        /**
         *
         * @return
         */
        public IFlatNode getProjet() {
            return projet;
        }

        /**
         *
         * @param localizedProjetName
         */
        public void setLocalizedProjetName(String localizedProjetName) {
            this.localizedProjetName = localizedProjetName;
        }
    }

    /**
     *
     */
    public class ParametersRequest {

        private Map<Long, PlateformeJSF> plateformesSelected = new HashMap<>();
        private Map<Long, EspeceJSF> especesSelected = new HashMap<>();
        private final Map<Long, AssociateJSF> associatesSelected = new HashMap<>();
        private final Map<Long, VariableJSF> variablesSelected = new HashMap<>();
        private DatesYearsContinuousFormParamVO datesYearsContinuousFormParamVO;
        private String commentExtraction;

        /**
         *
         */
        public ParametersRequest() {
            super();
        }

        /**
         *
         * @return
         */
        public Map<Long, AssociateJSF> getAssociatesSelected() {
            return associatesSelected;
        }

        /**
         *
         * @return
         */
        public String getCommentExtraction() {
            return commentExtraction;
        }

        /**
         *
         * @return
         */
        public Boolean getDateStepIsValid() {
            try {
                if (datesYearsContinuousFormParamVO != null) {
                    return datesYearsContinuousFormParamVO.getIsValid()
                            && getPlateformeStepIsValid();
                } else {
                    return false;
                }
            } catch (final Exception e) {
                UncatchedExceptionLogger.log("can't check is valid", e);
                return false;
            }
        }

        /**
         *
         * @return
         */
        public DatesYearsContinuousFormParamVO getDatesYearsContinuousFormParamVO() {
            if (datesYearsContinuousFormParamVO == null) {
                initDatesYearsContinuousFormParamVO();
            }
            return datesYearsContinuousFormParamVO;
        }

        /**
         *
         * @return
         */
        public Map<Long, EspeceJSF> getEspecesSelected() {
            return especesSelected;
        }

        /**
         *
         * @return
         */
        public Boolean getEspeceStepIsValid() {
            return getDateStepIsValid() && !especesSelected.keySet().isEmpty();
        }

        /**
         *
         * @return @throws DateTimeParseException
         */
        public LocalDateTime getFirstStartDate() throws DateTimeParseException {
            if (!datesYearsContinuousFormParamVO.getPeriods().isEmpty()) {
                LocalDate firstDate = null;
                for (final LocalDate mapDate : datesYearsContinuousFormParamVO.retrieveParametersMap()
                        .values()) {
                    if (firstDate == null) {
                        firstDate = mapDate;
                    } else if (firstDate.isAfter(mapDate)) {
                        firstDate = mapDate;
                    }
                }
                return firstDate.atStartOfDay();
            }
            return null;
        }

        /**
         *
         * @return
         */
        public boolean getFormIsValid() {
            return getVariableStepIsValid();
        }

        /**
         *
         * @return @throws DateTimeParseException
         */
        public LocalDateTime getLastEndDate() throws DateTimeParseException {
            if (!datesYearsContinuousFormParamVO.getPeriods().isEmpty()) {
                LocalDate lastDate = null;
                for (final LocalDate mapDate : datesYearsContinuousFormParamVO.retrieveParametersMap()
                        .values()) {
                    if (lastDate == null) {
                        lastDate = mapDate;
                    } else if (lastDate.isBefore(mapDate)) {
                        lastDate = mapDate;
                    }
                }
                return lastDate.plus(1, ChronoUnit.DAYS).atStartOfDay().minus(1, ChronoUnit.MICROS);
            }
            return null;
        }

        /**
         *
         * @return
         */
        public List<AssociateJSF> getListAssociatesSelected() {
            final List<AssociateJSF> associates = new LinkedList<>();
            associatesSelected.values().stream().forEach((associate) -> {
                associates.add(associate);
            });
            return associates;
        }

        /**
         *
         * @return
         */
        public List<EspeceJSF> getListEspecesSelected() {
            final List<EspeceJSF> especes = new LinkedList<>();
            especesSelected.values().stream().forEach((espece) -> {
                especes.add(espece);
            });
            return especes;
        }

        /**
         *
         * @return
         */
        public List<PlateformeJSF> getListPlateformesSelected() {
            return plateformesSelected.values().stream()
                    .collect(Collectors.toList());
        }

        /**
         *
         * @return
         */
        public List<VariableJSF> getListVariables() {
            return variablesSelected.values().stream()
                    .collect(Collectors.toList());
        }

        /**
         *
         * @return
         */
        public List<VariableJSF> getListVariablesSelected() {
            return new LinkedList<>(variablesSelected.values());
        }

        /**
         *
         * @return
         */
        public Map<Long, PlateformeJSF> getPlateformesSelected() {
            return plateformesSelected;
        }

        /**
         *
         * @return
         */
        public Boolean getPlateformeStepIsValid() {
            return !plateformesSelected.keySet().isEmpty();
        }

        /**
         *
         * @return
         */
        public Map<Long, VariableJSF> getVariablesSelected() {
            return variablesSelected;
        }

        /**
         *
         * @return
         */
        public Boolean getVariableStepIsValid() {
            return !variablesSelected.keySet().isEmpty() && getEspeceStepIsValid();
        }

        private void initDatesYearsContinuousFormParamVO() {
            datesYearsContinuousFormParamVO = new DatesYearsContinuousFormParamVO(
                    localizationManager);
        }

        /**
         *
         * @param commentExtraction
         */
        public void setCommentExtraction(String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         *
         * @param especesSelected
         */
        public void setEspecesSelected(Map<Long, EspeceJSF> especesSelected) {
            this.especesSelected = especesSelected;
        }

        /**
         *
         * @param plateformesSelected
         */
        public void setPlateformesSelected(Map<Long, PlateformeJSF> plateformesSelected) {
            this.plateformesSelected = plateformesSelected;
        }
    }

    /**
     *
     */
    public class VariableJSF {

        private final Set<NodeDataSet> datatypeVariableUnites = new HashSet();
        private final Variable variable;
        private String localizedVariableName;
        private Boolean selected = false;

        /**
         *
         * @param node
         */
        public VariableJSF(INode node) {
            super();
            datatypeVariableUnites.add((NodeDataSet) node);
            this.variable = ((DatatypeVariableUnite) node.getNodeable()).getVariable();
            if (variable != null) {
                this.localizedVariableName = getPropertiesVariableNames().getProperty(
                        variable.getName());
                if (Strings.isNullOrEmpty(this.localizedVariableName)) {
                    this.localizedVariableName = variable.getName();
                }
            }
        }

        /**
         *
         * @return
         */
        public Set<NodeDataSet> getDatatypeVariableUnites() {
            return datatypeVariableUnites;
        }

        /**
         *
         * @return
         */
        public String getLocalizedVariableName() {
            return localizedVariableName;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @return
         */
        public Variable getVariable() {
            return variable;
        }

        /**
         *
         * @param localizedVariableName
         */
        public void setLocalizedVariableName(String localizedVariableName) {
            this.localizedVariableName = localizedVariableName;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }
    }
}
